# UTR - Uvod u teoriju računarstva (Introduction to computer science)
This folder contains solutions to lab tasks of Introduction to computer science course.
Text of the lab tasks is in the .rar files of the subfolder.
Tests and evaluators for each tasks are in the .zip files of the subfolder.

## Content
* LAB-01 - Nondeterministic finite automaton with ε-moves
* LAB-02 - Minimization of Deterministic Finita Automaton
* LAB-03 - Pushdown automaton
* LAB-04 - Recursive descent LL(1) parser
* LAB-05 - Turing machine
