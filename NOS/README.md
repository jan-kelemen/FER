# NOS - Napredni operacijski sustavi (Advanced Operating Systems)

This folder contains solutions to lab tasks of Advanced Operating Systems course.  
Text of the lab tasks is in pdf files

## Content

* LAB-01 - Battleship game with message passing for communication
* LAB-02 - Interprocess synchronisation using distributed Lamport protocol and piplines for communication

