#ifndef MESSAGE_H
#define MESSAGE_H

#include <string>

namespace msg {
	namespace glob {
		void remove_message_queue(int);
	}
	
	namespace types {
		auto constexpr ready1 = 1l;
		auto constexpr ready2 = 2l;
		auto constexpr move = 3l;
		auto constexpr response = 4l;
		auto constexpr end = 5l;
	}

	auto constexpr data_size = 20;

	struct Buffer {
		long type;
		char data[data_size];

		Buffer(long t = 0);

		bool is_win();
	};

	int create_message_queue(int key);
	
	void remove_message_queue(int msqid);

	void send_message(int msqid, Buffer& buf, int size = data_size);

	Buffer receive_message(int msqid, long type = 0);
}

#endif
