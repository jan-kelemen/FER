#ifndef BOARD_H
#define BOARD_H

#include <array>
#include <iostream>

namespace gm {
	enum class Field { Empty, Ship, Hit };

	using Board = std::array<std::array<Field, 4>, 4>;

	bool has_live_ships(Board const& board);

	Board read_board(std::istream& stream);

	void print_board(std::ostream& stream, Board const& board);
}

#endif
