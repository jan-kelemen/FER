#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iostream>


#include "board.h"
#include "message.h"

void start_handshake(int msqid, int player) {
	using namespace msg;
	using namespace msg::types;

	Buffer buf(player == 1 ? ready1 : ready2);
	send_message(msqid, buf, 0);
	receive_message(msqid, player == 1 ? ready2 : ready1);
}

bool shoot(int msqid) {
	using namespace msg;

	Buffer to_send(types::move);
	std::cout << "send shot to: ";
	std::cin >> to_send.data[0] >> to_send.data[1];
	send_message(msqid, to_send, 2);
	
	auto rv = receive_message(msqid, types::response);
	std::cout << rv.data << "\n\n";
	
	return rv.is_win();
}

bool get_shot(int msqid, gm::Board& board) {
	using namespace msg;

	auto shoot_msg = receive_message(msqid, types::move);
	
	auto x = shoot_msg.data[0] - '0';
	auto y = shoot_msg.data[1] - '0';
	auto is_hit = board[x][y] == gm::Field::Ship;
	
	board[x][y] = gm::Field::Hit;
	auto is_alive = gm::has_live_ships(board);

	std::cout << "got shot to: " << x << ' ' << y << '\n';
	
	Buffer res(types::response);
	auto length = 0;
	if(!is_hit) {
		length = 4;
		strncpy(res.data, "miss", length);
	}
	else if(is_alive) {
		length = 3;
		strncpy(res.data, "hit", length);
	}
	else {
		length = 3;
		strncpy(res.data, "win", length);
	}

	std::cout << (res.is_win() ? "lost" : res.data) << "\n\n";
	send_message(msqid, res, length);

	return res.is_win();
}

int main(int argc, char** argv) {
	if(argc < 3) {
		std::cerr << "invalid number of arguments\n";
		return EXIT_FAILURE;
	}
	
	auto player = std::atoi(argv[1]);
	auto msqid = std::atoi(argv[2]);

	std::cout << "process: " << player << " msqid: " << msqid << '\n';
	
	gm::Board board;
	if(argc == 4) {
		std::ifstream file(argv[3]);
		board = gm::read_board(file);
	}
	else {
		board = gm::read_board(std::cin);
	}
	
	gm::print_board(std::cout, board);
	try {
		start_handshake(msqid, player);
		std::cout << "started\n";

		auto turn_to_shoot = player == 1;
		while(true) {
			if(turn_to_shoot) {
				auto game_end = shoot(msqid);
				if(game_end) {
					msg::Buffer buf(msg::types::end);
					msg::send_message(msqid, buf, 0);
					break;
				}
				turn_to_shoot = false;
			}
			else {
				auto game_end = get_shot(msqid, board);
				if(game_end) {
					break;
				}
				turn_to_shoot = true;
			}		
		}
	}
	catch (std::runtime_error& e) {
		std::cerr << e.what() << '\n';
	}

	std::cout << "press any key\n";
	std::cin.ignore();	
}
