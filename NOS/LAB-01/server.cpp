#include <csignal>
#include <iostream>
#include <string>

#include <unistd.h>

#include "message.h"

int main(int argc, char** argv) {
	auto msqid = -1;
	std::signal(SIGINT, &msg::glob::remove_message_queue);
	
	try {
		std::cout << "started server\n";
		
		msqid = msg::create_message_queue(getuid());
		std::cout << "created message queue with id " << msqid << '\n';
		
		auto msg = msg::receive_message(msqid, msg::types::end);

		msg::remove_message_queue(msqid);
		std::cout << "removed message queue\n";
	}
	catch (std::exception& e) {
		std::cerr << e.what() << '\n';
		msg::remove_message_queue(msqid);
	}
	std::cout << "press any key\n";
	std::cin.ignore();
}
