#include "board.h"

namespace gm {
	bool has_live_ships(Board const& board) {
		for(auto& r : board) {
			for(auto& f : r) {
				if(f == Field::Ship) {
					return true;
				}
			}
		}
		return false;
	}

	Board read_board(std::istream& stream) {
		Board rv;
		for(auto i = 0; i < 16; ++i) {
			char c;
			stream >> c;
			rv[i / 4][i % 4] = c == 'o' ? Field::Ship : Field::Empty;
		}
		return rv;
	}

	void print_board(std::ostream& stream, Board const& board) {
		for(auto& r : board) {
			for(auto& f : r) {
				switch(f) {
				case Field::Ship: stream << 'o'; break;
				case Field::Hit: stream << 'x'; break;
				case Field::Empty: stream << '-'; break;
				}
			}
			stream << '\n';
		}
	}
}
						
