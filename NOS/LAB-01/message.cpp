#include "message.h"

#include <cstring>
#include <exception>
#include <stdexcept>

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>

namespace msg {
	namespace {
		auto constexpr err_rv = -1;
	}
        
	namespace glob {
                auto msqid_ = -1;
                void remove_message_queue(int) {
                        msg::remove_message_queue(msqid_);
                }
        }
	
	Buffer::Buffer(long t): type{t}, data{0} {};

	bool Buffer::is_win() {
		return strncmp("win", data, 3) == 0;
	}

	int create_message_queue(int key) {
                auto rv = msgget(key, 0600 | IPC_CREAT);
                if(rv == err_rv) {
                        throw std::runtime_error("Unable to create message queue");
                }
                glob::msqid_ = rv;
                return rv;
        }
        
        void remove_message_queue(int msqid) {
                auto v = msgctl(msqid, IPC_RMID, nullptr);
                if(v == err_rv) {
                        throw std::runtime_error("Unable to clear message queue");
                }
        }

        void send_message(int msqid, Buffer& buf, int size) {
		auto v = msgsnd(msqid, &buf, size, 0);
		if(v == err_rv) {
			throw std::runtime_error("Unable to send message");
		}
	}

        Buffer receive_message(int msqid, long type) {
		Buffer rv;
		auto v = msgrcv(msqid, &rv, data_size, type, 0);
		if(v == err_rv) {
			throw std::runtime_error("Unable to receive message");
		}
		return rv;
	}
}

