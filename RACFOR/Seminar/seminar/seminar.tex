\documentclass[utf8, seminar, numeric]{fer}
\usepackage{booktabs}
\usepackage{url}
\usepackage{listings}
\usepackage{algpseudocode}
\usepackage{algorithm}

\lstset{
   basicstyle=\fontsize{11}{13}\selectfont\ttfamily
}

\renewcommand{\lstlistingname}{Isječak koda}

\begin{document}

% Ukljuci literaturu u seminar
\nocite{*}

\title{Dekompajleri \\ \large{Usporedba i mogućnosti}}

\author{Jan Kelemen}

\maketitle

\tableofcontents

\chapter{Uvod}
Ovaj rad bavi se dekompajlerima\footnote{Dekompajler - engl. decompiler}, alatima
koji na temelju izvršnih datoteka (ili neke razine međukoda) rekonstruiraju izvorni
kod programa. Na početku rada dan je pregled razina programskih jezika i jezici koji
tim razinama pripadaju. U nastavku je dan općenit pregled procesa dekompajliranja i
njegove faze. Nakon čega slijedi pregled i prikaz mogućnosti nekih postojećih alata
za dekompajliranje te moguće primjene dekompajliranja u računalnoj forenzici.

\chapter{Razine programskih jezika}
Programski jezici mogu se podijeliti na dvije razine, na programske jezike niže i
na programske jezike više razine.

\section{Programski jezici niske razine}
U programske jezike niske razine pripadaju strojni kod i asembler. Strojni kod
je način zapisa izvršnih programa koji računala razumiju i mogu izvršavati bez
dodatnih transformacija. On se sastoji niz bitova koji predstavljaju određene
instrukcije procesora. Asembler\footnote{Asembler - engl. assembly} je razina
apstrakcije iznad strojnog koda, često jedna naredba u asembleru odgovara jednoj
naredbi procesora u strojnom kodu, odnosno pružaju mapiranje iz strojnog koda u
oblik koji čovjek može jednostavnije čitati \cite{llpl}. Isječak koda \ref{lst:x86asm}
prikazuje asemblerski kod za računanje kvadrata broja.

\begin{lstlisting}[caption=Kod za arhitekturu x86 u asembleru,label=lst:x86asm]
square(int):
  imul edi, edi
  mov eax, edi
  ret
\end{lstlisting}

\section{Programski jezici visoke razine}
U programske jezike visoke razine pripadaju svi programski jezici koji su iznad
asemblera. Prema načinu izvršavanja dijele se na kompajlirane\footnote{
	Kompajlirani - engl. compiled}, interpretirane\footnote{
	Interpretirani - engl. interpreted} i transpajlirane\footnote{
	Transpajlirani - engl. transpiled}. Kompajlirani jezici prevode se
izravno iz izvornog koda u izvršni odnosno strojni kod, u ovu kategoriju spadaju
jezici poput C i C++. Interpretirani jezici se prilikom izvršavanja prevode u
izvršni kod, izvršni kod kod ovih jezika stvara se pomoću JIT\footnote{
	JIT - engl. Just In Time} kompajlera iz izvornog koda, kao što je slučaj
kod Pythona, ili se stvara iz međukoda kao što je slučaj kod Jave ili C\#. Transpajlirani
programski jezici su oni koji se prije izvođenja prevode u neki drugi programski jezik,
primjer ovakvog jezika jest TypeScript koji se prevodi u JavaScript \cite{hlpl}.
Isječak koda \ref{lst:cpp} prikazuje C++ kod za računanje kvadrata broja.

\begin{lstlisting}[caption=Kod u programskom jeziku C++,label=lst:cpp]
int square(int num) {
    return num * num;
}
\end{lstlisting}

\chapter{Dekompajliranje}
Dekompajliranje je suprotan postupak od kompajliranja, njime se od izvršnog
programa stvara izvorni kod. Cilj dekompajliranja dobiti je izvorni kod, koji je
istovjetan kodu iz kojeg je bio stvoren izvršni program. Postupak rada dekompajlera
može se podijeliti u nekoliko faza. Prva faza je predprocesiranje u kojoj se obrađuje
ulazna datoteka, pregledavaju se metapodaci i ostale informacije koje bi mogle pomoći
prilikom obrade. Druga faza je leksička analiza u kojoj se nizovi bitova grupiraju
u instrukcije. Treća faza je sintaksna analiza u kojoj se gradi sintaksno stablo.
Četvrta faza je stvaranje koda visoke razine, nakon čega slijedi optimizacija u
kojoj se pokušava dobiti više čitljiv programski kod \cite{guil:08}.

\section{Problemi kod dekompajliranja} \label{ssec:problems}
Iako sam postupak dekompajliranja ima vrlo slične korake kao i postupak kompajliranja\footnote{
	Postupkak kompajliranja dijeli se na iste faze, međutim rezultati pojedinih faza
	se razlikuju od onih prilikom dekompajliranja \cite{aho:06}.}, sam proces dekompajliranja
je složeniji i teži od kompajliranja. Jedan od problema koji dekompajleri moraju
rješiti je razdvajanje instrukcija i podataka, za svaki bajt ulaznog programa
potrebno je odlučiti da li se radi o instrukciji ili podatku, rješavanju ovog problema
također ne pomaže da su formati ulaznih datoteka često nedokumentirani. Postoji i
problem izgubljenih informacija, prilikom kompjaliranja izvornog koda sve pomoćne
informacije poput komentara ili imena varijabli i funkcija su odbačene, a mogle bi
pomoći u rekonstrukciji izvornog programa. Problem vezan uz gubitak informacija jest
i optimizacija, ukoliko je prilikom stvaranja izvršnog programa bila uključena
optimizacija, neke strukture i funkcije koje se nalaze u izvornom kodu nepovratno
su izgubljene, dok je njegovo semantičko značenje ostalo nepromijenjeno \cite{x86dis, guil:08}.

\section{Vrste dekompajlera}
S obzirom na mogućnost rekonstrukcije izvornog programa alate za dekompajliranje
izvršnog programa možemo podijeliti na dekompajlere i disasemblere\footnote{
	Disasembler - engl. disassembler}. Disasembleri nude mogućnost
rekonstrukcije izvršnog programa do razine asemblera, dok je dekompajlerima
moguće rekonstruirati i izvorni kod programa. Zbog problema prilikom postupka
dekompajliranja, spomenutih u poglavlju \ref{ssec:problems}, današnji dekompajeri
se najčešće mogu koristiti samo kao pomoć pri reverznom inženjerstvu programa
koji se analizira \cite{x86dis}. Većina dekompajlera podržava kao izlazni jezik
samo jedan programski jezik koji je najčešće C ili C++.

\chapter{Postojeći alati}
U ovome poglavlju predstavljeno je par dekompajlera, te njihove mogućnosti
na primjeru kratkog programa koji radi sortiranje bubble-sort algoritmom. Svi
alati predstavljeni u ovome poglavlju testirani su na Windows računalu s x86-64
arhitekturom.

\section{Snowman}
Snowman\footnote{Snowman - \url{https://derevenets.com/}} je besplatan disasembler
i dekompajler, koji podržava ARM, x86 i x86-64 arhitekture računala. Kao ulazne
formate moguće je koristiti ELF, Mach-O i PE izvršne datoteke, te radi na Windows
i Linux operacijskim sustavima. Rekonstruirani izvorni kod biti će u jeziku C++.
Alat nudi grafičko i konzolno sučelje. Slika \ref{gui:snowman} prikazuje grafičko 
sučelje alata Snowman.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.90\textwidth]{img/snowman.png}
    \caption{Grafičko sučelje alata Snowman}
    \label{gui:snowman}
\end{figure}

Alat je testiran na programu prikazanom u isječku koda \ref{cpp:original}.
\begin{lstlisting}[caption=C++ program na kojem je testiran Snowman,label=cpp:original]
void bubble(int* list, int n) {
    for(auto i = 0; i < n - 1; ++i) {
        for(auto j = 0; j < n - 1; ++j) {
            if(list[j] > list[j + 1]) {
                auto temp = list[j];
                list[j] = list[j + 1];
                list[j + 1] = list[j];
            }
        }
    }
}
\end{lstlisting}

Ovaj program kompajliran je s MSVC\footnote{MSVC - Microsoft Visual C++}
i gcc kompajlerom, rezultat dobiven alatom u oba slučaja bio je sličan,
rekonstruirani asemblerski kod imao je oko 13000 linija, dok je C++ kod
imao oko 4000 linija. Rekonstruirani kod je znatno duži, nego što je bio
izvorni kod, zbog toga što izvršni program ne sadrži samo naš kod već i kod iz 
standardne biblioteke jezika. Unatoč tome, moguće je pronaći rekonstruirani kod
bubble funkcije, prikazan u isječku koda \ref{cpp:snowman}.

\begin{lstlisting}[caption=Rekonstruirani kod bubble funkcije dobiven Snowmanom, label=cpp:snowman]
void fun_4112ad(int32_t* a1, int32_t a2) {
    int32_t ecx3; int32_t v4; int32_t v5;
    ecx3 = 57; while (ecx3) { --ecx3; }
    v4 = 0;
    while (v4 < a2 - 1) {
        v5 = 0;
        while (v5 < a2 - 1) {
            if (a1[v5] > (a1 + v5)[1]) {
                a1[v5] = (a1 + v5)[1];
                (a1 + v5)[1] = a1[v5];
            }
            ++v5;
        }
        ++v4;
    }
}
\end{lstlisting}

\section{RetargetableDecompiler}
RetargetableDecompiler\footnote{RetargetableDecompiler - \url{https://retdec.com/}}
je besplatan disasembler i dekompajler s podrškom za Intel x86, ARM, MIPS, PIC32
i PowerPC arhitekture, a podržava ELF, PE, Mach-O, COFF, AR i strojni kod kao
ulazne formate izvršnih datoteka. Dodatne mogućnosti koje nudi su statička analiza 
izvršnih datoteka, korištenje informacija za debugiranje\footnote{Debugiranje - engl. debugging}
i detekciju C++ hijerarhija klasa. Osim što podržava Windows i Linux operacijske
sustave, nudi i web sučelje na kojem se može testirati većina funkcionalnosti
ovog alata. 

Ovaj alat testiran je na istom izvršnom programu kao i Snowman alat, čiji je izvorni
kod prikazan u isječku koda \ref{cpp:original}. Unatoč tome što je kao ulaznu
datoteku dobio i datoteku s informacijama za debugiranje, rezultati su slični
rezultatima koji su bili dobiveni i sa Snowman alatom.

\section{dotPeek}
dotPeek\footnote{dotPeek - \url{https://www.jetbrains.com/decompiler/}} je besplatan
disasembler i dekompajler, za razliku od prethodno prikazanih alata koji su podržavali
izvršne datoteke pisane stvorene iz kompajliranih jezika, dotPeek namijenjen je za
.NET platformu. Izvršne datoteke koje se koriste na ovoj platformi pokreću se unutar
virtualnog stroja. Ovaj alat omogućava rekonstrukciju međukoda koji se izvršava unutar
ovog virtualnog stroja i rekonstrukciju C\# koda. Ovaj alat radi isključivo na Windows
operacijskom sustavu, a dolazi kao samostalan alat ili dodatak razvojnom okruženju
Microsoft Visual Studio. Slika \ref{gui:dotpeek} prikazuje sučelje alata dotPeek.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.90\textwidth]{img/dotpeek.png}
    \caption{Sučelje alata dotPeek}
    \label{gui:dotpeek}
\end{figure}

dotPeek testiran je na izvršnom programu stvorenom iz C\# koda koji implementira
bubble-sort algoritam, prikazan na isječku koda \ref{cs:original}, koji je bio
uspješno rekreiran u potpunosti, uz razliku da nije bio u mogućnosti rekonstruirati
imena varijabli, koje su u rekonstruiranom kodu imale nazive \texttt{index1}
i \texttt{index2} umjesto  originalnih \texttt{i} i \texttt{j}.

\begin{lstlisting}[caption=C\# program na kojem je testiran dotPeek,label=cs:original]
static void Bubble(int[] list) { 
    for (var i = 0; i < list.Length - 1; ++i) {
        for (var j = 0; j < list.Length - 1; ++j) {
            if (list[j] > list[j + 1]) {
                var temp = list[j];
                list[j] = list[j + 1];
                list[j + 1] = list[j];
            }
        }
    }
}
\end{lstlisting}

\chapter{Primjene dekompajliranja}
U ovome poglavlju predstavljene su dvije moguće uporabe dekompajliranja koje
su vezane uz računalnu forenziku i reverzno inženjerstvo, prva primjena je
ispravljanje grešaka u programima čiji izvorni kod nije dostupan, a druga primjena
je detektiranje autora programa.

\section{Ispravljanje grešaka u programima}
Prilikom korištenja nekog programa, moguće je da naići na neku grešku koja
onemogućava daljnju uporabu tog programa, ukoliko se taj program više ne održava
i izvorni kod nije dostupan, jedina mogućnost je da tu samostalno ispravljanje te greške.

Primjena dekompajlera u svrhe reverznog inženjerstva prikazana je u videu
\cite{exlordgp3} korisnika ExileLord\footnote{ExileLord (YouTube kanal) - \url{https://www.youtube.com/channel/UCfNRZxjhslYzbbsM-0ha4BA}}
koji je koristeći dekompajler popravio grešku u igri Guitar Hero III: Legends of Rock,
gdje se s tokom igranja smanjivao maksimalni broj pjesama koje su mogle biti
pridružene listi pjesama. U ovome videu prikazan je jedan od spomenutih
problema prilikom procesa dekompajliranja, korišteni dekompajler prepoznao kod za
konstruktor klase, kao niz podataka umjesto instrukcija.

\section{Detekcija autora programa}
Prilikom analize zloćudnih programa korisno bi bilo saznati tko je originalan autor
tog programa. Primjenom disasemblera i dekompajlera uz pomoć strojnog učenja
pokazano je da se uz određene granice preciznosti može odrediti tko je autor
originalnog programa. 

Rad \cite{rosenblum} prikazuje tehniku u kojoj se uspoređuju stilističke značajke
koda, poput grafova poziva funkcija, ova metoda je na setu programa od 20 autora
ispravno odredila izvornog autora u 77\% slučajeva, a u 94\% slučajeva se izvorni
autor nalazio u 5 najvjerojatnijih autora za taj program. Međutim kada se set
autora programa povećao na 191, korištena metoda je ispravno odredila autora u
51\% slučajeva.

Rad \cite{islam} prikazuje tehniku u kojoj se uz stilističkih značajki koda
uspoređuju i sintaksna stabla i tok kontrole dekompajliranog koda. Primjenom
ove metode na setu programa od 20 autora ispravno su određeni autori u 99\%
slučajeva, dok je na setu programa od 191 autora točno određen autor u 92\% slučaja.

Obje ove metode koristile su kao set za učenje koji je sadržavao barem 8 programa
od svakog autora.

\chapter{Zaključak}
Dekompajliranje je metoda koja može biti vrlo korisna u računalnoj forenzici i u
reverznom inženjerstvu. Iako današnje implementacije dekompajlera još uvijek
nisu dovoljno sofisticirane da bi omogućile rekonstrukciju cijelog izvršnog
programa u korektan istovjetan kod, svejedno mogu biti korisni u praksi kao
alat kojime je moguće detektirati zašto dolazi do pogreške u izvršnom programu
ili mogu pomoći u otkrivanju tko je autor zloćudnog programa.

\bibliography{literatura}
\bibliographystyle{fer}

\end{document}
