#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

// Reports a error and exits.
int report_error(char *message);

// Tells if the bit shoud be inverted
int should_invert(double probability);

// Transfers the input file with symmetric probability of error.
int transfer(FILE *input, FILE *output, double probability);

int main(int argc, char *argv[])
{
    double err_probability;
    FILE *input_file;
    FILE *output_file;

    argc == 4 || report_error("Insufficient arguments!");

    (input_file = fopen(argv[1], "rb")) || report_error("Can't open input file!");

    (output_file = fopen(argv[3], "wb")) || report_error("Can't open output file!");

    err_probability = atof(argv[2]);

    transfer(input_file, output_file, err_probability);

    fclose(input_file);
    fclose(output_file);

    return 0;
}

int report_error(char *message)
{
    fprintf(stderr, message);
    exit(-1);

    return 0;
}

int should_invert(double probability)
{
    return rand() < probability * ((double) RAND_MAX + 1.0);
}

int transfer(FILE *input, FILE *output, double probability)
{
    uint8_t byte = 0;
    int i = 0;
    uint8_t mask = 0x80;

    while (!feof(input))
    {
        fread(&byte, sizeof(byte), 1, input);
        for (i = 0, mask = 0x80; i < 8; i++, mask >>= 1)
        {
            if (should_invert(probability))
            {
                byte ^= mask;
            }
        }
        fwrite(&byte, sizeof(byte), 1, output);
    }

    return 1;
}