# TINF - Teorija informacije (Information theory)
Laboratory exercise for Information Theory course.

### Task description
Check *zadatak.pdf*.

### Task list
1. Statisticka analiza izvorista informacije (Josip Tomurad)
2. Kompresija podataka
  1. Huffmanova metoda (Jan Kelemen)
  2. Metoda rjecnika: LZW (Jan Kelemen)
  3. Usporedba metoda (Mihael Varga)
3. Zastita podataka
  1. Linearni blok kod (Tomislav Stanić)
  2. Binarni simetricni kanal (Mihael Varga)
  3. Prijenos podataka binarnim simetricnim kanalom (Tomislav Stanić)

### Bugs
Linear block code doesn't work.
  
### Group members
- [Jan Kelemen](https://github.com/jan-kelemen)
- [Tomislav Stanić](https://github.com/kiec)
- [Josip Tomurad](https://github.com/Anakin9)
- [Mihael Varga](https://github.com/mesonoxian1)

### License
MIT License, for more details see *LICENSE*.
