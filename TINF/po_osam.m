clear
for i=1:4
    if i==1 C=fileread('i1.txt');
    elseif i==2 C=fileread('i2.txt');
    elseif i==3 C=fileread('i3.txt');
    else C=fileread('i4.txt');
    end
        
    for j=1:2^8
        kombinacije(j)=0;
    end


    for j=1:8:length(C)
        for k=1:8
            if (C(j+k-1)=='x') polje(k)=0;
            else polje(k)=1*2^(8-k); 
            end
        end
    
        indeks=0;
        for k=1:8
            indeks=indeks+polje(k);
        end
    
        kombinacije(indeks+1)=kombinacije(indeks+1)+1;
    end

    for j=1:2^8
        if(kombinacije(j)~=0) 
            decimalToBinaryVector(j-1)
            kombinacije(j)
        end
    end

    entropija(i)=0;
    
    for j=1:2^8
        prob(i,j)=0;
        faktor=8*kombinacije(j)/length(C);
        if (kombinacije(j)~=0)
            entropija(i)=entropija(i)-faktor*log2(faktor);
            prob(i,j)=faktor;
        end
    end
end
entropija

prob_prvi=prob(1,:);
prob_prvi=prob_prvi(prob_prvi~=0);
prob_prvi=[NaN prob_prvi NaN];
prob_drugi=prob(2,:);
prob_drugi=prob_drugi(prob_drugi~=0);
prob_drugi=[NaN prob_drugi NaN];
prob_treci=prob(3,:);
prob_treci=prob_treci(prob_treci~=0);
prob_cetvrti=prob(4,:);
prob_cetvrti=prob_cetvrti(prob_cetvrti~=0);

subplot(2,2,1)
bar(prob_prvi)
xlabel('Izvoriste 1');
ylabel('Vjerojatnost');
set(gca,'XTickLabel',{''})
subplot(2,2,2)
bar(prob_drugi)
xlabel('Izvoriste 2');
ylabel('Vjerojatnost');
set(gca,'XTickLabel',{''})
subplot(2,2,3)
bar(prob_treci)
xlabel('Izvoriste 3');
ylabel('Vjerojatnost');
set(gca,'XTickLabel',{''})
subplot(2,2,4)
bar(prob_cetvrti)
xlabel('Izvoriste 4');
ylabel('Vjerojatnost');
set(gca,'XTickLabel',{''})

