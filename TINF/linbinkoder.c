#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

int g[8][16] = {
    { 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 1 },
    { 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1 },
    { 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 0 },
    { 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1 },
    { 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 1 },
    { 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 1, 0, 1, 0, 0 },
    { 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 0, 0, 1, 0 },
    { 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 1, 0, 0, 0 }
};

// Reports a error and exits.
int report_error(char *message);

// Codes the input file with the code.
int protect(FILE *input, FILE *output);

int main(int argc, char *argv[])
{
    //(argc == 3) || report_error("Insufficient arguments!");

    FILE *input_file = NULL;
    FILE *output_file = NULL;

    (input_file = fopen(argv[1], "rb")) || report_error("Can't open input file!");

    (output_file = fopen(argv[2], "wb")) || report_error("Can't open output file!");

    protect(input_file, output_file);

    return 0;
}

int report_error(char *message)
{
    fprintf(stderr, message);
    exit(-1);

    return 0;
}

int protect(FILE *input, FILE *output)
{
    while (!feof(input))
    {
        uint8_t byte = 0;
        fread(&byte, sizeof(uint8_t), 1, input);

        uint16_t coded = 0;
        for (int i = 0; i < 1; ++i)
        {
            for (int j = 0; j < 16; ++j)
            {
                uint16_t sum = 0;
                uint8_t mask = 0x80;
                for (int k = 0; k < 8; ++k, mask >>= 1)
                {
                    uint8_t bit = byte & mask;
                    sum += bit * g[k][j];
                }

                if (sum != 0) { sum = 1; }
                
                sum <<= (15 - j);
                coded |= sum;
            }
        }
        fwrite(&coded, sizeof(uint16_t), 1, output);
    }
    return 1;
}