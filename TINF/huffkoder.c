#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SYMBOL_COUNT 256

int needs_inverting = 0;

typedef struct
{
    uint8_t symbol;
    int freq;
} symbol_freq_pair;

typedef struct node
{
    int value;
    char *code;

    struct node *left;
    struct node *right;
} node;

typedef struct
{
    uint8_t symbol;
    char *code_word;
} symbol_code_pair;

// Reports a error and exits.
int report_error(char *message);

// Counts the number of symbols in the input file.
int count_symbols(FILE *input, int symbol_count[]);

// Constructs pairs of symbols and their frequencies.
int construct_pairs(int symbol_count[], symbol_freq_pair pairs[]);

// Sorts pairs as a preparation for coding.
int sort_pairs(symbol_freq_pair pairs[]);

// Sorts frequencies of symbols (first step of the coding process).
int sort_frequencies(int symbol_count[]);

// Sorts nodes during coding.
int sort_nodes(node nodes[], int size);

// Codes the symbols with the Hufffman method.
int create_code(int symbol_count[], char *code_words[SYMBOL_COUNT]);

// Parses a code tree.
int parse_tree(node *root, char *code_words[SYMBOL_COUNT]);

// Implementation of the function above.
int parse_tree_impl(node *root, char *code_words[SYMBOL_COUNT], char *up);

// Calculates the maximum depth of a tree.
int tree_depth(node *root);

// Inverts the code. Used when code word '0' is assigned as a code word.
// To resolve ambiguities during decoding as '0' is used as a padding on the end of the file.
int invert_code(char *code_words[SYMBOL_COUNT]);

// Joins symbols to code words in lexicographical order.
int join_symbols_to_codes(symbol_freq_pair *sf_pairs, char *code_words[SYMBOL_COUNT], symbol_code_pair *sc_pairs);

// Writes the code table in a textual file.
int write_code_table(symbol_code_pair *sc_pairs, FILE *code_table);

// Encodes the input file.
int code(FILE *input, symbol_code_pair *sc_pairs, FILE *output);

int main(int argc, char *argv[])
{
    int i = 0;
    //int symbol_count[SYMBOL_COUNT] = { 25, 1, 2, 0, 3, 4, 0, 7, 5, 3, 2, 11 };
    int symbol_count[SYMBOL_COUNT] = { 0 };
    symbol_freq_pair sf_pairs[SYMBOL_COUNT] = { 0 };
    symbol_code_pair sc_pairs[SYMBOL_COUNT] = { 0 };

    char **code_words = (char **) malloc(SYMBOL_COUNT * sizeof(char*));
    for (i = 0; i < SYMBOL_COUNT; ++i)
    {
        code_words[i] = calloc(SYMBOL_COUNT, sizeof(char));
    }

    FILE *input_file = NULL;
    FILE *code_table = NULL;
    FILE *output_file = NULL;

    argc == 4 || report_error("Insufficient arguments!");

    (input_file = fopen(argv[1], "rb")) || report_error("Can't open input file!");

    (code_table = fopen(argv[2], "w")) || report_error("Can't open code table file!");

    (output_file = fopen(argv[3], "w+b")) || report_error("Can't open output file!");

    count_symbols(input_file, symbol_count) || report_error("Read error!");

    construct_pairs(symbol_count, sf_pairs);

    sort_frequencies(symbol_count);

    sort_pairs(sf_pairs);

    create_code(symbol_count, code_words);

    needs_inverting && invert_code(code_words);

    join_symbols_to_codes(sf_pairs, code_words, sc_pairs);

    write_code_table(sc_pairs, code_table);

    code(input_file, sc_pairs, output_file);

    fclose(input_file);
    fclose(code_table);
    fclose(output_file);

    //sf_pairs and sc_pairs share the same memory with code_words array, so only this needs to be cleaned up
    for (i = 0; i < SYMBOL_COUNT; ++i)
    {
        free(code_words[i]);
    }
    free(code_words);

    return 0;
}

int report_error(char *message)
{
    fprintf(stderr, message);
    exit(-1);

    return 0;
}

int count_symbols(FILE *input, int symbol_count[])
{
    uint8_t symbol = '\0';
    while (!feof(input))
    {
        fread(&symbol, sizeof(symbol), 1, input);

        if (ferror(input)) { return 0; }

        ++symbol_count[symbol];
    }

    fseek(input, 0, SEEK_SET);
    return 1;
}

int construct_pairs(int symbol_count[], symbol_freq_pair pairs[])
{
    int i = 0;
    for (i = 0; i < SYMBOL_COUNT; ++i)
    {
        pairs[i].symbol = i;
        pairs[i].freq = symbol_count[i];
    }

    return 1;
}

int sort_pairs(symbol_freq_pair pairs[])
{
    int i = 0, change = 1;
    symbol_freq_pair temp;

    while (change)
    {
        change = 0;
        for (i = 1; i < SYMBOL_COUNT; i++)
        {
            if (pairs[i].freq > pairs[i - 1].freq)
            {
                temp = pairs[i];
                pairs[i] = pairs[i - 1];
                pairs[i - 1] = temp;

                change = 1;
            }
        }
    }

    return 1;
}

int sort_frequencies(int symbol_count[])
{
    int i = 0, change = 1;
    int temp;

    while (change)
    {
        change = 0;
        for (i = 1; i < SYMBOL_COUNT; i++)
        {
            if (symbol_count[i] > symbol_count[i - 1])
            {
                temp = symbol_count[i];
                symbol_count[i] = symbol_count[i - 1];
                symbol_count[i - 1] = temp;

                change = 1;
            }
        }
    }

    return 1;
}

int sort_nodes(node nodes[], int size)
{
    int i = 0, change = 1;
    node temp;

    while (change)
    {
        change = 0;
        for (i = 1; i < size; i++)
        {
            if (nodes[i].value > nodes[i - 1].value)
            {
                temp = nodes[i];
                nodes[i] = nodes[i - 1];
                nodes[i - 1] = temp;

                change = 1;
            }
        }
    }

    return 1;
}

int create_code(int symbol_count[], char *code_words[SYMBOL_COUNT])
{
    int i = 0;
    node temp;

    int active_symbols = SYMBOL_COUNT;
    int depth_count = 0; // Counts the number of assigned depths in the tree (for later cleanup).
    node **tree_list = (node **) malloc(SYMBOL_COUNT * sizeof(node *)); // Worst case scenario

    // Create initial row
    node *current_list = (node *) malloc(SYMBOL_COUNT * sizeof(node));
    for (i = 0; i < SYMBOL_COUNT; ++i)
    {
        current_list[i].value = symbol_count[i];
        current_list[i].code = code_words[i];
        current_list[i].left = current_list[i].right = NULL;
    }
    tree_list[depth_count++] = current_list;

    // Create a tree
    --active_symbols;
    while (active_symbols != 0)
    {
        current_list = (node *) malloc(active_symbols * sizeof(node));
        for (i = 0; i < active_symbols - 1; ++i)
        {
            current_list[i] = tree_list[depth_count - 1][i];
        }

        temp.value = tree_list[depth_count - 1][active_symbols - 1].value + tree_list[depth_count - 1][active_symbols].value;
        temp.right =
            tree_list[depth_count - 1][active_symbols - 1].value >= tree_list[depth_count - 1][active_symbols].value
            ? &tree_list[depth_count - 1][active_symbols - 1] : &tree_list[depth_count - 1][active_symbols];

        temp.left = tree_list[depth_count - 1][active_symbols - 1].value < tree_list[depth_count - 1][active_symbols].value
            ? &tree_list[depth_count - 1][active_symbols - 1] : &tree_list[depth_count - 1][active_symbols];

        current_list[active_symbols - 1] = temp;

        tree_list[depth_count++] = current_list;

        sort_nodes(current_list, active_symbols);

        --active_symbols;
    }

    parse_tree(&tree_list[depth_count - 1][0], code_words);

    //Memory cleanup
    for (i = 0; i < depth_count; ++i)
    {
        free(tree_list[i]);
    }
    free(tree_list);

    return 1;
}

int parse_tree(node *root, char *code_words[SYMBOL_COUNT])
{
    return parse_tree_impl(root->right, code_words, "1") && parse_tree_impl(root->left, code_words, "0");
}

int parse_tree_impl(node *root, char *code_words[SYMBOL_COUNT], char *up)
{
    char *current = strcpy((char *) calloc(1, (strlen(up) + 2)), up);

    if (root->right == NULL)
    {
        if (strcmp("0", up) == 0) needs_inverting = 1;

        strcpy(root->code, up);
        return 1;
    }

    if (tree_depth(root->left) >= tree_depth(root->right))
    {
        current[strlen(up)] = '1';
        parse_tree_impl(root->right, code_words, current);
        current[strlen(up)] = '0';
        parse_tree_impl(root->left, code_words, current);
    }
    else
    {
        current[strlen(up)] = '0';
        parse_tree_impl(root->left, code_words, current);
        current[strlen(up)] = '1';
        parse_tree_impl(root->right, code_words, current);
    }

    free(current);

    return 1;
}

int tree_depth(node *root)
{
    int left = 0;
    int right = 0;

    if (root == NULL) return 0;

    left = tree_depth(root->left);
    right = tree_depth(root->right);

    return (left > right) ? left + 1 : right + 1;
}

int invert_code(char *code_words[SYMBOL_COUNT])
{
    int i = 0;
    int j = 0;
    for (i = 0; i < SYMBOL_COUNT; ++i)
    {
        for (j = 0; j < strlen(code_words[i]); ++j)
        {
            code_words[i][j] = ((code_words[i][j] == '0') ? '1' : '0');
        }
    }

    return 1;
}

int join_symbols_to_codes(symbol_freq_pair *sf_pairs, char *code_words[SYMBOL_COUNT], symbol_code_pair *sc_pairs)
{
    int i = 0;
    int j = 0;
    for (i = 0; i < SYMBOL_COUNT; ++i)
    {
        j = 0;
        while (sf_pairs[j].symbol != i)
        {
            ++j;
        }

        sc_pairs[i].symbol = i;
        sc_pairs[i].code_word = code_words[j];
    }

    return 1;
}

int write_code_table(symbol_code_pair *sc_pairs, FILE *code_table)
{
    int i = 0;
    for (i = 0; i < SYMBOL_COUNT; ++i)
    {
        fprintf(code_table, "%s\n", sc_pairs[i].code_word);
    }

    return 1;
}

int code(FILE *input, symbol_code_pair *sc_pairs, FILE *output)
{
    uint8_t zero = 0;

    int i = 0;
    int filled_bytes = 0;
    uint8_t symbol = 0;
    int offset = 7; //Number of symbols free in the last byte written.
    char *code_word;
    uint8_t byte = 0;
    uint8_t bit = 0;

    //Write initial byte to the output file
    if (!feof(input))
    {
        fwrite(&zero, sizeof(zero), 1, output);
        fseek(output, 0, SEEK_SET);
    }

    while (!feof(input))
    {
        fread(&symbol, sizeof(symbol), 1, input);

        if (ferror(input)) { return 0; }

        code_word = sc_pairs[symbol].code_word;

        // Extremely inefficient method writing bit by bit
        for (i = 0; i < strlen(code_word); ++i)
        {
            byte = 0;
            fread(&byte, sizeof(byte), 1, output);
            fseek(output, filled_bytes, SEEK_SET);

            bit = (code_word[i] & 1) << offset;
            byte |= bit;
            --offset;

            fwrite(&byte, sizeof(byte), 1, output);

            if (offset == -1)
            {
                offset = 7;
                ++filled_bytes;
                fwrite(&zero, sizeof(zero), 1, output);
            }
            fseek(output, filled_bytes, SEEK_SET);
        }
    }

    return 1;
}