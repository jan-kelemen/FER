#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#define SYMBOL_COUNT 256

#define TRUE 1
#define FALSE 0

typedef struct
{
    uint8_t *word;
    size_t length;
} code_word_t;

typedef struct node_t
{
    int id;

    code_word_t *code_word;

    struct node_t *children[SYMBOL_COUNT];
} node_t;

node_t trie;

// Reports a error and exits.
int report_error(char *message);

// Fills the dict with words [0, ... 255]
int fill_starting_dict();

// Inserts the word to the dict
int insert(uint8_t *word, size_t length);

// Checks if the word exists in the trie.
int exists(uint8_t *word, size_t length);

// Returns the id of the node with the specified word.
uint16_t get_id(uint8_t *word, size_t length);

// Codes the input file with the LZW algorithm
int code(FILE *input, FILE *output);

// Clears the memory of the trie
int trie_cleanup();

int main(int argc, char *argv[])
{
    int i = 0;

    FILE *input_file = NULL;
    FILE *output_file = NULL;

    argc == 3 || report_error("Insufficient arguments!");

    (input_file = fopen(argv[1], "rb")) || report_error("Can't open input file!");
    (output_file = fopen(argv[2], "wb")) || report_error("Can't open output file!");

    trie.id = -1;
    trie.code_word = NULL;
    for (i = 0; i < SYMBOL_COUNT; ++i)
    {
        trie.children[i] = NULL;
    }

    fill_starting_dict();

    code(input_file, output_file);

    trie_cleanup();
    fclose(input_file);
    fclose(output_file);

    return 0;
}

int report_error(char *message)
{
    fprintf(stderr, message);
    exit(-1);

    return 0;
}

int fill_starting_dict()
{
    uint8_t symbol = 0;
    int i = 0;

    for (i = 0; i < SYMBOL_COUNT; ++i, ++symbol)
    {
        insert(&symbol, 1);
    }

    return TRUE;
}

int insert(uint8_t *word, size_t length)
{
    static int id = 0;

    int i = 0;

    node_t *current_node = &trie;
    node_t *new_node = (node_t *) malloc(sizeof(node_t));

    code_word_t *code_word = (code_word_t *) malloc(sizeof(code_word));
    code_word->word = (uint8_t *) malloc(length * sizeof(uint8_t));
    memcpy(code_word->word, word, length * sizeof(uint8_t));
    code_word->length = length;

    new_node->id = id++;
    new_node->code_word = code_word;
    for (i = 0; i < SYMBOL_COUNT; ++i)
    {
        new_node->children[i] = NULL;
    }

    for (i = 0; i < length - 1; ++i)
    {
        current_node = current_node->children[word[i]];
    }

    current_node->children[word[i]] = new_node;

    return TRUE;
}

int exists(uint8_t *word, size_t length)
{
    node_t *current_node = &trie;
    int i = 0;
    for (i = 0; i < length; ++i)
    {
        current_node = current_node->children[word[i]];
        if (current_node == NULL)
        {
            return FALSE;
        }
    }

    return TRUE;
}

uint16_t get_id(uint8_t *word, size_t length)
{
    node_t *current_node = &trie;
    int i = 0;
    for (i = 0; i < length; ++i)
    {
        current_node = current_node->children[word[i]];
    }

    return current_node->id;
}

int code(FILE *input, FILE *output)
{
    size_t length = 1;

    uint16_t out = 0;

    uint8_t *new_word = NULL;

    uint8_t new_symbol = 0;
    uint8_t *current_word = (uint8_t *) malloc(sizeof(uint8_t));

    fread(current_word, sizeof(uint8_t), 1, input);
    while (!feof(input))
    {
        fread(&new_symbol, sizeof(uint8_t), 1, input);
        new_word = (uint8_t *) malloc((length + 1) * sizeof(uint8_t));
        memcpy(new_word, current_word, length * sizeof(uint8_t));
        new_word[length] = new_symbol;

        if (exists(new_word, length + 1))
        {
            free(current_word);
            current_word = new_word;
            ++length;
        }
        else
        {
            out = get_id(current_word, length);
            fwrite(&out, sizeof(uint16_t), 1, output);

            insert(new_word, length + 1);

            free(current_word);
            current_word = (uint8_t *) malloc(sizeof(uint8_t));
            current_word[0] = new_symbol;
            length = 1;
        }
    }

    out = get_id(current_word, length);
    fwrite(&out, sizeof(uint16_t), 1, output);

    return TRUE;
}

int free_node(node_t *node)
{
    if (node == NULL)
    {
        return FALSE;
    }

    int i = 0;
    free(node->code_word->word);
    //free(node->code_word);
    for (i = 0; i < SYMBOL_COUNT; ++i)
        free_node(node->children[i]);

    free(node);

    return TRUE;
}

int trie_cleanup()
{
    int i = 0;
    for (i = 0; i < SYMBOL_COUNT; ++i)
    {
        free_node(trie.children[i]);
    }

    return TRUE;
}