#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

int h_t[16][8] = {
    { 0, 1, 0, 0, 1, 1, 0, 1 },
    { 0, 0, 0, 1, 0, 1, 1, 1 },
    { 0, 0, 1, 0, 1, 1, 1, 0 },
    { 1, 0, 0, 0, 1, 0, 1, 1 },
    { 1, 1, 1, 0, 0, 0, 0, 1 },
    { 1, 1, 0, 1, 0, 1, 0, 0 },
    { 0, 1, 1, 1, 0, 0, 1, 0 },
    { 1, 0, 1, 1, 1, 0, 0, 0 },
    { 1, 0, 0, 0, 0, 0, 0, 0 },
    { 0, 1, 0, 0, 0, 0, 0, 0 },
    { 0, 0, 1, 0, 0, 0, 0, 0 },
    { 0, 0, 0, 1, 0, 0, 0, 0 },
    { 0, 0, 0, 0, 1, 0, 0, 0 },
    { 0, 0, 0, 0, 0, 1, 0, 0 },
    { 0, 0, 0, 0, 0, 0, 1, 0 },
    { 0, 0, 0, 0, 0, 0, 0, 1 }
};

// Reports a error and exits.
int report_error(char *message);

// Decodes the input file.
int decode(FILE *input, FILE *output);

int main(int argc, char *argv[])
{
    (argc == 3) || report_error("Insufficient arguments!");

    FILE *input_file = NULL;
    FILE *output_file = NULL;

    (input_file = fopen(argv[1], "rb")) || report_error("Can't open input file!");

    (output_file = fopen(argv[2], "wb")) || report_error("Can't open output file!");

    decode(input_file, output_file);

    return 0;
}

int report_error(char *message)
{
    fprintf(stderr, message);
    exit(-1);

    return 0;
}

int decode(FILE *input, FILE *output)
{
    while (!feof(input))
    {
        uint16_t coded = 0;
        fread(&coded, sizeof(uint16_t), 1, input);

        uint8_t syndrome = 0;
        for (int j = 0; j < 8; ++j)
        {
            uint8_t flag = 0;
            for (int k = 0; k < 16; ++k)
            {
                flag = (flag ? 0 : 1);
            }

            if (flag != 0)
            {
                syndrome |= (0x1 << (7 - j));
            }
        }
        if (syndrome >= 1 && syndrome <= 16)
        {
            uint32_t temp = 0x1 << syndrome;
            uint16_t mask = temp >> 1;
            coded ^= mask;
        }
        uint8_t out = (coded & 0xff00) >> 8;
        fwrite(&out, sizeof(uint8_t), 1, output);
    }
    return 1;
}