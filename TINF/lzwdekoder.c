#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#define SYMBOL_COUNT 256

#define TRUE 1
#define FALSE 0

typedef struct
{
    uint8_t *word;
    size_t length;
} code_word_t;

typedef struct
{
    code_word_t code_words[UINT16_MAX];
    size_t size;
} dict_t;

dict_t dict;

// Reports a error and exits.
int report_error(char *message);

// Fills the dict with words [0, ... 255]
int fill_starting_dict();

// Inserts the word to the dict
int insert(uint8_t *word, size_t length);

int exists(uint16_t id);

// Returns the code word with the id.
code_word_t get_code_word(uint16_t id);

// Decodes the input file with the LZW algorithm
int decode(FILE *input, FILE *output);

int main(int argc, char *argv[])
{
    int i = 0;

    FILE *input_file = NULL;
    FILE *output_file = NULL;

    argc == 3 || report_error("Insufficient arguments!");

    (input_file = fopen(argv[1], "rb")) || report_error("Can't open input file!");
    (output_file = fopen(argv[2], "wb")) || report_error("Can't open output file!");

    dict.size = 0;

    fill_starting_dict();

    decode(input_file, output_file);

    for (int i = 0; i < dict.size; ++i)
    {
        free(get_code_word(i).word);
    }

    fclose(input_file);
    fclose(output_file);

    return 0;
}

int report_error(char *message)
{
    fprintf(stderr, message);
    exit(-1);

    return 0;
}

int fill_starting_dict()
{
    uint8_t symbol = 0;
    int i = 0;

    for (i = 0; i < SYMBOL_COUNT; ++i, ++symbol)
    {
        insert(&symbol, 1);
    }

    return TRUE;
}

int insert(uint8_t *word, size_t length)
{
    code_word_t code_word;
    code_word.word = (uint8_t *) malloc(length * sizeof(uint8_t));
    memcpy(code_word.word, word, length * sizeof(uint8_t));
    code_word.length = length;

    dict.code_words[dict.size++] = code_word;

    return TRUE;
}

int insert_with_suffix(uint8_t *word, uint8_t suffix, size_t length)
{
    uint8_t *temp = (uint8_t *) malloc(sizeof(uint8_t) * length);
    memcpy(temp, word, length - 1);
    temp[length - 1] = suffix;
    insert(temp, length);
    free(temp);
    return TRUE;
}

int exists(uint16_t id)
{
    return id < dict.size;
}

code_word_t get_code_word(uint16_t id)
{
    return dict.code_words[id];
}

int decode(FILE *input, FILE *output)
{
    //read a character k;
    //output k;
    //w = k;
    //while (read a character k)
    ///* k could be a character or a code. */
    //{
    //    if k exists in the dictionary
    //        entry = dictionary entry for k;
    //        output entry;
    //        add w + entry[0] to dictionary;
    //        w = entry;
    //    else
    //        output entry = w + firstCharacterOf(w);
    //        add entry to dictionary;
    //        w = entry;
    //}
    uint16_t k;
    fread(&k, sizeof(uint16_t), 1, input);
    fwrite(&k, sizeof(uint8_t), 1, output);
    uint8_t *word = (uint8_t *) malloc(sizeof(uint8_t));
    word[0] = k;
    int length = 1;
    while (!feof(input))
    {
        fread(&k, sizeof(uint16_t), 1, input);
        if (exists(k))
        {
            code_word_t entry = get_code_word(k);
            fwrite(entry.word, sizeof(uint8_t), entry.length, output);
            insert_with_suffix(word, entry.word[0], length + 1);

            free(word);
            length = entry.length;
            word = (uint8_t *) malloc(sizeof(uint8_t) * length);
            memcpy(word, entry.word, sizeof(uint8_t) * length);
        }
        else
        {
            uint8_t *new = (uint8_t *) malloc(sizeof(uint8_t) * (length + 1));
            memcpy(new, word, sizeof(uint8_t) * length);
            new[length] = word[0];
            fwrite(new, sizeof(uint8_t), length, output);
            insert(new, length);
            free(word);
            word = new;
        }
    }
    free(word);

    return TRUE;
}