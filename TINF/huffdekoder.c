#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SYMBOL_COUNT 256

typedef struct
{
    uint8_t symbol;
    char *code_word;
} symbol_code_pair;

// Reports a error and exits.
int report_error(char *message);

// Reads the code words from the code table.
int read_code_table(FILE *code_table, symbol_code_pair *sc_pairs);

// Decodes the input file using specified symbol-code pairs.
int decode(FILE *input, symbol_code_pair *sc_pairs, FILE *output);

int main(int argc, char *argv[])
{
    int i = 0;

    symbol_code_pair sc_pairs[SYMBOL_COUNT] = { 0 };

    FILE *code_table = NULL;
    FILE *input_file = NULL;
    FILE *output_file = NULL;

    argc == 4 || report_error("Insufficient arguments!");

    (code_table = fopen(argv[1], "r")) || report_error("Can't open code table file!");

    (input_file = fopen(argv[2], "rb")) || report_error("Can't open input file!");

    (output_file = fopen(argv[3], "wb")) || report_error("Can't open output file!");

    read_code_table(code_table, sc_pairs);

    decode(input_file, sc_pairs, output_file);

    fclose(code_table);
    fclose(input_file);
    fclose(output_file);

    for (i = 0; i < SYMBOL_COUNT; ++i)
    {
        free(sc_pairs[i].code_word);
    }
    return 0;
}

int report_error(char *message)
{
    fprintf(stderr, message);
    exit(-1);

    return 0;
}

int read_code_table(FILE *code_table, symbol_code_pair *sc_pairs)
{
    int i = 0;
    for (i = 0; i < SYMBOL_COUNT; ++i)
    {
        sc_pairs[i].symbol = i;
        // Longest code word is SYMBOL_COUNT - 1 symbols, so this accounts in the null terminator too.
        sc_pairs[i].code_word = (char *) calloc(1, SYMBOL_COUNT);
        fgets(sc_pairs[i].code_word, SYMBOL_COUNT, code_table);
        *strchr(sc_pairs[i].code_word, '\n') = '\0'; //Terminate the string.
    }

    return 1;
}

int prefix(char *prefix, char *code_word)
{
    return strncmp(prefix, code_word, strlen(prefix)) == 0;
}

int decode(FILE *input, symbol_code_pair *sc_pairs, FILE *output)
{
    int i = 0;
    int cmp = 0;

    symbol_code_pair current_selection[SYMBOL_COUNT] = { 0 };
    int active_symbols = SYMBOL_COUNT;

    char current_prefix[SYMBOL_COUNT] = { 0 };
    char read_bit[2] = { 0 }; //Array is used for safe str* operations

    uint8_t mask = 0x80;
    uint8_t byte = 0;

    memcpy(current_selection, sc_pairs, SYMBOL_COUNT * sizeof(symbol_code_pair));

    while (!feof(input))
    {
        mask = 0x80;
        fread(&byte, sizeof(byte), 1, input);

    unused:

        read_bit[0] = (byte & mask) ? '1' : '0';
        strcat(current_prefix, read_bit);

        for (i = 0; i < active_symbols;)
        {
            if (prefix(current_prefix, current_selection[i].code_word) == 0)
            {
                --active_symbols;

                if (active_symbols == 0)
                {
                    return 1;
                }

                memmove(current_selection + i, current_selection + i + 1, (SYMBOL_COUNT - i) * sizeof(symbol_code_pair));

                continue;
            }
            cmp = strcmp(current_prefix, current_selection[i].code_word);

            if (cmp < 0)
            {
                ++i;
                continue;
            }
            else if (cmp == 0)
            {
                fwrite(&current_selection[i].symbol, sizeof(uint8_t), 1, output);
                memset(current_prefix, 0, SYMBOL_COUNT * sizeof(char));
                memcpy(current_selection, sc_pairs, SYMBOL_COUNT * sizeof(symbol_code_pair));
                active_symbols = SYMBOL_COUNT;
                break;
            }
        }

        mask >>= 1;
        if (mask != 0)
        {
            goto unused;
        }
    }

    return 1;
}