# SKRJ - Skriptni jezici (Scripting Languages)
This folder contains solutions to lab tasks of Scripting Languages course.
Text of the lab tasks is in the .pdf files of subfolders.

## Content
* LAB-01 - Bash
* LAB-02 - Perl
* LAB-03 - Python
