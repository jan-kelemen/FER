﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SeaBattle.Models;

namespace SeaBattle.Models
{
    class BoardPosition
    {
        public BoardPosition(int x, int y)
        {
            X = x;
            Y = y;
        }

        public int X { get; }

        public int Y { get; }

        public override bool Equals(object obj)
        {
            var position = obj as BoardPosition;

            if (position == null) { return false; }

            return this == position;
        }

        public override int GetHashCode()
        {
            int hash = 13;
            hash = (hash * 7) * X.GetHashCode();
            hash = (hash * 7) * Y.GetHashCode();
            return hash;
        }

        public static bool operator ==(BoardPosition lhs, BoardPosition rhs)
        {
            if (ReferenceEquals(lhs, rhs)) { return true; }

            if (((object)lhs == null) || ((object)rhs == null)) { return false; }

            return lhs.X == rhs.X && lhs.Y == rhs.Y;
        }

        public static bool operator !=(BoardPosition lhs, BoardPosition rhs) => !(lhs == rhs);
    }

    class BoardHalf
    {
        List<ShipWrapper> shipWrappers;

        public BoardHalf()
        {
            shipWrappers = new ShipPlacer().Place(FleetFactory.Create());
        }

        public List<BoardPosition> TakenPositions
        {
            get
            {
                var taken = new List<BoardPosition>();
                foreach (var wrapper in shipWrappers)
                {
                    taken.AddRange(wrapper.Positions);
                }
                return taken;
            }
        }

        public bool HasLiveShips
        {
            get
            {
                foreach (var wrapper in shipWrappers)
                {
                    if (!wrapper.Ship.IsSunk)
                    {
                        return true;
                    }
                }

                return false;
            }
        }

        public Tuple<bool, bool, Ship> Hit(BoardPosition position)
        {
            return Hit(position.X, position.Y);
        }

        public Tuple<bool, bool, Ship> Hit(int x, int y)
        {
            var position = new BoardPosition(x, y);

            foreach (var wrapper in shipWrappers)
            {
                if(wrapper.Ship.IsSunk) { continue; }

                if (!wrapper.Positions.Contains(position)) { continue; }

                return new Tuple<bool, bool, Ship>(true, wrapper.Hit(position), wrapper.Ship);
            }

            return new Tuple<bool, bool, Ship>(false, false, null);
        }
    }

    class Board
    {
        public Board()
        {
            AiBoard = new BoardHalf();
            HpBoard = new BoardHalf();
        }

        public BoardHalf AiBoard { get; }
        public BoardHalf HpBoard { get; }
    }
}
