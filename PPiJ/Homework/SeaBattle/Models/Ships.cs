﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace SeaBattle.Models
{
    class Ship
    {
        bool[] parts;

        public Ship(string name, int size)
        {
            Name = name;
            Size = size;

            parts = new bool[size];
        }

        public bool IsSunk
        {
            get
            {
                foreach (var part in parts)
                    if (!part)
                        return false;

                return true;
            }
        }

        public string Name { get; }

        public int Size { get; }

        public void Hit(int index)
        {
            parts[index] = true;
        }
    }

    static class FleetFactory
    {
        public static List<Ship> Create()
        {
            return new List<Ship>()
            {
                new Ship("Borbeni brod 1", 4),
                new Ship("Krstarica 1", 3),
                new Ship("Krstarica 2", 3),
                new Ship("Razarač 1", 2),
                new Ship("Razarač 2", 2),
                new Ship("Razarač 3", 2),
                new Ship("Podmornica 1", 1),
                new Ship("Podmornica 2", 1),
                new Ship("Podmornica 3", 1),
                new Ship("Podmornica 4", 1)
            };
        }
    }

    class ShipWrapper
    {
        public ShipWrapper(Ship ship, List<BoardPosition> positions)
        {
            Ship = ship;
            Positions = positions;
        }

        public Ship Ship { get; }
        public List<BoardPosition> Positions { get; }

        public bool Hit(BoardPosition position)
        {
            Ship.Hit(Positions.IndexOf(position));

            return Ship.IsSunk;
        }
    }

    class ShipPlacer
    {
        enum Orientation { Left, Right, Up, Down }

        static Random generator = new Random();

        public List<ShipWrapper> Place(List<Ship> ships)
        {
            var takenIndices = new List<BoardPosition>();
            var shipWrappers = new List<ShipWrapper>();

            foreach (var ship in ships)
            {
                var shipIndices = new List<BoardPosition>();

                var shipPlaced = false;
                while (!shipPlaced)
                {
                    var initialIndex = generateInitialIndex();
                    if (takenIndices.Contains(initialIndex)) { continue; }

                    foreach (var orientation in shuffleOrientations())
                    {
                        var indices = generateIndices(ship, initialIndex, orientation);
                        if (indices == null) continue;

                        var allFree = true;
                        foreach (var index in indices)
                        {
                            if (takenIndices.Contains(index))
                            {
                                allFree = false;
                                break;
                            }
                        }

                        if (allFree)
                        {
                            shipWrappers.Add(new ShipWrapper(ship, indices));
                            takenIndices.AddRange(indices);
                            shipPlaced = true;
                            break;
                        }
                    }
                }
            }

            return shipWrappers;
        }

        BoardPosition generateInitialIndex()
        {
            return new BoardPosition(generator.Next(10), generator.Next(10));
        }

        ICollection<Orientation> shuffleOrientations()
        {
            var orientations = new List<Orientation>();
            foreach (var orientation in Enum.GetValues(typeof(Orientation)))
            {
                orientations.Add((Orientation)orientation);
            }

            var shuffledOrientations = new List<Orientation>();
            while (orientations.Count > 0)
            {
                var index = generator.Next(0, orientations.Count);
                shuffledOrientations.Add(orientations[index]);
                orientations.RemoveAt(index);
            }

            return shuffledOrientations;
        }

        List<BoardPosition> generateIndices(Ship ship, BoardPosition initial, Orientation orientation)
        {
            var indices = new List<BoardPosition>();
            for (var i = 0; i < ship.Size; i++)
            {
                var generated = generateIndex(initial, i, orientation);

                if (generated == null) return null;

                indices.Add(generated);
            }
            return indices;
        }

        BoardPosition generateIndex(BoardPosition initial, int offset, Orientation orientation)
        {
            var new_index = 0;
            switch (orientation)
            {
                case Orientation.Left:
                    new_index = initial.X - offset;
                    if (validIndex(new_index)) return new BoardPosition(new_index, initial.Y);
                    break;
                case Orientation.Right:
                    new_index = initial.X + offset;
                    if (validIndex(new_index)) return new BoardPosition(new_index, initial.Y);
                    break;
                case Orientation.Up:
                    new_index = initial.Y - offset;
                    if (validIndex(new_index)) return new BoardPosition(initial.X, new_index);
                    break;
                case Orientation.Down:
                    new_index = initial.Y+ offset;
                    if (validIndex(new_index)) return new BoardPosition(initial.X, new_index);
                    break;
            }
            return null;
        }

        bool validIndex(int index)
        {
            return index >= 0 && index <= 9;
        }
    }
}
