﻿using SeaBattle.Models;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace SeaBattle
{
    public partial class GameForm : Form
    {
        private class Ai
        {
            Random gen;
            List<BoardPosition> played;

            public Ai()
            {
                gen = new Random();
                played = new List<BoardPosition>();
            }

            public BoardPosition NextMove
            {
                get
                {
                    BoardPosition value = new BoardPosition(generateIndex(), generateIndex());
                    while(played.Contains(value))
                    {
                        value = new BoardPosition(generateIndex(), generateIndex());
                    }
                    played.Add(value);

                    return value;
                }
            }

            private int generateIndex() => gen.Next(10);
        }

        Board board;
        Ai ai;
        int ticks = 0;

        public GameForm()
        {
            InitializeComponent();
            board = new Board();
            ai = new Ai();
        }

        private void gameForm_Load(object sender, EventArgs e)
        {
            var taken = board.HpBoard.TakenPositions;
            foreach(var place in taken)
            {
                foreach(var control in Controls)
                {
                    if(control is Button)
                    {
                        var button = control as Button;
                        if(button.Name == hp_btn_Name(place.X, place.Y))
                        {
                            button.BackColor = System.Drawing.Color.Gray;
                        }
                    }
                }
            }
        }

        string hp_btn_Name(int x, int y) => $"hp_{x}_{y}";

        string ai_btn_Name(int x, int y) => $"ai_{x}_{y}";

        private void tick(object sender, EventArgs e)
        {
            ticks++;

            Button button = sender as Button;
            if (handleHit(button))
            {
                gameEnd("korisnik");
            };

            if(handleHit(hpButton(ai.NextMove)))
            {
                gameEnd("računalo");
            }
        }

        bool handleHit(Button button)
        {
            var parsed = btn_ParseName(button.Name);

            var boardHalf = parsed.Item1 == "ai" ? board.AiBoard : board.HpBoard;
            var hitResult = boardHalf.Hit(parsed.Item2);

            if (hitResult.Item1)
            {
                button.BackColor = System.Drawing.Color.Red;

                if (hitResult.Item2)
                {
                    MessageBox.Show($"Potopljen brod: {hitResult.Item3.Name}");
                }
            }

            button.Text = "X";
            button.Enabled = false;

            return !boardHalf.HasLiveShips;
        }

        Button hpButton(BoardPosition position)
        {
            foreach (var control in Controls)
            {
                if (control is Button)
                {
                    var button = control as Button;
                    if (button.Name == hp_btn_Name(position.X, position.Y))
                    {
                        return button;
                    }
                }
            }

            return null;
        }

        Tuple<string, BoardPosition> btn_ParseName(string name)
        {
            var reg = new Regex(@"(\w{2})_(\d)_(\d)");
            var match = reg.Match(name);

            var player = match.Groups[1].Value;
            var x = Convert.ToInt32(match.Groups[2].Value);
            var y = Convert.ToInt32(match.Groups[3].Value);

            return new Tuple<string, BoardPosition>(player, new BoardPosition(x, y));
        }

        void gameEnd(string player)
        {
            MessageBox.Show($"Pobjednik je {player}, broj pokušaja: {ticks}");
            this.Hide();
            var newGame = new GameForm();
            newGame.Closed += (s, args) => this.Close();
            newGame.Show();
        }
    }
}
