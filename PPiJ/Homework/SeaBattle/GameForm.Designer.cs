﻿namespace SeaBattle
{
    partial class GameForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ai_0_0 = new System.Windows.Forms.Button();
            this.ai_0_1 = new System.Windows.Forms.Button();
            this.ai_0_2 = new System.Windows.Forms.Button();
            this.ai_0_3 = new System.Windows.Forms.Button();
            this.ai_0_4 = new System.Windows.Forms.Button();
            this.ai_0_9 = new System.Windows.Forms.Button();
            this.ai_0_8 = new System.Windows.Forms.Button();
            this.ai_0_7 = new System.Windows.Forms.Button();
            this.ai_0_6 = new System.Windows.Forms.Button();
            this.ai_0_5 = new System.Windows.Forms.Button();
            this.ai_2_9 = new System.Windows.Forms.Button();
            this.ai_2_8 = new System.Windows.Forms.Button();
            this.ai_2_7 = new System.Windows.Forms.Button();
            this.ai_2_6 = new System.Windows.Forms.Button();
            this.ai_2_5 = new System.Windows.Forms.Button();
            this.ai_2_4 = new System.Windows.Forms.Button();
            this.ai_2_3 = new System.Windows.Forms.Button();
            this.ai_2_2 = new System.Windows.Forms.Button();
            this.ai_2_1 = new System.Windows.Forms.Button();
            this.ai_2_0 = new System.Windows.Forms.Button();
            this.ai_3_9 = new System.Windows.Forms.Button();
            this.ai_3_8 = new System.Windows.Forms.Button();
            this.ai_3_7 = new System.Windows.Forms.Button();
            this.ai_3_6 = new System.Windows.Forms.Button();
            this.ai_3_5 = new System.Windows.Forms.Button();
            this.ai_3_4 = new System.Windows.Forms.Button();
            this.ai_3_3 = new System.Windows.Forms.Button();
            this.ai_3_2 = new System.Windows.Forms.Button();
            this.ai_3_1 = new System.Windows.Forms.Button();
            this.ai_3_0 = new System.Windows.Forms.Button();
            this.ai_4_9 = new System.Windows.Forms.Button();
            this.ai_4_8 = new System.Windows.Forms.Button();
            this.ai_4_7 = new System.Windows.Forms.Button();
            this.ai_4_6 = new System.Windows.Forms.Button();
            this.ai_4_5 = new System.Windows.Forms.Button();
            this.ai_4_4 = new System.Windows.Forms.Button();
            this.ai_4_3 = new System.Windows.Forms.Button();
            this.ai_4_2 = new System.Windows.Forms.Button();
            this.ai_4_1 = new System.Windows.Forms.Button();
            this.ai_4_0 = new System.Windows.Forms.Button();
            this.ai_5_9 = new System.Windows.Forms.Button();
            this.ai_5_8 = new System.Windows.Forms.Button();
            this.ai_5_7 = new System.Windows.Forms.Button();
            this.ai_5_6 = new System.Windows.Forms.Button();
            this.ai_5_5 = new System.Windows.Forms.Button();
            this.ai_5_4 = new System.Windows.Forms.Button();
            this.ai_5_3 = new System.Windows.Forms.Button();
            this.ai_5_2 = new System.Windows.Forms.Button();
            this.ai_5_1 = new System.Windows.Forms.Button();
            this.ai_5_0 = new System.Windows.Forms.Button();
            this.ai_6_9 = new System.Windows.Forms.Button();
            this.ai_6_8 = new System.Windows.Forms.Button();
            this.ai_6_7 = new System.Windows.Forms.Button();
            this.ai_6_6 = new System.Windows.Forms.Button();
            this.ai_6_5 = new System.Windows.Forms.Button();
            this.ai_6_4 = new System.Windows.Forms.Button();
            this.ai_6_3 = new System.Windows.Forms.Button();
            this.ai_6_2 = new System.Windows.Forms.Button();
            this.ai_6_1 = new System.Windows.Forms.Button();
            this.ai_6_0 = new System.Windows.Forms.Button();
            this.ai_7_9 = new System.Windows.Forms.Button();
            this.ai_7_8 = new System.Windows.Forms.Button();
            this.ai_7_7 = new System.Windows.Forms.Button();
            this.ai_7_6 = new System.Windows.Forms.Button();
            this.ai_7_5 = new System.Windows.Forms.Button();
            this.ai_7_4 = new System.Windows.Forms.Button();
            this.ai_7_3 = new System.Windows.Forms.Button();
            this.ai_7_2 = new System.Windows.Forms.Button();
            this.ai_7_1 = new System.Windows.Forms.Button();
            this.ai_7_0 = new System.Windows.Forms.Button();
            this.ai_8_9 = new System.Windows.Forms.Button();
            this.ai_8_8 = new System.Windows.Forms.Button();
            this.ai_8_7 = new System.Windows.Forms.Button();
            this.ai_8_6 = new System.Windows.Forms.Button();
            this.ai_8_5 = new System.Windows.Forms.Button();
            this.ai_8_4 = new System.Windows.Forms.Button();
            this.ai_8_3 = new System.Windows.Forms.Button();
            this.ai_8_2 = new System.Windows.Forms.Button();
            this.ai_8_1 = new System.Windows.Forms.Button();
            this.ai_8_0 = new System.Windows.Forms.Button();
            this.ai_9_9 = new System.Windows.Forms.Button();
            this.ai_9_8 = new System.Windows.Forms.Button();
            this.ai_9_7 = new System.Windows.Forms.Button();
            this.ai_9_6 = new System.Windows.Forms.Button();
            this.ai_9_5 = new System.Windows.Forms.Button();
            this.ai_9_4 = new System.Windows.Forms.Button();
            this.ai_9_3 = new System.Windows.Forms.Button();
            this.ai_9_2 = new System.Windows.Forms.Button();
            this.ai_9_1 = new System.Windows.Forms.Button();
            this.ai_9_0 = new System.Windows.Forms.Button();
            this.ai_label = new System.Windows.Forms.Label();
            this.hp_label = new System.Windows.Forms.Label();
            this.ai_1_0 = new System.Windows.Forms.Button();
            this.ai_1_1 = new System.Windows.Forms.Button();
            this.ai_1_2 = new System.Windows.Forms.Button();
            this.ai_1_3 = new System.Windows.Forms.Button();
            this.ai_1_4 = new System.Windows.Forms.Button();
            this.ai_1_5 = new System.Windows.Forms.Button();
            this.ai_1_6 = new System.Windows.Forms.Button();
            this.ai_1_7 = new System.Windows.Forms.Button();
            this.ai_1_8 = new System.Windows.Forms.Button();
            this.ai_1_9 = new System.Windows.Forms.Button();
            this.hp_9_9 = new System.Windows.Forms.Button();
            this.hp_9_8 = new System.Windows.Forms.Button();
            this.hp_9_7 = new System.Windows.Forms.Button();
            this.hp_9_6 = new System.Windows.Forms.Button();
            this.hp_9_5 = new System.Windows.Forms.Button();
            this.hp_9_4 = new System.Windows.Forms.Button();
            this.hp_9_3 = new System.Windows.Forms.Button();
            this.hp_9_2 = new System.Windows.Forms.Button();
            this.hp_9_1 = new System.Windows.Forms.Button();
            this.hp_9_0 = new System.Windows.Forms.Button();
            this.hp_8_9 = new System.Windows.Forms.Button();
            this.hp_8_8 = new System.Windows.Forms.Button();
            this.hp_8_7 = new System.Windows.Forms.Button();
            this.hp_8_6 = new System.Windows.Forms.Button();
            this.hp_8_5 = new System.Windows.Forms.Button();
            this.hp_8_4 = new System.Windows.Forms.Button();
            this.hp_8_3 = new System.Windows.Forms.Button();
            this.hp_8_2 = new System.Windows.Forms.Button();
            this.hp_8_1 = new System.Windows.Forms.Button();
            this.hp_8_0 = new System.Windows.Forms.Button();
            this.hp_7_9 = new System.Windows.Forms.Button();
            this.hp_7_8 = new System.Windows.Forms.Button();
            this.hp_7_7 = new System.Windows.Forms.Button();
            this.hp_7_6 = new System.Windows.Forms.Button();
            this.hp_7_5 = new System.Windows.Forms.Button();
            this.hp_7_4 = new System.Windows.Forms.Button();
            this.hp_7_3 = new System.Windows.Forms.Button();
            this.hp_7_2 = new System.Windows.Forms.Button();
            this.hp_7_1 = new System.Windows.Forms.Button();
            this.hp_7_0 = new System.Windows.Forms.Button();
            this.hp_6_9 = new System.Windows.Forms.Button();
            this.hp_6_8 = new System.Windows.Forms.Button();
            this.hp_6_7 = new System.Windows.Forms.Button();
            this.hp_6_6 = new System.Windows.Forms.Button();
            this.hp_6_5 = new System.Windows.Forms.Button();
            this.hp_6_4 = new System.Windows.Forms.Button();
            this.hp_6_3 = new System.Windows.Forms.Button();
            this.hp_6_2 = new System.Windows.Forms.Button();
            this.hp_6_1 = new System.Windows.Forms.Button();
            this.hp_6_0 = new System.Windows.Forms.Button();
            this.hp_5_9 = new System.Windows.Forms.Button();
            this.hp_5_8 = new System.Windows.Forms.Button();
            this.hp_5_7 = new System.Windows.Forms.Button();
            this.hp_5_6 = new System.Windows.Forms.Button();
            this.hp_5_5 = new System.Windows.Forms.Button();
            this.hp_5_4 = new System.Windows.Forms.Button();
            this.hp_5_3 = new System.Windows.Forms.Button();
            this.hp_5_2 = new System.Windows.Forms.Button();
            this.hp_5_1 = new System.Windows.Forms.Button();
            this.hp_5_0 = new System.Windows.Forms.Button();
            this.hp_4_9 = new System.Windows.Forms.Button();
            this.hp_4_8 = new System.Windows.Forms.Button();
            this.hp_4_7 = new System.Windows.Forms.Button();
            this.hp_4_6 = new System.Windows.Forms.Button();
            this.hp_4_5 = new System.Windows.Forms.Button();
            this.hp_4_4 = new System.Windows.Forms.Button();
            this.hp_4_3 = new System.Windows.Forms.Button();
            this.hp_4_2 = new System.Windows.Forms.Button();
            this.hp_4_1 = new System.Windows.Forms.Button();
            this.hp_4_0 = new System.Windows.Forms.Button();
            this.hp_3_9 = new System.Windows.Forms.Button();
            this.hp_3_8 = new System.Windows.Forms.Button();
            this.hp_3_7 = new System.Windows.Forms.Button();
            this.hp_3_6 = new System.Windows.Forms.Button();
            this.hp_3_5 = new System.Windows.Forms.Button();
            this.hp_3_4 = new System.Windows.Forms.Button();
            this.hp_3_3 = new System.Windows.Forms.Button();
            this.hp_3_2 = new System.Windows.Forms.Button();
            this.hp_3_1 = new System.Windows.Forms.Button();
            this.hp_3_0 = new System.Windows.Forms.Button();
            this.hp_2_9 = new System.Windows.Forms.Button();
            this.hp_2_8 = new System.Windows.Forms.Button();
            this.hp_2_7 = new System.Windows.Forms.Button();
            this.hp_2_6 = new System.Windows.Forms.Button();
            this.hp_2_5 = new System.Windows.Forms.Button();
            this.hp_2_4 = new System.Windows.Forms.Button();
            this.hp_2_3 = new System.Windows.Forms.Button();
            this.hp_2_2 = new System.Windows.Forms.Button();
            this.hp_2_1 = new System.Windows.Forms.Button();
            this.hp_2_0 = new System.Windows.Forms.Button();
            this.hp_1_9 = new System.Windows.Forms.Button();
            this.hp_1_8 = new System.Windows.Forms.Button();
            this.hp_1_7 = new System.Windows.Forms.Button();
            this.hp_1_6 = new System.Windows.Forms.Button();
            this.hp_1_5 = new System.Windows.Forms.Button();
            this.hp_1_4 = new System.Windows.Forms.Button();
            this.hp_1_3 = new System.Windows.Forms.Button();
            this.hp_1_2 = new System.Windows.Forms.Button();
            this.hp_1_1 = new System.Windows.Forms.Button();
            this.hp_1_0 = new System.Windows.Forms.Button();
            this.hp_0_9 = new System.Windows.Forms.Button();
            this.hp_0_8 = new System.Windows.Forms.Button();
            this.hp_0_7 = new System.Windows.Forms.Button();
            this.hp_0_6 = new System.Windows.Forms.Button();
            this.hp_0_5 = new System.Windows.Forms.Button();
            this.hp_0_4 = new System.Windows.Forms.Button();
            this.hp_0_3 = new System.Windows.Forms.Button();
            this.hp_0_2 = new System.Windows.Forms.Button();
            this.hp_0_1 = new System.Windows.Forms.Button();
            this.hp_0_0 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ai_0_0
            // 
            this.ai_0_0.ForeColor = System.Drawing.Color.Black;
            this.ai_0_0.Location = new System.Drawing.Point(12, 35);
            this.ai_0_0.Name = "ai_0_0";
            this.ai_0_0.Size = new System.Drawing.Size(25, 25);
            this.ai_0_0.TabIndex = 0;
            this.ai_0_0.UseVisualStyleBackColor = true;
            this.ai_0_0.Click += new System.EventHandler(this.tick);
            // 
            // ai_0_1
            // 
            this.ai_0_1.ForeColor = System.Drawing.Color.Black;
            this.ai_0_1.Location = new System.Drawing.Point(43, 35);
            this.ai_0_1.Name = "ai_0_1";
            this.ai_0_1.Size = new System.Drawing.Size(25, 25);
            this.ai_0_1.TabIndex = 1;
            this.ai_0_1.UseVisualStyleBackColor = true;
            this.ai_0_1.Click += new System.EventHandler(this.tick);
            // 
            // ai_0_2
            // 
            this.ai_0_2.ForeColor = System.Drawing.Color.Black;
            this.ai_0_2.Location = new System.Drawing.Point(74, 35);
            this.ai_0_2.Name = "ai_0_2";
            this.ai_0_2.Size = new System.Drawing.Size(25, 25);
            this.ai_0_2.TabIndex = 2;
            this.ai_0_2.UseVisualStyleBackColor = true;
            this.ai_0_2.Click += new System.EventHandler(this.tick);
            // 
            // ai_0_3
            // 
            this.ai_0_3.ForeColor = System.Drawing.Color.Black;
            this.ai_0_3.Location = new System.Drawing.Point(105, 35);
            this.ai_0_3.Name = "ai_0_3";
            this.ai_0_3.Size = new System.Drawing.Size(25, 25);
            this.ai_0_3.TabIndex = 3;
            this.ai_0_3.UseVisualStyleBackColor = true;
            this.ai_0_3.Click += new System.EventHandler(this.tick);
            // 
            // ai_0_4
            // 
            this.ai_0_4.ForeColor = System.Drawing.Color.Black;
            this.ai_0_4.Location = new System.Drawing.Point(136, 35);
            this.ai_0_4.Name = "ai_0_4";
            this.ai_0_4.Size = new System.Drawing.Size(25, 25);
            this.ai_0_4.TabIndex = 4;
            this.ai_0_4.UseVisualStyleBackColor = true;
            this.ai_0_4.Click += new System.EventHandler(this.tick);
            // 
            // ai_0_9
            // 
            this.ai_0_9.ForeColor = System.Drawing.Color.Black;
            this.ai_0_9.Location = new System.Drawing.Point(291, 35);
            this.ai_0_9.Name = "ai_0_9";
            this.ai_0_9.Size = new System.Drawing.Size(25, 25);
            this.ai_0_9.TabIndex = 9;
            this.ai_0_9.UseVisualStyleBackColor = true;
            this.ai_0_9.Click += new System.EventHandler(this.tick);
            // 
            // ai_0_8
            // 
            this.ai_0_8.ForeColor = System.Drawing.Color.Black;
            this.ai_0_8.Location = new System.Drawing.Point(260, 35);
            this.ai_0_8.Name = "ai_0_8";
            this.ai_0_8.Size = new System.Drawing.Size(25, 25);
            this.ai_0_8.TabIndex = 8;
            this.ai_0_8.UseVisualStyleBackColor = true;
            this.ai_0_8.Click += new System.EventHandler(this.tick);
            // 
            // ai_0_7
            // 
            this.ai_0_7.ForeColor = System.Drawing.Color.Black;
            this.ai_0_7.Location = new System.Drawing.Point(229, 35);
            this.ai_0_7.Name = "ai_0_7";
            this.ai_0_7.Size = new System.Drawing.Size(25, 25);
            this.ai_0_7.TabIndex = 7;
            this.ai_0_7.UseVisualStyleBackColor = true;
            this.ai_0_7.Click += new System.EventHandler(this.tick);
            // 
            // ai_0_6
            // 
            this.ai_0_6.ForeColor = System.Drawing.Color.Black;
            this.ai_0_6.Location = new System.Drawing.Point(198, 35);
            this.ai_0_6.Name = "ai_0_6";
            this.ai_0_6.Size = new System.Drawing.Size(25, 25);
            this.ai_0_6.TabIndex = 6;
            this.ai_0_6.UseVisualStyleBackColor = true;
            this.ai_0_6.Click += new System.EventHandler(this.tick);
            // 
            // ai_0_5
            // 
            this.ai_0_5.ForeColor = System.Drawing.Color.Black;
            this.ai_0_5.Location = new System.Drawing.Point(167, 35);
            this.ai_0_5.Name = "ai_0_5";
            this.ai_0_5.Size = new System.Drawing.Size(25, 25);
            this.ai_0_5.TabIndex = 5;
            this.ai_0_5.UseVisualStyleBackColor = true;
            this.ai_0_5.Click += new System.EventHandler(this.tick);
            // 
            // ai_2_9
            // 
            this.ai_2_9.ForeColor = System.Drawing.Color.Black;
            this.ai_2_9.Location = new System.Drawing.Point(291, 99);
            this.ai_2_9.Name = "ai_2_9";
            this.ai_2_9.Size = new System.Drawing.Size(25, 25);
            this.ai_2_9.TabIndex = 29;
            this.ai_2_9.UseVisualStyleBackColor = true;
            this.ai_2_9.Click += new System.EventHandler(this.tick);
            // 
            // ai_2_8
            // 
            this.ai_2_8.ForeColor = System.Drawing.Color.Black;
            this.ai_2_8.Location = new System.Drawing.Point(260, 99);
            this.ai_2_8.Name = "ai_2_8";
            this.ai_2_8.Size = new System.Drawing.Size(25, 25);
            this.ai_2_8.TabIndex = 28;
            this.ai_2_8.UseVisualStyleBackColor = true;
            this.ai_2_8.Click += new System.EventHandler(this.tick);
            // 
            // ai_2_7
            // 
            this.ai_2_7.ForeColor = System.Drawing.Color.Black;
            this.ai_2_7.Location = new System.Drawing.Point(229, 99);
            this.ai_2_7.Name = "ai_2_7";
            this.ai_2_7.Size = new System.Drawing.Size(25, 25);
            this.ai_2_7.TabIndex = 27;
            this.ai_2_7.UseVisualStyleBackColor = true;
            this.ai_2_7.Click += new System.EventHandler(this.tick);
            // 
            // ai_2_6
            // 
            this.ai_2_6.ForeColor = System.Drawing.Color.Black;
            this.ai_2_6.Location = new System.Drawing.Point(198, 99);
            this.ai_2_6.Name = "ai_2_6";
            this.ai_2_6.Size = new System.Drawing.Size(25, 25);
            this.ai_2_6.TabIndex = 26;
            this.ai_2_6.UseVisualStyleBackColor = true;
            this.ai_2_6.Click += new System.EventHandler(this.tick);
            // 
            // ai_2_5
            // 
            this.ai_2_5.ForeColor = System.Drawing.Color.Black;
            this.ai_2_5.Location = new System.Drawing.Point(167, 99);
            this.ai_2_5.Name = "ai_2_5";
            this.ai_2_5.Size = new System.Drawing.Size(25, 25);
            this.ai_2_5.TabIndex = 25;
            this.ai_2_5.UseVisualStyleBackColor = true;
            this.ai_2_5.Click += new System.EventHandler(this.tick);
            // 
            // ai_2_4
            // 
            this.ai_2_4.ForeColor = System.Drawing.Color.Black;
            this.ai_2_4.Location = new System.Drawing.Point(136, 99);
            this.ai_2_4.Name = "ai_2_4";
            this.ai_2_4.Size = new System.Drawing.Size(25, 25);
            this.ai_2_4.TabIndex = 24;
            this.ai_2_4.UseVisualStyleBackColor = true;
            this.ai_2_4.Click += new System.EventHandler(this.tick);
            // 
            // ai_2_3
            // 
            this.ai_2_3.ForeColor = System.Drawing.Color.Black;
            this.ai_2_3.Location = new System.Drawing.Point(105, 99);
            this.ai_2_3.Name = "ai_2_3";
            this.ai_2_3.Size = new System.Drawing.Size(25, 25);
            this.ai_2_3.TabIndex = 23;
            this.ai_2_3.UseVisualStyleBackColor = true;
            this.ai_2_3.Click += new System.EventHandler(this.tick);
            // 
            // ai_2_2
            // 
            this.ai_2_2.ForeColor = System.Drawing.Color.Black;
            this.ai_2_2.Location = new System.Drawing.Point(74, 99);
            this.ai_2_2.Name = "ai_2_2";
            this.ai_2_2.Size = new System.Drawing.Size(25, 25);
            this.ai_2_2.TabIndex = 22;
            this.ai_2_2.UseVisualStyleBackColor = true;
            this.ai_2_2.Click += new System.EventHandler(this.tick);
            // 
            // ai_2_1
            // 
            this.ai_2_1.ForeColor = System.Drawing.Color.Black;
            this.ai_2_1.Location = new System.Drawing.Point(43, 99);
            this.ai_2_1.Name = "ai_2_1";
            this.ai_2_1.Size = new System.Drawing.Size(25, 25);
            this.ai_2_1.TabIndex = 21;
            this.ai_2_1.UseVisualStyleBackColor = true;
            this.ai_2_1.Click += new System.EventHandler(this.tick);
            // 
            // ai_2_0
            // 
            this.ai_2_0.ForeColor = System.Drawing.Color.Black;
            this.ai_2_0.Location = new System.Drawing.Point(12, 99);
            this.ai_2_0.Name = "ai_2_0";
            this.ai_2_0.Size = new System.Drawing.Size(25, 25);
            this.ai_2_0.TabIndex = 20;
            this.ai_2_0.UseVisualStyleBackColor = true;
            this.ai_2_0.Click += new System.EventHandler(this.tick);
            // 
            // ai_3_9
            // 
            this.ai_3_9.ForeColor = System.Drawing.Color.Black;
            this.ai_3_9.Location = new System.Drawing.Point(291, 131);
            this.ai_3_9.Name = "ai_3_9";
            this.ai_3_9.Size = new System.Drawing.Size(25, 25);
            this.ai_3_9.TabIndex = 39;
            this.ai_3_9.UseVisualStyleBackColor = true;
            this.ai_3_9.Click += new System.EventHandler(this.tick);
            // 
            // ai_3_8
            // 
            this.ai_3_8.ForeColor = System.Drawing.Color.Black;
            this.ai_3_8.Location = new System.Drawing.Point(260, 131);
            this.ai_3_8.Name = "ai_3_8";
            this.ai_3_8.Size = new System.Drawing.Size(25, 25);
            this.ai_3_8.TabIndex = 38;
            this.ai_3_8.UseVisualStyleBackColor = true;
            this.ai_3_8.Click += new System.EventHandler(this.tick);
            // 
            // ai_3_7
            // 
            this.ai_3_7.ForeColor = System.Drawing.Color.Black;
            this.ai_3_7.Location = new System.Drawing.Point(229, 131);
            this.ai_3_7.Name = "ai_3_7";
            this.ai_3_7.Size = new System.Drawing.Size(25, 25);
            this.ai_3_7.TabIndex = 37;
            this.ai_3_7.UseVisualStyleBackColor = true;
            this.ai_3_7.Click += new System.EventHandler(this.tick);
            // 
            // ai_3_6
            // 
            this.ai_3_6.ForeColor = System.Drawing.Color.Black;
            this.ai_3_6.Location = new System.Drawing.Point(198, 131);
            this.ai_3_6.Name = "ai_3_6";
            this.ai_3_6.Size = new System.Drawing.Size(25, 25);
            this.ai_3_6.TabIndex = 36;
            this.ai_3_6.UseVisualStyleBackColor = true;
            this.ai_3_6.Click += new System.EventHandler(this.tick);
            // 
            // ai_3_5
            // 
            this.ai_3_5.ForeColor = System.Drawing.Color.Black;
            this.ai_3_5.Location = new System.Drawing.Point(167, 131);
            this.ai_3_5.Name = "ai_3_5";
            this.ai_3_5.Size = new System.Drawing.Size(25, 25);
            this.ai_3_5.TabIndex = 35;
            this.ai_3_5.UseVisualStyleBackColor = true;
            this.ai_3_5.Click += new System.EventHandler(this.tick);
            // 
            // ai_3_4
            // 
            this.ai_3_4.ForeColor = System.Drawing.Color.Black;
            this.ai_3_4.Location = new System.Drawing.Point(136, 131);
            this.ai_3_4.Name = "ai_3_4";
            this.ai_3_4.Size = new System.Drawing.Size(25, 25);
            this.ai_3_4.TabIndex = 34;
            this.ai_3_4.UseVisualStyleBackColor = true;
            this.ai_3_4.Click += new System.EventHandler(this.tick);
            // 
            // ai_3_3
            // 
            this.ai_3_3.ForeColor = System.Drawing.Color.Black;
            this.ai_3_3.Location = new System.Drawing.Point(105, 131);
            this.ai_3_3.Name = "ai_3_3";
            this.ai_3_3.Size = new System.Drawing.Size(25, 25);
            this.ai_3_3.TabIndex = 33;
            this.ai_3_3.UseVisualStyleBackColor = true;
            this.ai_3_3.Click += new System.EventHandler(this.tick);
            // 
            // ai_3_2
            // 
            this.ai_3_2.ForeColor = System.Drawing.Color.Black;
            this.ai_3_2.Location = new System.Drawing.Point(74, 131);
            this.ai_3_2.Name = "ai_3_2";
            this.ai_3_2.Size = new System.Drawing.Size(25, 25);
            this.ai_3_2.TabIndex = 32;
            this.ai_3_2.UseVisualStyleBackColor = true;
            this.ai_3_2.Click += new System.EventHandler(this.tick);
            // 
            // ai_3_1
            // 
            this.ai_3_1.ForeColor = System.Drawing.Color.Black;
            this.ai_3_1.Location = new System.Drawing.Point(43, 131);
            this.ai_3_1.Name = "ai_3_1";
            this.ai_3_1.Size = new System.Drawing.Size(25, 25);
            this.ai_3_1.TabIndex = 31;
            this.ai_3_1.UseVisualStyleBackColor = true;
            this.ai_3_1.Click += new System.EventHandler(this.tick);
            // 
            // ai_3_0
            // 
            this.ai_3_0.ForeColor = System.Drawing.Color.Black;
            this.ai_3_0.Location = new System.Drawing.Point(12, 131);
            this.ai_3_0.Name = "ai_3_0";
            this.ai_3_0.Size = new System.Drawing.Size(25, 25);
            this.ai_3_0.TabIndex = 30;
            this.ai_3_0.UseVisualStyleBackColor = true;
            this.ai_3_0.Click += new System.EventHandler(this.tick);
            // 
            // ai_4_9
            // 
            this.ai_4_9.ForeColor = System.Drawing.Color.Black;
            this.ai_4_9.Location = new System.Drawing.Point(291, 163);
            this.ai_4_9.Name = "ai_4_9";
            this.ai_4_9.Size = new System.Drawing.Size(25, 25);
            this.ai_4_9.TabIndex = 49;
            this.ai_4_9.UseVisualStyleBackColor = true;
            this.ai_4_9.Click += new System.EventHandler(this.tick);
            // 
            // ai_4_8
            // 
            this.ai_4_8.ForeColor = System.Drawing.Color.Black;
            this.ai_4_8.Location = new System.Drawing.Point(260, 163);
            this.ai_4_8.Name = "ai_4_8";
            this.ai_4_8.Size = new System.Drawing.Size(25, 25);
            this.ai_4_8.TabIndex = 48;
            this.ai_4_8.UseVisualStyleBackColor = true;
            this.ai_4_8.Click += new System.EventHandler(this.tick);
            // 
            // ai_4_7
            // 
            this.ai_4_7.ForeColor = System.Drawing.Color.Black;
            this.ai_4_7.Location = new System.Drawing.Point(229, 163);
            this.ai_4_7.Name = "ai_4_7";
            this.ai_4_7.Size = new System.Drawing.Size(25, 25);
            this.ai_4_7.TabIndex = 47;
            this.ai_4_7.UseVisualStyleBackColor = true;
            this.ai_4_7.Click += new System.EventHandler(this.tick);
            // 
            // ai_4_6
            // 
            this.ai_4_6.ForeColor = System.Drawing.Color.Black;
            this.ai_4_6.Location = new System.Drawing.Point(198, 163);
            this.ai_4_6.Name = "ai_4_6";
            this.ai_4_6.Size = new System.Drawing.Size(25, 25);
            this.ai_4_6.TabIndex = 46;
            this.ai_4_6.UseVisualStyleBackColor = true;
            this.ai_4_6.Click += new System.EventHandler(this.tick);
            // 
            // ai_4_5
            // 
            this.ai_4_5.ForeColor = System.Drawing.Color.Black;
            this.ai_4_5.Location = new System.Drawing.Point(167, 163);
            this.ai_4_5.Name = "ai_4_5";
            this.ai_4_5.Size = new System.Drawing.Size(25, 25);
            this.ai_4_5.TabIndex = 45;
            this.ai_4_5.UseVisualStyleBackColor = true;
            this.ai_4_5.Click += new System.EventHandler(this.tick);
            // 
            // ai_4_4
            // 
            this.ai_4_4.ForeColor = System.Drawing.Color.Black;
            this.ai_4_4.Location = new System.Drawing.Point(136, 163);
            this.ai_4_4.Name = "ai_4_4";
            this.ai_4_4.Size = new System.Drawing.Size(25, 25);
            this.ai_4_4.TabIndex = 44;
            this.ai_4_4.UseVisualStyleBackColor = true;
            this.ai_4_4.Click += new System.EventHandler(this.tick);
            // 
            // ai_4_3
            // 
            this.ai_4_3.ForeColor = System.Drawing.Color.Black;
            this.ai_4_3.Location = new System.Drawing.Point(105, 163);
            this.ai_4_3.Name = "ai_4_3";
            this.ai_4_3.Size = new System.Drawing.Size(25, 25);
            this.ai_4_3.TabIndex = 43;
            this.ai_4_3.UseVisualStyleBackColor = true;
            this.ai_4_3.Click += new System.EventHandler(this.tick);
            // 
            // ai_4_2
            // 
            this.ai_4_2.ForeColor = System.Drawing.Color.Black;
            this.ai_4_2.Location = new System.Drawing.Point(74, 163);
            this.ai_4_2.Name = "ai_4_2";
            this.ai_4_2.Size = new System.Drawing.Size(25, 25);
            this.ai_4_2.TabIndex = 42;
            this.ai_4_2.UseVisualStyleBackColor = true;
            this.ai_4_2.Click += new System.EventHandler(this.tick);
            // 
            // ai_4_1
            // 
            this.ai_4_1.ForeColor = System.Drawing.Color.Black;
            this.ai_4_1.Location = new System.Drawing.Point(43, 163);
            this.ai_4_1.Name = "ai_4_1";
            this.ai_4_1.Size = new System.Drawing.Size(25, 25);
            this.ai_4_1.TabIndex = 41;
            this.ai_4_1.UseVisualStyleBackColor = true;
            this.ai_4_1.Click += new System.EventHandler(this.tick);
            // 
            // ai_4_0
            // 
            this.ai_4_0.ForeColor = System.Drawing.Color.Black;
            this.ai_4_0.Location = new System.Drawing.Point(12, 163);
            this.ai_4_0.Name = "ai_4_0";
            this.ai_4_0.Size = new System.Drawing.Size(25, 25);
            this.ai_4_0.TabIndex = 40;
            this.ai_4_0.UseVisualStyleBackColor = true;
            this.ai_4_0.Click += new System.EventHandler(this.tick);
            // 
            // ai_5_9
            // 
            this.ai_5_9.ForeColor = System.Drawing.Color.Black;
            this.ai_5_9.Location = new System.Drawing.Point(291, 195);
            this.ai_5_9.Name = "ai_5_9";
            this.ai_5_9.Size = new System.Drawing.Size(25, 25);
            this.ai_5_9.TabIndex = 59;
            this.ai_5_9.UseVisualStyleBackColor = true;
            this.ai_5_9.Click += new System.EventHandler(this.tick);
            // 
            // ai_5_8
            // 
            this.ai_5_8.ForeColor = System.Drawing.Color.Black;
            this.ai_5_8.Location = new System.Drawing.Point(260, 195);
            this.ai_5_8.Name = "ai_5_8";
            this.ai_5_8.Size = new System.Drawing.Size(25, 25);
            this.ai_5_8.TabIndex = 58;
            this.ai_5_8.UseVisualStyleBackColor = true;
            this.ai_5_8.Click += new System.EventHandler(this.tick);
            // 
            // ai_5_7
            // 
            this.ai_5_7.ForeColor = System.Drawing.Color.Black;
            this.ai_5_7.Location = new System.Drawing.Point(229, 195);
            this.ai_5_7.Name = "ai_5_7";
            this.ai_5_7.Size = new System.Drawing.Size(25, 25);
            this.ai_5_7.TabIndex = 57;
            this.ai_5_7.UseVisualStyleBackColor = true;
            this.ai_5_7.Click += new System.EventHandler(this.tick);
            // 
            // ai_5_6
            // 
            this.ai_5_6.ForeColor = System.Drawing.Color.Black;
            this.ai_5_6.Location = new System.Drawing.Point(198, 195);
            this.ai_5_6.Name = "ai_5_6";
            this.ai_5_6.Size = new System.Drawing.Size(25, 25);
            this.ai_5_6.TabIndex = 56;
            this.ai_5_6.UseVisualStyleBackColor = true;
            this.ai_5_6.Click += new System.EventHandler(this.tick);
            // 
            // ai_5_5
            // 
            this.ai_5_5.ForeColor = System.Drawing.Color.Black;
            this.ai_5_5.Location = new System.Drawing.Point(167, 195);
            this.ai_5_5.Name = "ai_5_5";
            this.ai_5_5.Size = new System.Drawing.Size(25, 25);
            this.ai_5_5.TabIndex = 55;
            this.ai_5_5.UseVisualStyleBackColor = true;
            this.ai_5_5.Click += new System.EventHandler(this.tick);
            // 
            // ai_5_4
            // 
            this.ai_5_4.ForeColor = System.Drawing.Color.Black;
            this.ai_5_4.Location = new System.Drawing.Point(136, 195);
            this.ai_5_4.Name = "ai_5_4";
            this.ai_5_4.Size = new System.Drawing.Size(25, 25);
            this.ai_5_4.TabIndex = 54;
            this.ai_5_4.UseVisualStyleBackColor = true;
            this.ai_5_4.Click += new System.EventHandler(this.tick);
            // 
            // ai_5_3
            // 
            this.ai_5_3.ForeColor = System.Drawing.Color.Black;
            this.ai_5_3.Location = new System.Drawing.Point(105, 195);
            this.ai_5_3.Name = "ai_5_3";
            this.ai_5_3.Size = new System.Drawing.Size(25, 25);
            this.ai_5_3.TabIndex = 53;
            this.ai_5_3.UseVisualStyleBackColor = true;
            this.ai_5_3.Click += new System.EventHandler(this.tick);
            // 
            // ai_5_2
            // 
            this.ai_5_2.ForeColor = System.Drawing.Color.Black;
            this.ai_5_2.Location = new System.Drawing.Point(74, 195);
            this.ai_5_2.Name = "ai_5_2";
            this.ai_5_2.Size = new System.Drawing.Size(25, 25);
            this.ai_5_2.TabIndex = 52;
            this.ai_5_2.UseVisualStyleBackColor = true;
            this.ai_5_2.Click += new System.EventHandler(this.tick);
            // 
            // ai_5_1
            // 
            this.ai_5_1.ForeColor = System.Drawing.Color.Black;
            this.ai_5_1.Location = new System.Drawing.Point(43, 195);
            this.ai_5_1.Name = "ai_5_1";
            this.ai_5_1.Size = new System.Drawing.Size(25, 25);
            this.ai_5_1.TabIndex = 51;
            this.ai_5_1.UseVisualStyleBackColor = true;
            this.ai_5_1.Click += new System.EventHandler(this.tick);
            // 
            // ai_5_0
            // 
            this.ai_5_0.ForeColor = System.Drawing.Color.Black;
            this.ai_5_0.Location = new System.Drawing.Point(12, 195);
            this.ai_5_0.Name = "ai_5_0";
            this.ai_5_0.Size = new System.Drawing.Size(25, 25);
            this.ai_5_0.TabIndex = 50;
            this.ai_5_0.UseVisualStyleBackColor = true;
            this.ai_5_0.Click += new System.EventHandler(this.tick);
            // 
            // ai_6_9
            // 
            this.ai_6_9.ForeColor = System.Drawing.Color.Black;
            this.ai_6_9.Location = new System.Drawing.Point(291, 227);
            this.ai_6_9.Name = "ai_6_9";
            this.ai_6_9.Size = new System.Drawing.Size(25, 25);
            this.ai_6_9.TabIndex = 69;
            this.ai_6_9.UseVisualStyleBackColor = true;
            this.ai_6_9.Click += new System.EventHandler(this.tick);
            // 
            // ai_6_8
            // 
            this.ai_6_8.ForeColor = System.Drawing.Color.Black;
            this.ai_6_8.Location = new System.Drawing.Point(260, 227);
            this.ai_6_8.Name = "ai_6_8";
            this.ai_6_8.Size = new System.Drawing.Size(25, 25);
            this.ai_6_8.TabIndex = 68;
            this.ai_6_8.UseVisualStyleBackColor = true;
            this.ai_6_8.Click += new System.EventHandler(this.tick);
            // 
            // ai_6_7
            // 
            this.ai_6_7.ForeColor = System.Drawing.Color.Black;
            this.ai_6_7.Location = new System.Drawing.Point(229, 227);
            this.ai_6_7.Name = "ai_6_7";
            this.ai_6_7.Size = new System.Drawing.Size(25, 25);
            this.ai_6_7.TabIndex = 67;
            this.ai_6_7.UseVisualStyleBackColor = true;
            this.ai_6_7.Click += new System.EventHandler(this.tick);
            // 
            // ai_6_6
            // 
            this.ai_6_6.ForeColor = System.Drawing.Color.Black;
            this.ai_6_6.Location = new System.Drawing.Point(198, 227);
            this.ai_6_6.Name = "ai_6_6";
            this.ai_6_6.Size = new System.Drawing.Size(25, 25);
            this.ai_6_6.TabIndex = 66;
            this.ai_6_6.UseVisualStyleBackColor = true;
            this.ai_6_6.Click += new System.EventHandler(this.tick);
            // 
            // ai_6_5
            // 
            this.ai_6_5.ForeColor = System.Drawing.Color.Black;
            this.ai_6_5.Location = new System.Drawing.Point(167, 227);
            this.ai_6_5.Name = "ai_6_5";
            this.ai_6_5.Size = new System.Drawing.Size(25, 25);
            this.ai_6_5.TabIndex = 65;
            this.ai_6_5.UseVisualStyleBackColor = true;
            this.ai_6_5.Click += new System.EventHandler(this.tick);
            // 
            // ai_6_4
            // 
            this.ai_6_4.ForeColor = System.Drawing.Color.Black;
            this.ai_6_4.Location = new System.Drawing.Point(136, 227);
            this.ai_6_4.Name = "ai_6_4";
            this.ai_6_4.Size = new System.Drawing.Size(25, 25);
            this.ai_6_4.TabIndex = 64;
            this.ai_6_4.UseVisualStyleBackColor = true;
            this.ai_6_4.Click += new System.EventHandler(this.tick);
            // 
            // ai_6_3
            // 
            this.ai_6_3.ForeColor = System.Drawing.Color.Black;
            this.ai_6_3.Location = new System.Drawing.Point(105, 227);
            this.ai_6_3.Name = "ai_6_3";
            this.ai_6_3.Size = new System.Drawing.Size(25, 25);
            this.ai_6_3.TabIndex = 63;
            this.ai_6_3.UseVisualStyleBackColor = true;
            this.ai_6_3.Click += new System.EventHandler(this.tick);
            // 
            // ai_6_2
            // 
            this.ai_6_2.ForeColor = System.Drawing.Color.Black;
            this.ai_6_2.Location = new System.Drawing.Point(74, 227);
            this.ai_6_2.Name = "ai_6_2";
            this.ai_6_2.Size = new System.Drawing.Size(25, 25);
            this.ai_6_2.TabIndex = 62;
            this.ai_6_2.UseVisualStyleBackColor = true;
            this.ai_6_2.Click += new System.EventHandler(this.tick);
            // 
            // ai_6_1
            // 
            this.ai_6_1.ForeColor = System.Drawing.Color.Black;
            this.ai_6_1.Location = new System.Drawing.Point(43, 227);
            this.ai_6_1.Name = "ai_6_1";
            this.ai_6_1.Size = new System.Drawing.Size(25, 25);
            this.ai_6_1.TabIndex = 61;
            this.ai_6_1.UseVisualStyleBackColor = true;
            this.ai_6_1.Click += new System.EventHandler(this.tick);
            // 
            // ai_6_0
            // 
            this.ai_6_0.ForeColor = System.Drawing.Color.Black;
            this.ai_6_0.Location = new System.Drawing.Point(12, 227);
            this.ai_6_0.Name = "ai_6_0";
            this.ai_6_0.Size = new System.Drawing.Size(25, 25);
            this.ai_6_0.TabIndex = 60;
            this.ai_6_0.UseVisualStyleBackColor = true;
            this.ai_6_0.Click += new System.EventHandler(this.tick);
            // 
            // ai_7_9
            // 
            this.ai_7_9.ForeColor = System.Drawing.Color.Black;
            this.ai_7_9.Location = new System.Drawing.Point(291, 259);
            this.ai_7_9.Name = "ai_7_9";
            this.ai_7_9.Size = new System.Drawing.Size(25, 25);
            this.ai_7_9.TabIndex = 79;
            this.ai_7_9.UseVisualStyleBackColor = true;
            this.ai_7_9.Click += new System.EventHandler(this.tick);
            // 
            // ai_7_8
            // 
            this.ai_7_8.ForeColor = System.Drawing.Color.Black;
            this.ai_7_8.Location = new System.Drawing.Point(260, 259);
            this.ai_7_8.Name = "ai_7_8";
            this.ai_7_8.Size = new System.Drawing.Size(25, 25);
            this.ai_7_8.TabIndex = 78;
            this.ai_7_8.UseVisualStyleBackColor = true;
            this.ai_7_8.Click += new System.EventHandler(this.tick);
            // 
            // ai_7_7
            // 
            this.ai_7_7.ForeColor = System.Drawing.Color.Black;
            this.ai_7_7.Location = new System.Drawing.Point(229, 259);
            this.ai_7_7.Name = "ai_7_7";
            this.ai_7_7.Size = new System.Drawing.Size(25, 25);
            this.ai_7_7.TabIndex = 77;
            this.ai_7_7.UseVisualStyleBackColor = true;
            this.ai_7_7.Click += new System.EventHandler(this.tick);
            // 
            // ai_7_6
            // 
            this.ai_7_6.ForeColor = System.Drawing.Color.Black;
            this.ai_7_6.Location = new System.Drawing.Point(198, 259);
            this.ai_7_6.Name = "ai_7_6";
            this.ai_7_6.Size = new System.Drawing.Size(25, 25);
            this.ai_7_6.TabIndex = 76;
            this.ai_7_6.UseVisualStyleBackColor = true;
            this.ai_7_6.Click += new System.EventHandler(this.tick);
            // 
            // ai_7_5
            // 
            this.ai_7_5.ForeColor = System.Drawing.Color.Black;
            this.ai_7_5.Location = new System.Drawing.Point(167, 259);
            this.ai_7_5.Name = "ai_7_5";
            this.ai_7_5.Size = new System.Drawing.Size(25, 25);
            this.ai_7_5.TabIndex = 75;
            this.ai_7_5.UseVisualStyleBackColor = true;
            this.ai_7_5.Click += new System.EventHandler(this.tick);
            // 
            // ai_7_4
            // 
            this.ai_7_4.ForeColor = System.Drawing.Color.Black;
            this.ai_7_4.Location = new System.Drawing.Point(136, 259);
            this.ai_7_4.Name = "ai_7_4";
            this.ai_7_4.Size = new System.Drawing.Size(25, 25);
            this.ai_7_4.TabIndex = 74;
            this.ai_7_4.UseVisualStyleBackColor = true;
            this.ai_7_4.Click += new System.EventHandler(this.tick);
            // 
            // ai_7_3
            // 
            this.ai_7_3.ForeColor = System.Drawing.Color.Black;
            this.ai_7_3.Location = new System.Drawing.Point(105, 259);
            this.ai_7_3.Name = "ai_7_3";
            this.ai_7_3.Size = new System.Drawing.Size(25, 25);
            this.ai_7_3.TabIndex = 73;
            this.ai_7_3.UseVisualStyleBackColor = true;
            this.ai_7_3.Click += new System.EventHandler(this.tick);
            // 
            // ai_7_2
            // 
            this.ai_7_2.ForeColor = System.Drawing.Color.Black;
            this.ai_7_2.Location = new System.Drawing.Point(74, 259);
            this.ai_7_2.Name = "ai_7_2";
            this.ai_7_2.Size = new System.Drawing.Size(25, 25);
            this.ai_7_2.TabIndex = 72;
            this.ai_7_2.UseVisualStyleBackColor = true;
            this.ai_7_2.Click += new System.EventHandler(this.tick);
            // 
            // ai_7_1
            // 
            this.ai_7_1.ForeColor = System.Drawing.Color.Black;
            this.ai_7_1.Location = new System.Drawing.Point(43, 259);
            this.ai_7_1.Name = "ai_7_1";
            this.ai_7_1.Size = new System.Drawing.Size(25, 25);
            this.ai_7_1.TabIndex = 71;
            this.ai_7_1.UseVisualStyleBackColor = true;
            this.ai_7_1.Click += new System.EventHandler(this.tick);
            // 
            // ai_7_0
            // 
            this.ai_7_0.ForeColor = System.Drawing.Color.Black;
            this.ai_7_0.Location = new System.Drawing.Point(12, 259);
            this.ai_7_0.Name = "ai_7_0";
            this.ai_7_0.Size = new System.Drawing.Size(25, 25);
            this.ai_7_0.TabIndex = 70;
            this.ai_7_0.UseVisualStyleBackColor = true;
            this.ai_7_0.Click += new System.EventHandler(this.tick);
            // 
            // ai_8_9
            // 
            this.ai_8_9.ForeColor = System.Drawing.Color.Black;
            this.ai_8_9.Location = new System.Drawing.Point(291, 291);
            this.ai_8_9.Name = "ai_8_9";
            this.ai_8_9.Size = new System.Drawing.Size(25, 25);
            this.ai_8_9.TabIndex = 89;
            this.ai_8_9.UseVisualStyleBackColor = true;
            this.ai_8_9.Click += new System.EventHandler(this.tick);
            // 
            // ai_8_8
            // 
            this.ai_8_8.ForeColor = System.Drawing.Color.Black;
            this.ai_8_8.Location = new System.Drawing.Point(260, 291);
            this.ai_8_8.Name = "ai_8_8";
            this.ai_8_8.Size = new System.Drawing.Size(25, 25);
            this.ai_8_8.TabIndex = 88;
            this.ai_8_8.UseVisualStyleBackColor = true;
            this.ai_8_8.Click += new System.EventHandler(this.tick);
            // 
            // ai_8_7
            // 
            this.ai_8_7.ForeColor = System.Drawing.Color.Black;
            this.ai_8_7.Location = new System.Drawing.Point(229, 291);
            this.ai_8_7.Name = "ai_8_7";
            this.ai_8_7.Size = new System.Drawing.Size(25, 25);
            this.ai_8_7.TabIndex = 87;
            this.ai_8_7.UseVisualStyleBackColor = true;
            this.ai_8_7.Click += new System.EventHandler(this.tick);
            // 
            // ai_8_6
            // 
            this.ai_8_6.ForeColor = System.Drawing.Color.Black;
            this.ai_8_6.Location = new System.Drawing.Point(198, 291);
            this.ai_8_6.Name = "ai_8_6";
            this.ai_8_6.Size = new System.Drawing.Size(25, 25);
            this.ai_8_6.TabIndex = 86;
            this.ai_8_6.UseVisualStyleBackColor = true;
            this.ai_8_6.Click += new System.EventHandler(this.tick);
            // 
            // ai_8_5
            // 
            this.ai_8_5.ForeColor = System.Drawing.Color.Black;
            this.ai_8_5.Location = new System.Drawing.Point(167, 291);
            this.ai_8_5.Name = "ai_8_5";
            this.ai_8_5.Size = new System.Drawing.Size(25, 25);
            this.ai_8_5.TabIndex = 85;
            this.ai_8_5.UseVisualStyleBackColor = true;
            this.ai_8_5.Click += new System.EventHandler(this.tick);
            // 
            // ai_8_4
            // 
            this.ai_8_4.ForeColor = System.Drawing.Color.Black;
            this.ai_8_4.Location = new System.Drawing.Point(136, 291);
            this.ai_8_4.Name = "ai_8_4";
            this.ai_8_4.Size = new System.Drawing.Size(25, 25);
            this.ai_8_4.TabIndex = 84;
            this.ai_8_4.UseVisualStyleBackColor = true;
            this.ai_8_4.Click += new System.EventHandler(this.tick);
            // 
            // ai_8_3
            // 
            this.ai_8_3.ForeColor = System.Drawing.Color.Black;
            this.ai_8_3.Location = new System.Drawing.Point(105, 291);
            this.ai_8_3.Name = "ai_8_3";
            this.ai_8_3.Size = new System.Drawing.Size(25, 25);
            this.ai_8_3.TabIndex = 83;
            this.ai_8_3.UseVisualStyleBackColor = true;
            this.ai_8_3.Click += new System.EventHandler(this.tick);
            // 
            // ai_8_2
            // 
            this.ai_8_2.ForeColor = System.Drawing.Color.Black;
            this.ai_8_2.Location = new System.Drawing.Point(74, 291);
            this.ai_8_2.Name = "ai_8_2";
            this.ai_8_2.Size = new System.Drawing.Size(25, 25);
            this.ai_8_2.TabIndex = 82;
            this.ai_8_2.UseVisualStyleBackColor = true;
            this.ai_8_2.Click += new System.EventHandler(this.tick);
            // 
            // ai_8_1
            // 
            this.ai_8_1.ForeColor = System.Drawing.Color.Black;
            this.ai_8_1.Location = new System.Drawing.Point(43, 291);
            this.ai_8_1.Name = "ai_8_1";
            this.ai_8_1.Size = new System.Drawing.Size(25, 25);
            this.ai_8_1.TabIndex = 81;
            this.ai_8_1.UseVisualStyleBackColor = true;
            this.ai_8_1.Click += new System.EventHandler(this.tick);
            // 
            // ai_8_0
            // 
            this.ai_8_0.ForeColor = System.Drawing.Color.Black;
            this.ai_8_0.Location = new System.Drawing.Point(12, 291);
            this.ai_8_0.Name = "ai_8_0";
            this.ai_8_0.Size = new System.Drawing.Size(25, 25);
            this.ai_8_0.TabIndex = 80;
            this.ai_8_0.UseVisualStyleBackColor = true;
            this.ai_8_0.Click += new System.EventHandler(this.tick);
            // 
            // ai_9_9
            // 
            this.ai_9_9.ForeColor = System.Drawing.Color.Black;
            this.ai_9_9.Location = new System.Drawing.Point(291, 323);
            this.ai_9_9.Name = "ai_9_9";
            this.ai_9_9.Size = new System.Drawing.Size(25, 25);
            this.ai_9_9.TabIndex = 99;
            this.ai_9_9.UseVisualStyleBackColor = true;
            this.ai_9_9.Click += new System.EventHandler(this.tick);
            // 
            // ai_9_8
            // 
            this.ai_9_8.ForeColor = System.Drawing.Color.Black;
            this.ai_9_8.Location = new System.Drawing.Point(260, 323);
            this.ai_9_8.Name = "ai_9_8";
            this.ai_9_8.Size = new System.Drawing.Size(25, 25);
            this.ai_9_8.TabIndex = 98;
            this.ai_9_8.UseVisualStyleBackColor = true;
            this.ai_9_8.Click += new System.EventHandler(this.tick);
            // 
            // ai_9_7
            // 
            this.ai_9_7.ForeColor = System.Drawing.Color.Black;
            this.ai_9_7.Location = new System.Drawing.Point(229, 323);
            this.ai_9_7.Name = "ai_9_7";
            this.ai_9_7.Size = new System.Drawing.Size(25, 25);
            this.ai_9_7.TabIndex = 97;
            this.ai_9_7.UseVisualStyleBackColor = true;
            this.ai_9_7.Click += new System.EventHandler(this.tick);
            // 
            // ai_9_6
            // 
            this.ai_9_6.ForeColor = System.Drawing.Color.Black;
            this.ai_9_6.Location = new System.Drawing.Point(198, 323);
            this.ai_9_6.Name = "ai_9_6";
            this.ai_9_6.Size = new System.Drawing.Size(25, 25);
            this.ai_9_6.TabIndex = 96;
            this.ai_9_6.UseVisualStyleBackColor = true;
            this.ai_9_6.Click += new System.EventHandler(this.tick);
            // 
            // ai_9_5
            // 
            this.ai_9_5.ForeColor = System.Drawing.Color.Black;
            this.ai_9_5.Location = new System.Drawing.Point(167, 323);
            this.ai_9_5.Name = "ai_9_5";
            this.ai_9_5.Size = new System.Drawing.Size(25, 25);
            this.ai_9_5.TabIndex = 95;
            this.ai_9_5.UseVisualStyleBackColor = true;
            this.ai_9_5.Click += new System.EventHandler(this.tick);
            // 
            // ai_9_4
            // 
            this.ai_9_4.ForeColor = System.Drawing.Color.Black;
            this.ai_9_4.Location = new System.Drawing.Point(136, 323);
            this.ai_9_4.Name = "ai_9_4";
            this.ai_9_4.Size = new System.Drawing.Size(25, 25);
            this.ai_9_4.TabIndex = 94;
            this.ai_9_4.UseVisualStyleBackColor = true;
            this.ai_9_4.Click += new System.EventHandler(this.tick);
            // 
            // ai_9_3
            // 
            this.ai_9_3.ForeColor = System.Drawing.Color.Black;
            this.ai_9_3.Location = new System.Drawing.Point(105, 323);
            this.ai_9_3.Name = "ai_9_3";
            this.ai_9_3.Size = new System.Drawing.Size(25, 25);
            this.ai_9_3.TabIndex = 93;
            this.ai_9_3.UseVisualStyleBackColor = true;
            this.ai_9_3.Click += new System.EventHandler(this.tick);
            // 
            // ai_9_2
            // 
            this.ai_9_2.ForeColor = System.Drawing.Color.Black;
            this.ai_9_2.Location = new System.Drawing.Point(74, 323);
            this.ai_9_2.Name = "ai_9_2";
            this.ai_9_2.Size = new System.Drawing.Size(25, 25);
            this.ai_9_2.TabIndex = 92;
            this.ai_9_2.UseVisualStyleBackColor = true;
            this.ai_9_2.Click += new System.EventHandler(this.tick);
            // 
            // ai_9_1
            // 
            this.ai_9_1.ForeColor = System.Drawing.Color.Black;
            this.ai_9_1.Location = new System.Drawing.Point(43, 323);
            this.ai_9_1.Name = "ai_9_1";
            this.ai_9_1.Size = new System.Drawing.Size(25, 25);
            this.ai_9_1.TabIndex = 91;
            this.ai_9_1.UseVisualStyleBackColor = true;
            this.ai_9_1.Click += new System.EventHandler(this.tick);
            // 
            // ai_9_0
            // 
            this.ai_9_0.ForeColor = System.Drawing.Color.Black;
            this.ai_9_0.Location = new System.Drawing.Point(12, 323);
            this.ai_9_0.Name = "ai_9_0";
            this.ai_9_0.Size = new System.Drawing.Size(25, 25);
            this.ai_9_0.TabIndex = 90;
            this.ai_9_0.UseVisualStyleBackColor = true;
            this.ai_9_0.Click += new System.EventHandler(this.tick);
            // 
            // ai_label
            // 
            this.ai_label.AutoSize = true;
            this.ai_label.Location = new System.Drawing.Point(118, 9);
            this.ai_label.Name = "ai_label";
            this.ai_label.Size = new System.Drawing.Size(74, 13);
            this.ai_label.TabIndex = 200;
            this.ai_label.Text = "Flota računala";
            // 
            // hp_label
            // 
            this.hp_label.AutoSize = true;
            this.hp_label.Location = new System.Drawing.Point(505, 9);
            this.hp_label.Name = "hp_label";
            this.hp_label.Size = new System.Drawing.Size(75, 13);
            this.hp_label.TabIndex = 201;
            this.hp_label.Text = "Flota korisnika";
            // 
            // ai_1_0
            // 
            this.ai_1_0.ForeColor = System.Drawing.Color.Black;
            this.ai_1_0.Location = new System.Drawing.Point(12, 67);
            this.ai_1_0.Name = "ai_1_0";
            this.ai_1_0.Size = new System.Drawing.Size(25, 25);
            this.ai_1_0.TabIndex = 10;
            this.ai_1_0.UseVisualStyleBackColor = true;
            this.ai_1_0.Click += new System.EventHandler(this.tick);
            // 
            // ai_1_1
            // 
            this.ai_1_1.ForeColor = System.Drawing.Color.Black;
            this.ai_1_1.Location = new System.Drawing.Point(43, 67);
            this.ai_1_1.Name = "ai_1_1";
            this.ai_1_1.Size = new System.Drawing.Size(25, 25);
            this.ai_1_1.TabIndex = 11;
            this.ai_1_1.UseVisualStyleBackColor = true;
            this.ai_1_1.Click += new System.EventHandler(this.tick);
            // 
            // ai_1_2
            // 
            this.ai_1_2.ForeColor = System.Drawing.Color.Black;
            this.ai_1_2.Location = new System.Drawing.Point(74, 67);
            this.ai_1_2.Name = "ai_1_2";
            this.ai_1_2.Size = new System.Drawing.Size(25, 25);
            this.ai_1_2.TabIndex = 12;
            this.ai_1_2.UseVisualStyleBackColor = true;
            this.ai_1_2.Click += new System.EventHandler(this.tick);
            // 
            // ai_1_3
            // 
            this.ai_1_3.ForeColor = System.Drawing.Color.Black;
            this.ai_1_3.Location = new System.Drawing.Point(105, 67);
            this.ai_1_3.Name = "ai_1_3";
            this.ai_1_3.Size = new System.Drawing.Size(25, 25);
            this.ai_1_3.TabIndex = 13;
            this.ai_1_3.UseVisualStyleBackColor = true;
            this.ai_1_3.Click += new System.EventHandler(this.tick);
            // 
            // ai_1_4
            // 
            this.ai_1_4.ForeColor = System.Drawing.Color.Black;
            this.ai_1_4.Location = new System.Drawing.Point(136, 67);
            this.ai_1_4.Name = "ai_1_4";
            this.ai_1_4.Size = new System.Drawing.Size(25, 25);
            this.ai_1_4.TabIndex = 14;
            this.ai_1_4.UseVisualStyleBackColor = true;
            this.ai_1_4.Click += new System.EventHandler(this.tick);
            // 
            // ai_1_5
            // 
            this.ai_1_5.ForeColor = System.Drawing.Color.Black;
            this.ai_1_5.Location = new System.Drawing.Point(167, 67);
            this.ai_1_5.Name = "ai_1_5";
            this.ai_1_5.Size = new System.Drawing.Size(25, 25);
            this.ai_1_5.TabIndex = 15;
            this.ai_1_5.UseVisualStyleBackColor = true;
            this.ai_1_5.Click += new System.EventHandler(this.tick);
            // 
            // ai_1_6
            // 
            this.ai_1_6.ForeColor = System.Drawing.Color.Black;
            this.ai_1_6.Location = new System.Drawing.Point(198, 67);
            this.ai_1_6.Name = "ai_1_6";
            this.ai_1_6.Size = new System.Drawing.Size(25, 25);
            this.ai_1_6.TabIndex = 16;
            this.ai_1_6.UseVisualStyleBackColor = true;
            this.ai_1_6.Click += new System.EventHandler(this.tick);
            // 
            // ai_1_7
            // 
            this.ai_1_7.ForeColor = System.Drawing.Color.Black;
            this.ai_1_7.Location = new System.Drawing.Point(229, 67);
            this.ai_1_7.Name = "ai_1_7";
            this.ai_1_7.Size = new System.Drawing.Size(25, 25);
            this.ai_1_7.TabIndex = 17;
            this.ai_1_7.UseVisualStyleBackColor = true;
            this.ai_1_7.Click += new System.EventHandler(this.tick);
            // 
            // ai_1_8
            // 
            this.ai_1_8.ForeColor = System.Drawing.Color.Black;
            this.ai_1_8.Location = new System.Drawing.Point(260, 67);
            this.ai_1_8.Name = "ai_1_8";
            this.ai_1_8.Size = new System.Drawing.Size(25, 25);
            this.ai_1_8.TabIndex = 18;
            this.ai_1_8.UseVisualStyleBackColor = true;
            this.ai_1_8.Click += new System.EventHandler(this.tick);
            // 
            // ai_1_9
            // 
            this.ai_1_9.ForeColor = System.Drawing.Color.Black;
            this.ai_1_9.Location = new System.Drawing.Point(291, 67);
            this.ai_1_9.Name = "ai_1_9";
            this.ai_1_9.Size = new System.Drawing.Size(25, 25);
            this.ai_1_9.TabIndex = 19;
            this.ai_1_9.UseVisualStyleBackColor = true;
            this.ai_1_9.Click += new System.EventHandler(this.tick);
            // 
            // hp_9_9
            // 
            this.hp_9_9.Enabled = false;
            this.hp_9_9.ForeColor = System.Drawing.Color.Black;
            this.hp_9_9.Location = new System.Drawing.Point(663, 323);
            this.hp_9_9.Name = "hp_9_9";
            this.hp_9_9.Size = new System.Drawing.Size(25, 25);
            this.hp_9_9.TabIndex = 301;
            this.hp_9_9.UseVisualStyleBackColor = true;
            // 
            // hp_9_8
            // 
            this.hp_9_8.Enabled = false;
            this.hp_9_8.ForeColor = System.Drawing.Color.Black;
            this.hp_9_8.Location = new System.Drawing.Point(632, 323);
            this.hp_9_8.Name = "hp_9_8";
            this.hp_9_8.Size = new System.Drawing.Size(25, 25);
            this.hp_9_8.TabIndex = 300;
            this.hp_9_8.UseVisualStyleBackColor = true;
            // 
            // hp_9_7
            // 
            this.hp_9_7.Enabled = false;
            this.hp_9_7.ForeColor = System.Drawing.Color.Black;
            this.hp_9_7.Location = new System.Drawing.Point(601, 323);
            this.hp_9_7.Name = "hp_9_7";
            this.hp_9_7.Size = new System.Drawing.Size(25, 25);
            this.hp_9_7.TabIndex = 299;
            this.hp_9_7.UseVisualStyleBackColor = true;
            // 
            // hp_9_6
            // 
            this.hp_9_6.Enabled = false;
            this.hp_9_6.ForeColor = System.Drawing.Color.Black;
            this.hp_9_6.Location = new System.Drawing.Point(570, 323);
            this.hp_9_6.Name = "hp_9_6";
            this.hp_9_6.Size = new System.Drawing.Size(25, 25);
            this.hp_9_6.TabIndex = 298;
            this.hp_9_6.UseVisualStyleBackColor = true;
            // 
            // hp_9_5
            // 
            this.hp_9_5.Enabled = false;
            this.hp_9_5.ForeColor = System.Drawing.Color.Black;
            this.hp_9_5.Location = new System.Drawing.Point(539, 323);
            this.hp_9_5.Name = "hp_9_5";
            this.hp_9_5.Size = new System.Drawing.Size(25, 25);
            this.hp_9_5.TabIndex = 297;
            this.hp_9_5.UseVisualStyleBackColor = true;
            // 
            // hp_9_4
            // 
            this.hp_9_4.Enabled = false;
            this.hp_9_4.ForeColor = System.Drawing.Color.Black;
            this.hp_9_4.Location = new System.Drawing.Point(508, 323);
            this.hp_9_4.Name = "hp_9_4";
            this.hp_9_4.Size = new System.Drawing.Size(25, 25);
            this.hp_9_4.TabIndex = 296;
            this.hp_9_4.UseVisualStyleBackColor = true;
            // 
            // hp_9_3
            // 
            this.hp_9_3.Enabled = false;
            this.hp_9_3.ForeColor = System.Drawing.Color.Black;
            this.hp_9_3.Location = new System.Drawing.Point(477, 323);
            this.hp_9_3.Name = "hp_9_3";
            this.hp_9_3.Size = new System.Drawing.Size(25, 25);
            this.hp_9_3.TabIndex = 295;
            this.hp_9_3.UseVisualStyleBackColor = true;
            // 
            // hp_9_2
            // 
            this.hp_9_2.Enabled = false;
            this.hp_9_2.ForeColor = System.Drawing.Color.Black;
            this.hp_9_2.Location = new System.Drawing.Point(446, 323);
            this.hp_9_2.Name = "hp_9_2";
            this.hp_9_2.Size = new System.Drawing.Size(25, 25);
            this.hp_9_2.TabIndex = 294;
            this.hp_9_2.UseVisualStyleBackColor = true;
            // 
            // hp_9_1
            // 
            this.hp_9_1.Enabled = false;
            this.hp_9_1.ForeColor = System.Drawing.Color.Black;
            this.hp_9_1.Location = new System.Drawing.Point(415, 323);
            this.hp_9_1.Name = "hp_9_1";
            this.hp_9_1.Size = new System.Drawing.Size(25, 25);
            this.hp_9_1.TabIndex = 293;
            this.hp_9_1.UseVisualStyleBackColor = true;
            // 
            // hp_9_0
            // 
            this.hp_9_0.Enabled = false;
            this.hp_9_0.ForeColor = System.Drawing.Color.Black;
            this.hp_9_0.Location = new System.Drawing.Point(384, 323);
            this.hp_9_0.Name = "hp_9_0";
            this.hp_9_0.Size = new System.Drawing.Size(25, 25);
            this.hp_9_0.TabIndex = 292;
            this.hp_9_0.UseVisualStyleBackColor = true;
            // 
            // hp_8_9
            // 
            this.hp_8_9.Enabled = false;
            this.hp_8_9.ForeColor = System.Drawing.Color.Black;
            this.hp_8_9.Location = new System.Drawing.Point(663, 291);
            this.hp_8_9.Name = "hp_8_9";
            this.hp_8_9.Size = new System.Drawing.Size(25, 25);
            this.hp_8_9.TabIndex = 291;
            this.hp_8_9.UseVisualStyleBackColor = true;
            // 
            // hp_8_8
            // 
            this.hp_8_8.Enabled = false;
            this.hp_8_8.ForeColor = System.Drawing.Color.Black;
            this.hp_8_8.Location = new System.Drawing.Point(632, 291);
            this.hp_8_8.Name = "hp_8_8";
            this.hp_8_8.Size = new System.Drawing.Size(25, 25);
            this.hp_8_8.TabIndex = 290;
            this.hp_8_8.UseVisualStyleBackColor = true;
            // 
            // hp_8_7
            // 
            this.hp_8_7.Enabled = false;
            this.hp_8_7.ForeColor = System.Drawing.Color.Black;
            this.hp_8_7.Location = new System.Drawing.Point(601, 291);
            this.hp_8_7.Name = "hp_8_7";
            this.hp_8_7.Size = new System.Drawing.Size(25, 25);
            this.hp_8_7.TabIndex = 289;
            this.hp_8_7.UseVisualStyleBackColor = true;
            // 
            // hp_8_6
            // 
            this.hp_8_6.Enabled = false;
            this.hp_8_6.ForeColor = System.Drawing.Color.Black;
            this.hp_8_6.Location = new System.Drawing.Point(570, 291);
            this.hp_8_6.Name = "hp_8_6";
            this.hp_8_6.Size = new System.Drawing.Size(25, 25);
            this.hp_8_6.TabIndex = 288;
            this.hp_8_6.UseVisualStyleBackColor = true;
            // 
            // hp_8_5
            // 
            this.hp_8_5.Enabled = false;
            this.hp_8_5.ForeColor = System.Drawing.Color.Black;
            this.hp_8_5.Location = new System.Drawing.Point(539, 291);
            this.hp_8_5.Name = "hp_8_5";
            this.hp_8_5.Size = new System.Drawing.Size(25, 25);
            this.hp_8_5.TabIndex = 287;
            this.hp_8_5.UseVisualStyleBackColor = true;
            // 
            // hp_8_4
            // 
            this.hp_8_4.Enabled = false;
            this.hp_8_4.ForeColor = System.Drawing.Color.Black;
            this.hp_8_4.Location = new System.Drawing.Point(508, 291);
            this.hp_8_4.Name = "hp_8_4";
            this.hp_8_4.Size = new System.Drawing.Size(25, 25);
            this.hp_8_4.TabIndex = 286;
            this.hp_8_4.UseVisualStyleBackColor = true;
            // 
            // hp_8_3
            // 
            this.hp_8_3.Enabled = false;
            this.hp_8_3.ForeColor = System.Drawing.Color.Black;
            this.hp_8_3.Location = new System.Drawing.Point(477, 291);
            this.hp_8_3.Name = "hp_8_3";
            this.hp_8_3.Size = new System.Drawing.Size(25, 25);
            this.hp_8_3.TabIndex = 285;
            this.hp_8_3.UseVisualStyleBackColor = true;
            // 
            // hp_8_2
            // 
            this.hp_8_2.Enabled = false;
            this.hp_8_2.ForeColor = System.Drawing.Color.Black;
            this.hp_8_2.Location = new System.Drawing.Point(446, 291);
            this.hp_8_2.Name = "hp_8_2";
            this.hp_8_2.Size = new System.Drawing.Size(25, 25);
            this.hp_8_2.TabIndex = 284;
            this.hp_8_2.UseVisualStyleBackColor = true;
            // 
            // hp_8_1
            // 
            this.hp_8_1.Enabled = false;
            this.hp_8_1.ForeColor = System.Drawing.Color.Black;
            this.hp_8_1.Location = new System.Drawing.Point(415, 291);
            this.hp_8_1.Name = "hp_8_1";
            this.hp_8_1.Size = new System.Drawing.Size(25, 25);
            this.hp_8_1.TabIndex = 283;
            this.hp_8_1.UseVisualStyleBackColor = true;
            // 
            // hp_8_0
            // 
            this.hp_8_0.Enabled = false;
            this.hp_8_0.ForeColor = System.Drawing.Color.Black;
            this.hp_8_0.Location = new System.Drawing.Point(384, 291);
            this.hp_8_0.Name = "hp_8_0";
            this.hp_8_0.Size = new System.Drawing.Size(25, 25);
            this.hp_8_0.TabIndex = 282;
            this.hp_8_0.UseVisualStyleBackColor = true;
            // 
            // hp_7_9
            // 
            this.hp_7_9.Enabled = false;
            this.hp_7_9.ForeColor = System.Drawing.Color.Black;
            this.hp_7_9.Location = new System.Drawing.Point(663, 259);
            this.hp_7_9.Name = "hp_7_9";
            this.hp_7_9.Size = new System.Drawing.Size(25, 25);
            this.hp_7_9.TabIndex = 281;
            this.hp_7_9.UseVisualStyleBackColor = true;
            // 
            // hp_7_8
            // 
            this.hp_7_8.Enabled = false;
            this.hp_7_8.ForeColor = System.Drawing.Color.Black;
            this.hp_7_8.Location = new System.Drawing.Point(632, 259);
            this.hp_7_8.Name = "hp_7_8";
            this.hp_7_8.Size = new System.Drawing.Size(25, 25);
            this.hp_7_8.TabIndex = 280;
            this.hp_7_8.UseVisualStyleBackColor = true;
            // 
            // hp_7_7
            // 
            this.hp_7_7.Enabled = false;
            this.hp_7_7.ForeColor = System.Drawing.Color.Black;
            this.hp_7_7.Location = new System.Drawing.Point(601, 259);
            this.hp_7_7.Name = "hp_7_7";
            this.hp_7_7.Size = new System.Drawing.Size(25, 25);
            this.hp_7_7.TabIndex = 279;
            this.hp_7_7.UseVisualStyleBackColor = true;
            // 
            // hp_7_6
            // 
            this.hp_7_6.Enabled = false;
            this.hp_7_6.ForeColor = System.Drawing.Color.Black;
            this.hp_7_6.Location = new System.Drawing.Point(570, 259);
            this.hp_7_6.Name = "hp_7_6";
            this.hp_7_6.Size = new System.Drawing.Size(25, 25);
            this.hp_7_6.TabIndex = 278;
            this.hp_7_6.UseVisualStyleBackColor = true;
            // 
            // hp_7_5
            // 
            this.hp_7_5.Enabled = false;
            this.hp_7_5.ForeColor = System.Drawing.Color.Black;
            this.hp_7_5.Location = new System.Drawing.Point(539, 259);
            this.hp_7_5.Name = "hp_7_5";
            this.hp_7_5.Size = new System.Drawing.Size(25, 25);
            this.hp_7_5.TabIndex = 277;
            this.hp_7_5.UseVisualStyleBackColor = true;
            // 
            // hp_7_4
            // 
            this.hp_7_4.Enabled = false;
            this.hp_7_4.ForeColor = System.Drawing.Color.Black;
            this.hp_7_4.Location = new System.Drawing.Point(508, 259);
            this.hp_7_4.Name = "hp_7_4";
            this.hp_7_4.Size = new System.Drawing.Size(25, 25);
            this.hp_7_4.TabIndex = 276;
            this.hp_7_4.UseVisualStyleBackColor = true;
            // 
            // hp_7_3
            // 
            this.hp_7_3.Enabled = false;
            this.hp_7_3.ForeColor = System.Drawing.Color.Black;
            this.hp_7_3.Location = new System.Drawing.Point(477, 259);
            this.hp_7_3.Name = "hp_7_3";
            this.hp_7_3.Size = new System.Drawing.Size(25, 25);
            this.hp_7_3.TabIndex = 275;
            this.hp_7_3.UseVisualStyleBackColor = true;
            // 
            // hp_7_2
            // 
            this.hp_7_2.Enabled = false;
            this.hp_7_2.ForeColor = System.Drawing.Color.Black;
            this.hp_7_2.Location = new System.Drawing.Point(446, 259);
            this.hp_7_2.Name = "hp_7_2";
            this.hp_7_2.Size = new System.Drawing.Size(25, 25);
            this.hp_7_2.TabIndex = 274;
            this.hp_7_2.UseVisualStyleBackColor = true;
            // 
            // hp_7_1
            // 
            this.hp_7_1.Enabled = false;
            this.hp_7_1.ForeColor = System.Drawing.Color.Black;
            this.hp_7_1.Location = new System.Drawing.Point(415, 259);
            this.hp_7_1.Name = "hp_7_1";
            this.hp_7_1.Size = new System.Drawing.Size(25, 25);
            this.hp_7_1.TabIndex = 273;
            this.hp_7_1.UseVisualStyleBackColor = true;
            // 
            // hp_7_0
            // 
            this.hp_7_0.Enabled = false;
            this.hp_7_0.ForeColor = System.Drawing.Color.Black;
            this.hp_7_0.Location = new System.Drawing.Point(384, 259);
            this.hp_7_0.Name = "hp_7_0";
            this.hp_7_0.Size = new System.Drawing.Size(25, 25);
            this.hp_7_0.TabIndex = 272;
            this.hp_7_0.UseVisualStyleBackColor = true;
            // 
            // hp_6_9
            // 
            this.hp_6_9.Enabled = false;
            this.hp_6_9.ForeColor = System.Drawing.Color.Black;
            this.hp_6_9.Location = new System.Drawing.Point(663, 227);
            this.hp_6_9.Name = "hp_6_9";
            this.hp_6_9.Size = new System.Drawing.Size(25, 25);
            this.hp_6_9.TabIndex = 271;
            this.hp_6_9.UseVisualStyleBackColor = true;
            // 
            // hp_6_8
            // 
            this.hp_6_8.Enabled = false;
            this.hp_6_8.ForeColor = System.Drawing.Color.Black;
            this.hp_6_8.Location = new System.Drawing.Point(632, 227);
            this.hp_6_8.Name = "hp_6_8";
            this.hp_6_8.Size = new System.Drawing.Size(25, 25);
            this.hp_6_8.TabIndex = 270;
            this.hp_6_8.UseVisualStyleBackColor = true;
            // 
            // hp_6_7
            // 
            this.hp_6_7.Enabled = false;
            this.hp_6_7.ForeColor = System.Drawing.Color.Black;
            this.hp_6_7.Location = new System.Drawing.Point(601, 227);
            this.hp_6_7.Name = "hp_6_7";
            this.hp_6_7.Size = new System.Drawing.Size(25, 25);
            this.hp_6_7.TabIndex = 269;
            this.hp_6_7.UseVisualStyleBackColor = true;
            // 
            // hp_6_6
            // 
            this.hp_6_6.Enabled = false;
            this.hp_6_6.ForeColor = System.Drawing.Color.Black;
            this.hp_6_6.Location = new System.Drawing.Point(570, 227);
            this.hp_6_6.Name = "hp_6_6";
            this.hp_6_6.Size = new System.Drawing.Size(25, 25);
            this.hp_6_6.TabIndex = 268;
            this.hp_6_6.UseVisualStyleBackColor = true;
            // 
            // hp_6_5
            // 
            this.hp_6_5.Enabled = false;
            this.hp_6_5.ForeColor = System.Drawing.Color.Black;
            this.hp_6_5.Location = new System.Drawing.Point(539, 227);
            this.hp_6_5.Name = "hp_6_5";
            this.hp_6_5.Size = new System.Drawing.Size(25, 25);
            this.hp_6_5.TabIndex = 267;
            this.hp_6_5.UseVisualStyleBackColor = true;
            // 
            // hp_6_4
            // 
            this.hp_6_4.Enabled = false;
            this.hp_6_4.ForeColor = System.Drawing.Color.Black;
            this.hp_6_4.Location = new System.Drawing.Point(508, 227);
            this.hp_6_4.Name = "hp_6_4";
            this.hp_6_4.Size = new System.Drawing.Size(25, 25);
            this.hp_6_4.TabIndex = 266;
            this.hp_6_4.UseVisualStyleBackColor = true;
            // 
            // hp_6_3
            // 
            this.hp_6_3.Enabled = false;
            this.hp_6_3.ForeColor = System.Drawing.Color.Black;
            this.hp_6_3.Location = new System.Drawing.Point(477, 227);
            this.hp_6_3.Name = "hp_6_3";
            this.hp_6_3.Size = new System.Drawing.Size(25, 25);
            this.hp_6_3.TabIndex = 265;
            this.hp_6_3.UseVisualStyleBackColor = true;
            // 
            // hp_6_2
            // 
            this.hp_6_2.Enabled = false;
            this.hp_6_2.ForeColor = System.Drawing.Color.Black;
            this.hp_6_2.Location = new System.Drawing.Point(446, 227);
            this.hp_6_2.Name = "hp_6_2";
            this.hp_6_2.Size = new System.Drawing.Size(25, 25);
            this.hp_6_2.TabIndex = 264;
            this.hp_6_2.UseVisualStyleBackColor = true;
            // 
            // hp_6_1
            // 
            this.hp_6_1.Enabled = false;
            this.hp_6_1.ForeColor = System.Drawing.Color.Black;
            this.hp_6_1.Location = new System.Drawing.Point(415, 227);
            this.hp_6_1.Name = "hp_6_1";
            this.hp_6_1.Size = new System.Drawing.Size(25, 25);
            this.hp_6_1.TabIndex = 263;
            this.hp_6_1.UseVisualStyleBackColor = true;
            // 
            // hp_6_0
            // 
            this.hp_6_0.Enabled = false;
            this.hp_6_0.ForeColor = System.Drawing.Color.Black;
            this.hp_6_0.Location = new System.Drawing.Point(384, 227);
            this.hp_6_0.Name = "hp_6_0";
            this.hp_6_0.Size = new System.Drawing.Size(25, 25);
            this.hp_6_0.TabIndex = 262;
            this.hp_6_0.UseVisualStyleBackColor = true;
            // 
            // hp_5_9
            // 
            this.hp_5_9.Enabled = false;
            this.hp_5_9.ForeColor = System.Drawing.Color.Black;
            this.hp_5_9.Location = new System.Drawing.Point(663, 195);
            this.hp_5_9.Name = "hp_5_9";
            this.hp_5_9.Size = new System.Drawing.Size(25, 25);
            this.hp_5_9.TabIndex = 261;
            this.hp_5_9.UseVisualStyleBackColor = true;
            // 
            // hp_5_8
            // 
            this.hp_5_8.Enabled = false;
            this.hp_5_8.ForeColor = System.Drawing.Color.Black;
            this.hp_5_8.Location = new System.Drawing.Point(632, 195);
            this.hp_5_8.Name = "hp_5_8";
            this.hp_5_8.Size = new System.Drawing.Size(25, 25);
            this.hp_5_8.TabIndex = 260;
            this.hp_5_8.UseVisualStyleBackColor = true;
            // 
            // hp_5_7
            // 
            this.hp_5_7.Enabled = false;
            this.hp_5_7.ForeColor = System.Drawing.Color.Black;
            this.hp_5_7.Location = new System.Drawing.Point(601, 195);
            this.hp_5_7.Name = "hp_5_7";
            this.hp_5_7.Size = new System.Drawing.Size(25, 25);
            this.hp_5_7.TabIndex = 259;
            this.hp_5_7.UseVisualStyleBackColor = true;
            // 
            // hp_5_6
            // 
            this.hp_5_6.Enabled = false;
            this.hp_5_6.ForeColor = System.Drawing.Color.Black;
            this.hp_5_6.Location = new System.Drawing.Point(570, 195);
            this.hp_5_6.Name = "hp_5_6";
            this.hp_5_6.Size = new System.Drawing.Size(25, 25);
            this.hp_5_6.TabIndex = 258;
            this.hp_5_6.UseVisualStyleBackColor = true;
            // 
            // hp_5_5
            // 
            this.hp_5_5.Enabled = false;
            this.hp_5_5.ForeColor = System.Drawing.Color.Black;
            this.hp_5_5.Location = new System.Drawing.Point(539, 195);
            this.hp_5_5.Name = "hp_5_5";
            this.hp_5_5.Size = new System.Drawing.Size(25, 25);
            this.hp_5_5.TabIndex = 257;
            this.hp_5_5.UseVisualStyleBackColor = true;
            // 
            // hp_5_4
            // 
            this.hp_5_4.Enabled = false;
            this.hp_5_4.ForeColor = System.Drawing.Color.Black;
            this.hp_5_4.Location = new System.Drawing.Point(508, 195);
            this.hp_5_4.Name = "hp_5_4";
            this.hp_5_4.Size = new System.Drawing.Size(25, 25);
            this.hp_5_4.TabIndex = 256;
            this.hp_5_4.UseVisualStyleBackColor = true;
            // 
            // hp_5_3
            // 
            this.hp_5_3.Enabled = false;
            this.hp_5_3.ForeColor = System.Drawing.Color.Black;
            this.hp_5_3.Location = new System.Drawing.Point(477, 195);
            this.hp_5_3.Name = "hp_5_3";
            this.hp_5_3.Size = new System.Drawing.Size(25, 25);
            this.hp_5_3.TabIndex = 255;
            this.hp_5_3.UseVisualStyleBackColor = true;
            // 
            // hp_5_2
            // 
            this.hp_5_2.Enabled = false;
            this.hp_5_2.ForeColor = System.Drawing.Color.Black;
            this.hp_5_2.Location = new System.Drawing.Point(446, 195);
            this.hp_5_2.Name = "hp_5_2";
            this.hp_5_2.Size = new System.Drawing.Size(25, 25);
            this.hp_5_2.TabIndex = 254;
            this.hp_5_2.UseVisualStyleBackColor = true;
            // 
            // hp_5_1
            // 
            this.hp_5_1.Enabled = false;
            this.hp_5_1.ForeColor = System.Drawing.Color.Black;
            this.hp_5_1.Location = new System.Drawing.Point(415, 195);
            this.hp_5_1.Name = "hp_5_1";
            this.hp_5_1.Size = new System.Drawing.Size(25, 25);
            this.hp_5_1.TabIndex = 253;
            this.hp_5_1.UseVisualStyleBackColor = true;
            // 
            // hp_5_0
            // 
            this.hp_5_0.Enabled = false;
            this.hp_5_0.ForeColor = System.Drawing.Color.Black;
            this.hp_5_0.Location = new System.Drawing.Point(384, 195);
            this.hp_5_0.Name = "hp_5_0";
            this.hp_5_0.Size = new System.Drawing.Size(25, 25);
            this.hp_5_0.TabIndex = 252;
            this.hp_5_0.UseVisualStyleBackColor = true;
            // 
            // hp_4_9
            // 
            this.hp_4_9.Enabled = false;
            this.hp_4_9.ForeColor = System.Drawing.Color.Black;
            this.hp_4_9.Location = new System.Drawing.Point(663, 163);
            this.hp_4_9.Name = "hp_4_9";
            this.hp_4_9.Size = new System.Drawing.Size(25, 25);
            this.hp_4_9.TabIndex = 251;
            this.hp_4_9.UseVisualStyleBackColor = true;
            // 
            // hp_4_8
            // 
            this.hp_4_8.Enabled = false;
            this.hp_4_8.ForeColor = System.Drawing.Color.Black;
            this.hp_4_8.Location = new System.Drawing.Point(632, 163);
            this.hp_4_8.Name = "hp_4_8";
            this.hp_4_8.Size = new System.Drawing.Size(25, 25);
            this.hp_4_8.TabIndex = 250;
            this.hp_4_8.UseVisualStyleBackColor = true;
            // 
            // hp_4_7
            // 
            this.hp_4_7.Enabled = false;
            this.hp_4_7.ForeColor = System.Drawing.Color.Black;
            this.hp_4_7.Location = new System.Drawing.Point(601, 163);
            this.hp_4_7.Name = "hp_4_7";
            this.hp_4_7.Size = new System.Drawing.Size(25, 25);
            this.hp_4_7.TabIndex = 249;
            this.hp_4_7.UseVisualStyleBackColor = true;
            // 
            // hp_4_6
            // 
            this.hp_4_6.Enabled = false;
            this.hp_4_6.ForeColor = System.Drawing.Color.Black;
            this.hp_4_6.Location = new System.Drawing.Point(570, 163);
            this.hp_4_6.Name = "hp_4_6";
            this.hp_4_6.Size = new System.Drawing.Size(25, 25);
            this.hp_4_6.TabIndex = 248;
            this.hp_4_6.UseVisualStyleBackColor = true;
            // 
            // hp_4_5
            // 
            this.hp_4_5.Enabled = false;
            this.hp_4_5.ForeColor = System.Drawing.Color.Black;
            this.hp_4_5.Location = new System.Drawing.Point(539, 163);
            this.hp_4_5.Name = "hp_4_5";
            this.hp_4_5.Size = new System.Drawing.Size(25, 25);
            this.hp_4_5.TabIndex = 247;
            this.hp_4_5.UseVisualStyleBackColor = true;
            // 
            // hp_4_4
            // 
            this.hp_4_4.Enabled = false;
            this.hp_4_4.ForeColor = System.Drawing.Color.Black;
            this.hp_4_4.Location = new System.Drawing.Point(508, 163);
            this.hp_4_4.Name = "hp_4_4";
            this.hp_4_4.Size = new System.Drawing.Size(25, 25);
            this.hp_4_4.TabIndex = 246;
            this.hp_4_4.UseVisualStyleBackColor = true;
            // 
            // hp_4_3
            // 
            this.hp_4_3.Enabled = false;
            this.hp_4_3.ForeColor = System.Drawing.Color.Black;
            this.hp_4_3.Location = new System.Drawing.Point(477, 163);
            this.hp_4_3.Name = "hp_4_3";
            this.hp_4_3.Size = new System.Drawing.Size(25, 25);
            this.hp_4_3.TabIndex = 245;
            this.hp_4_3.UseVisualStyleBackColor = true;
            // 
            // hp_4_2
            // 
            this.hp_4_2.Enabled = false;
            this.hp_4_2.ForeColor = System.Drawing.Color.Black;
            this.hp_4_2.Location = new System.Drawing.Point(446, 163);
            this.hp_4_2.Name = "hp_4_2";
            this.hp_4_2.Size = new System.Drawing.Size(25, 25);
            this.hp_4_2.TabIndex = 244;
            this.hp_4_2.UseVisualStyleBackColor = true;
            // 
            // hp_4_1
            // 
            this.hp_4_1.Enabled = false;
            this.hp_4_1.ForeColor = System.Drawing.Color.Black;
            this.hp_4_1.Location = new System.Drawing.Point(415, 163);
            this.hp_4_1.Name = "hp_4_1";
            this.hp_4_1.Size = new System.Drawing.Size(25, 25);
            this.hp_4_1.TabIndex = 243;
            this.hp_4_1.UseVisualStyleBackColor = true;
            // 
            // hp_4_0
            // 
            this.hp_4_0.Enabled = false;
            this.hp_4_0.ForeColor = System.Drawing.Color.Black;
            this.hp_4_0.Location = new System.Drawing.Point(384, 163);
            this.hp_4_0.Name = "hp_4_0";
            this.hp_4_0.Size = new System.Drawing.Size(25, 25);
            this.hp_4_0.TabIndex = 242;
            this.hp_4_0.UseVisualStyleBackColor = true;
            // 
            // hp_3_9
            // 
            this.hp_3_9.Enabled = false;
            this.hp_3_9.ForeColor = System.Drawing.Color.Black;
            this.hp_3_9.Location = new System.Drawing.Point(663, 131);
            this.hp_3_9.Name = "hp_3_9";
            this.hp_3_9.Size = new System.Drawing.Size(25, 25);
            this.hp_3_9.TabIndex = 241;
            this.hp_3_9.UseVisualStyleBackColor = true;
            // 
            // hp_3_8
            // 
            this.hp_3_8.Enabled = false;
            this.hp_3_8.ForeColor = System.Drawing.Color.Black;
            this.hp_3_8.Location = new System.Drawing.Point(632, 131);
            this.hp_3_8.Name = "hp_3_8";
            this.hp_3_8.Size = new System.Drawing.Size(25, 25);
            this.hp_3_8.TabIndex = 240;
            this.hp_3_8.UseVisualStyleBackColor = true;
            // 
            // hp_3_7
            // 
            this.hp_3_7.Enabled = false;
            this.hp_3_7.ForeColor = System.Drawing.Color.Black;
            this.hp_3_7.Location = new System.Drawing.Point(601, 131);
            this.hp_3_7.Name = "hp_3_7";
            this.hp_3_7.Size = new System.Drawing.Size(25, 25);
            this.hp_3_7.TabIndex = 239;
            this.hp_3_7.UseVisualStyleBackColor = true;
            // 
            // hp_3_6
            // 
            this.hp_3_6.Enabled = false;
            this.hp_3_6.ForeColor = System.Drawing.Color.Black;
            this.hp_3_6.Location = new System.Drawing.Point(570, 131);
            this.hp_3_6.Name = "hp_3_6";
            this.hp_3_6.Size = new System.Drawing.Size(25, 25);
            this.hp_3_6.TabIndex = 238;
            this.hp_3_6.UseVisualStyleBackColor = true;
            // 
            // hp_3_5
            // 
            this.hp_3_5.Enabled = false;
            this.hp_3_5.ForeColor = System.Drawing.Color.Black;
            this.hp_3_5.Location = new System.Drawing.Point(539, 131);
            this.hp_3_5.Name = "hp_3_5";
            this.hp_3_5.Size = new System.Drawing.Size(25, 25);
            this.hp_3_5.TabIndex = 237;
            this.hp_3_5.UseVisualStyleBackColor = true;
            // 
            // hp_3_4
            // 
            this.hp_3_4.Enabled = false;
            this.hp_3_4.ForeColor = System.Drawing.Color.Black;
            this.hp_3_4.Location = new System.Drawing.Point(508, 131);
            this.hp_3_4.Name = "hp_3_4";
            this.hp_3_4.Size = new System.Drawing.Size(25, 25);
            this.hp_3_4.TabIndex = 236;
            this.hp_3_4.UseVisualStyleBackColor = true;
            // 
            // hp_3_3
            // 
            this.hp_3_3.Enabled = false;
            this.hp_3_3.ForeColor = System.Drawing.Color.Black;
            this.hp_3_3.Location = new System.Drawing.Point(477, 131);
            this.hp_3_3.Name = "hp_3_3";
            this.hp_3_3.Size = new System.Drawing.Size(25, 25);
            this.hp_3_3.TabIndex = 235;
            this.hp_3_3.UseVisualStyleBackColor = true;
            // 
            // hp_3_2
            // 
            this.hp_3_2.Enabled = false;
            this.hp_3_2.ForeColor = System.Drawing.Color.Black;
            this.hp_3_2.Location = new System.Drawing.Point(446, 131);
            this.hp_3_2.Name = "hp_3_2";
            this.hp_3_2.Size = new System.Drawing.Size(25, 25);
            this.hp_3_2.TabIndex = 234;
            this.hp_3_2.UseVisualStyleBackColor = true;
            // 
            // hp_3_1
            // 
            this.hp_3_1.Enabled = false;
            this.hp_3_1.ForeColor = System.Drawing.Color.Black;
            this.hp_3_1.Location = new System.Drawing.Point(415, 131);
            this.hp_3_1.Name = "hp_3_1";
            this.hp_3_1.Size = new System.Drawing.Size(25, 25);
            this.hp_3_1.TabIndex = 233;
            this.hp_3_1.UseVisualStyleBackColor = true;
            // 
            // hp_3_0
            // 
            this.hp_3_0.Enabled = false;
            this.hp_3_0.ForeColor = System.Drawing.Color.Black;
            this.hp_3_0.Location = new System.Drawing.Point(384, 131);
            this.hp_3_0.Name = "hp_3_0";
            this.hp_3_0.Size = new System.Drawing.Size(25, 25);
            this.hp_3_0.TabIndex = 232;
            this.hp_3_0.UseVisualStyleBackColor = true;
            // 
            // hp_2_9
            // 
            this.hp_2_9.Enabled = false;
            this.hp_2_9.ForeColor = System.Drawing.Color.Black;
            this.hp_2_9.Location = new System.Drawing.Point(663, 99);
            this.hp_2_9.Name = "hp_2_9";
            this.hp_2_9.Size = new System.Drawing.Size(25, 25);
            this.hp_2_9.TabIndex = 231;
            this.hp_2_9.UseVisualStyleBackColor = true;
            // 
            // hp_2_8
            // 
            this.hp_2_8.Enabled = false;
            this.hp_2_8.ForeColor = System.Drawing.Color.Black;
            this.hp_2_8.Location = new System.Drawing.Point(632, 99);
            this.hp_2_8.Name = "hp_2_8";
            this.hp_2_8.Size = new System.Drawing.Size(25, 25);
            this.hp_2_8.TabIndex = 230;
            this.hp_2_8.UseVisualStyleBackColor = true;
            // 
            // hp_2_7
            // 
            this.hp_2_7.Enabled = false;
            this.hp_2_7.ForeColor = System.Drawing.Color.Black;
            this.hp_2_7.Location = new System.Drawing.Point(601, 99);
            this.hp_2_7.Name = "hp_2_7";
            this.hp_2_7.Size = new System.Drawing.Size(25, 25);
            this.hp_2_7.TabIndex = 229;
            this.hp_2_7.UseVisualStyleBackColor = true;
            // 
            // hp_2_6
            // 
            this.hp_2_6.Enabled = false;
            this.hp_2_6.ForeColor = System.Drawing.Color.Black;
            this.hp_2_6.Location = new System.Drawing.Point(570, 99);
            this.hp_2_6.Name = "hp_2_6";
            this.hp_2_6.Size = new System.Drawing.Size(25, 25);
            this.hp_2_6.TabIndex = 228;
            this.hp_2_6.UseVisualStyleBackColor = true;
            // 
            // hp_2_5
            // 
            this.hp_2_5.Enabled = false;
            this.hp_2_5.ForeColor = System.Drawing.Color.Black;
            this.hp_2_5.Location = new System.Drawing.Point(539, 99);
            this.hp_2_5.Name = "hp_2_5";
            this.hp_2_5.Size = new System.Drawing.Size(25, 25);
            this.hp_2_5.TabIndex = 227;
            this.hp_2_5.UseVisualStyleBackColor = true;
            // 
            // hp_2_4
            // 
            this.hp_2_4.Enabled = false;
            this.hp_2_4.ForeColor = System.Drawing.Color.Black;
            this.hp_2_4.Location = new System.Drawing.Point(508, 99);
            this.hp_2_4.Name = "hp_2_4";
            this.hp_2_4.Size = new System.Drawing.Size(25, 25);
            this.hp_2_4.TabIndex = 226;
            this.hp_2_4.UseVisualStyleBackColor = true;
            // 
            // hp_2_3
            // 
            this.hp_2_3.Enabled = false;
            this.hp_2_3.ForeColor = System.Drawing.Color.Black;
            this.hp_2_3.Location = new System.Drawing.Point(477, 99);
            this.hp_2_3.Name = "hp_2_3";
            this.hp_2_3.Size = new System.Drawing.Size(25, 25);
            this.hp_2_3.TabIndex = 225;
            this.hp_2_3.UseVisualStyleBackColor = true;
            // 
            // hp_2_2
            // 
            this.hp_2_2.Enabled = false;
            this.hp_2_2.ForeColor = System.Drawing.Color.Black;
            this.hp_2_2.Location = new System.Drawing.Point(446, 99);
            this.hp_2_2.Name = "hp_2_2";
            this.hp_2_2.Size = new System.Drawing.Size(25, 25);
            this.hp_2_2.TabIndex = 224;
            this.hp_2_2.UseVisualStyleBackColor = true;
            // 
            // hp_2_1
            // 
            this.hp_2_1.Enabled = false;
            this.hp_2_1.ForeColor = System.Drawing.Color.Black;
            this.hp_2_1.Location = new System.Drawing.Point(415, 99);
            this.hp_2_1.Name = "hp_2_1";
            this.hp_2_1.Size = new System.Drawing.Size(25, 25);
            this.hp_2_1.TabIndex = 223;
            this.hp_2_1.UseVisualStyleBackColor = true;
            // 
            // hp_2_0
            // 
            this.hp_2_0.Enabled = false;
            this.hp_2_0.ForeColor = System.Drawing.Color.Black;
            this.hp_2_0.Location = new System.Drawing.Point(384, 99);
            this.hp_2_0.Name = "hp_2_0";
            this.hp_2_0.Size = new System.Drawing.Size(25, 25);
            this.hp_2_0.TabIndex = 222;
            this.hp_2_0.UseVisualStyleBackColor = true;
            // 
            // hp_1_9
            // 
            this.hp_1_9.Enabled = false;
            this.hp_1_9.ForeColor = System.Drawing.Color.Black;
            this.hp_1_9.Location = new System.Drawing.Point(663, 67);
            this.hp_1_9.Name = "hp_1_9";
            this.hp_1_9.Size = new System.Drawing.Size(25, 25);
            this.hp_1_9.TabIndex = 221;
            this.hp_1_9.UseVisualStyleBackColor = true;
            // 
            // hp_1_8
            // 
            this.hp_1_8.Enabled = false;
            this.hp_1_8.ForeColor = System.Drawing.Color.Black;
            this.hp_1_8.Location = new System.Drawing.Point(632, 67);
            this.hp_1_8.Name = "hp_1_8";
            this.hp_1_8.Size = new System.Drawing.Size(25, 25);
            this.hp_1_8.TabIndex = 220;
            this.hp_1_8.UseVisualStyleBackColor = true;
            // 
            // hp_1_7
            // 
            this.hp_1_7.Enabled = false;
            this.hp_1_7.ForeColor = System.Drawing.Color.Black;
            this.hp_1_7.Location = new System.Drawing.Point(601, 67);
            this.hp_1_7.Name = "hp_1_7";
            this.hp_1_7.Size = new System.Drawing.Size(25, 25);
            this.hp_1_7.TabIndex = 219;
            this.hp_1_7.UseVisualStyleBackColor = true;
            // 
            // hp_1_6
            // 
            this.hp_1_6.Enabled = false;
            this.hp_1_6.ForeColor = System.Drawing.Color.Black;
            this.hp_1_6.Location = new System.Drawing.Point(570, 67);
            this.hp_1_6.Name = "hp_1_6";
            this.hp_1_6.Size = new System.Drawing.Size(25, 25);
            this.hp_1_6.TabIndex = 218;
            this.hp_1_6.UseVisualStyleBackColor = true;
            // 
            // hp_1_5
            // 
            this.hp_1_5.Enabled = false;
            this.hp_1_5.ForeColor = System.Drawing.Color.Black;
            this.hp_1_5.Location = new System.Drawing.Point(539, 67);
            this.hp_1_5.Name = "hp_1_5";
            this.hp_1_5.Size = new System.Drawing.Size(25, 25);
            this.hp_1_5.TabIndex = 217;
            this.hp_1_5.UseVisualStyleBackColor = true;
            // 
            // hp_1_4
            // 
            this.hp_1_4.Enabled = false;
            this.hp_1_4.ForeColor = System.Drawing.Color.Black;
            this.hp_1_4.Location = new System.Drawing.Point(508, 67);
            this.hp_1_4.Name = "hp_1_4";
            this.hp_1_4.Size = new System.Drawing.Size(25, 25);
            this.hp_1_4.TabIndex = 216;
            this.hp_1_4.UseVisualStyleBackColor = true;
            // 
            // hp_1_3
            // 
            this.hp_1_3.Enabled = false;
            this.hp_1_3.ForeColor = System.Drawing.Color.Black;
            this.hp_1_3.Location = new System.Drawing.Point(477, 67);
            this.hp_1_3.Name = "hp_1_3";
            this.hp_1_3.Size = new System.Drawing.Size(25, 25);
            this.hp_1_3.TabIndex = 215;
            this.hp_1_3.UseVisualStyleBackColor = true;
            // 
            // hp_1_2
            // 
            this.hp_1_2.Enabled = false;
            this.hp_1_2.ForeColor = System.Drawing.Color.Black;
            this.hp_1_2.Location = new System.Drawing.Point(446, 67);
            this.hp_1_2.Name = "hp_1_2";
            this.hp_1_2.Size = new System.Drawing.Size(25, 25);
            this.hp_1_2.TabIndex = 214;
            this.hp_1_2.UseVisualStyleBackColor = true;
            // 
            // hp_1_1
            // 
            this.hp_1_1.Enabled = false;
            this.hp_1_1.ForeColor = System.Drawing.Color.Black;
            this.hp_1_1.Location = new System.Drawing.Point(415, 67);
            this.hp_1_1.Name = "hp_1_1";
            this.hp_1_1.Size = new System.Drawing.Size(25, 25);
            this.hp_1_1.TabIndex = 213;
            this.hp_1_1.UseVisualStyleBackColor = true;
            // 
            // hp_1_0
            // 
            this.hp_1_0.Enabled = false;
            this.hp_1_0.ForeColor = System.Drawing.Color.Black;
            this.hp_1_0.Location = new System.Drawing.Point(384, 67);
            this.hp_1_0.Name = "hp_1_0";
            this.hp_1_0.Size = new System.Drawing.Size(25, 25);
            this.hp_1_0.TabIndex = 212;
            this.hp_1_0.UseVisualStyleBackColor = true;
            // 
            // hp_0_9
            // 
            this.hp_0_9.Enabled = false;
            this.hp_0_9.ForeColor = System.Drawing.Color.Black;
            this.hp_0_9.Location = new System.Drawing.Point(663, 35);
            this.hp_0_9.Name = "hp_0_9";
            this.hp_0_9.Size = new System.Drawing.Size(25, 25);
            this.hp_0_9.TabIndex = 211;
            this.hp_0_9.UseVisualStyleBackColor = true;
            // 
            // hp_0_8
            // 
            this.hp_0_8.Enabled = false;
            this.hp_0_8.ForeColor = System.Drawing.Color.Black;
            this.hp_0_8.Location = new System.Drawing.Point(632, 35);
            this.hp_0_8.Name = "hp_0_8";
            this.hp_0_8.Size = new System.Drawing.Size(25, 25);
            this.hp_0_8.TabIndex = 210;
            this.hp_0_8.UseVisualStyleBackColor = true;
            // 
            // hp_0_7
            // 
            this.hp_0_7.Enabled = false;
            this.hp_0_7.ForeColor = System.Drawing.Color.Black;
            this.hp_0_7.Location = new System.Drawing.Point(601, 35);
            this.hp_0_7.Name = "hp_0_7";
            this.hp_0_7.Size = new System.Drawing.Size(25, 25);
            this.hp_0_7.TabIndex = 209;
            this.hp_0_7.UseVisualStyleBackColor = true;
            // 
            // hp_0_6
            // 
            this.hp_0_6.Enabled = false;
            this.hp_0_6.ForeColor = System.Drawing.Color.Black;
            this.hp_0_6.Location = new System.Drawing.Point(570, 35);
            this.hp_0_6.Name = "hp_0_6";
            this.hp_0_6.Size = new System.Drawing.Size(25, 25);
            this.hp_0_6.TabIndex = 208;
            this.hp_0_6.UseVisualStyleBackColor = true;
            // 
            // hp_0_5
            // 
            this.hp_0_5.Enabled = false;
            this.hp_0_5.ForeColor = System.Drawing.Color.Black;
            this.hp_0_5.Location = new System.Drawing.Point(539, 35);
            this.hp_0_5.Name = "hp_0_5";
            this.hp_0_5.Size = new System.Drawing.Size(25, 25);
            this.hp_0_5.TabIndex = 207;
            this.hp_0_5.UseVisualStyleBackColor = true;
            // 
            // hp_0_4
            // 
            this.hp_0_4.Enabled = false;
            this.hp_0_4.ForeColor = System.Drawing.Color.Black;
            this.hp_0_4.Location = new System.Drawing.Point(508, 35);
            this.hp_0_4.Name = "hp_0_4";
            this.hp_0_4.Size = new System.Drawing.Size(25, 25);
            this.hp_0_4.TabIndex = 206;
            this.hp_0_4.UseVisualStyleBackColor = true;
            // 
            // hp_0_3
            // 
            this.hp_0_3.Enabled = false;
            this.hp_0_3.ForeColor = System.Drawing.Color.Black;
            this.hp_0_3.Location = new System.Drawing.Point(477, 35);
            this.hp_0_3.Name = "hp_0_3";
            this.hp_0_3.Size = new System.Drawing.Size(25, 25);
            this.hp_0_3.TabIndex = 205;
            this.hp_0_3.UseVisualStyleBackColor = true;
            // 
            // hp_0_2
            // 
            this.hp_0_2.Enabled = false;
            this.hp_0_2.ForeColor = System.Drawing.Color.Black;
            this.hp_0_2.Location = new System.Drawing.Point(446, 35);
            this.hp_0_2.Name = "hp_0_2";
            this.hp_0_2.Size = new System.Drawing.Size(25, 25);
            this.hp_0_2.TabIndex = 204;
            this.hp_0_2.UseVisualStyleBackColor = true;
            // 
            // hp_0_1
            // 
            this.hp_0_1.Enabled = false;
            this.hp_0_1.ForeColor = System.Drawing.Color.Black;
            this.hp_0_1.Location = new System.Drawing.Point(415, 35);
            this.hp_0_1.Name = "hp_0_1";
            this.hp_0_1.Size = new System.Drawing.Size(25, 25);
            this.hp_0_1.TabIndex = 203;
            this.hp_0_1.UseVisualStyleBackColor = true;
            // 
            // hp_0_0
            // 
            this.hp_0_0.Enabled = false;
            this.hp_0_0.ForeColor = System.Drawing.Color.Black;
            this.hp_0_0.Location = new System.Drawing.Point(384, 35);
            this.hp_0_0.Name = "hp_0_0";
            this.hp_0_0.Size = new System.Drawing.Size(25, 25);
            this.hp_0_0.TabIndex = 202;
            this.hp_0_0.UseVisualStyleBackColor = true;
            // 
            // GameForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(701, 359);
            this.Controls.Add(this.hp_9_9);
            this.Controls.Add(this.hp_9_8);
            this.Controls.Add(this.hp_9_7);
            this.Controls.Add(this.hp_9_6);
            this.Controls.Add(this.hp_9_5);
            this.Controls.Add(this.hp_9_4);
            this.Controls.Add(this.hp_9_3);
            this.Controls.Add(this.hp_9_2);
            this.Controls.Add(this.hp_9_1);
            this.Controls.Add(this.hp_9_0);
            this.Controls.Add(this.hp_8_9);
            this.Controls.Add(this.hp_8_8);
            this.Controls.Add(this.hp_8_7);
            this.Controls.Add(this.hp_8_6);
            this.Controls.Add(this.hp_8_5);
            this.Controls.Add(this.hp_8_4);
            this.Controls.Add(this.hp_8_3);
            this.Controls.Add(this.hp_8_2);
            this.Controls.Add(this.hp_8_1);
            this.Controls.Add(this.hp_8_0);
            this.Controls.Add(this.hp_7_9);
            this.Controls.Add(this.hp_7_8);
            this.Controls.Add(this.hp_7_7);
            this.Controls.Add(this.hp_7_6);
            this.Controls.Add(this.hp_7_5);
            this.Controls.Add(this.hp_7_4);
            this.Controls.Add(this.hp_7_3);
            this.Controls.Add(this.hp_7_2);
            this.Controls.Add(this.hp_7_1);
            this.Controls.Add(this.hp_7_0);
            this.Controls.Add(this.hp_6_9);
            this.Controls.Add(this.hp_6_8);
            this.Controls.Add(this.hp_6_7);
            this.Controls.Add(this.hp_6_6);
            this.Controls.Add(this.hp_6_5);
            this.Controls.Add(this.hp_6_4);
            this.Controls.Add(this.hp_6_3);
            this.Controls.Add(this.hp_6_2);
            this.Controls.Add(this.hp_6_1);
            this.Controls.Add(this.hp_6_0);
            this.Controls.Add(this.hp_5_9);
            this.Controls.Add(this.hp_5_8);
            this.Controls.Add(this.hp_5_7);
            this.Controls.Add(this.hp_5_6);
            this.Controls.Add(this.hp_5_5);
            this.Controls.Add(this.hp_5_4);
            this.Controls.Add(this.hp_5_3);
            this.Controls.Add(this.hp_5_2);
            this.Controls.Add(this.hp_5_1);
            this.Controls.Add(this.hp_5_0);
            this.Controls.Add(this.hp_4_9);
            this.Controls.Add(this.hp_4_8);
            this.Controls.Add(this.hp_4_7);
            this.Controls.Add(this.hp_4_6);
            this.Controls.Add(this.hp_4_5);
            this.Controls.Add(this.hp_4_4);
            this.Controls.Add(this.hp_4_3);
            this.Controls.Add(this.hp_4_2);
            this.Controls.Add(this.hp_4_1);
            this.Controls.Add(this.hp_4_0);
            this.Controls.Add(this.hp_3_9);
            this.Controls.Add(this.hp_3_8);
            this.Controls.Add(this.hp_3_7);
            this.Controls.Add(this.hp_3_6);
            this.Controls.Add(this.hp_3_5);
            this.Controls.Add(this.hp_3_4);
            this.Controls.Add(this.hp_3_3);
            this.Controls.Add(this.hp_3_2);
            this.Controls.Add(this.hp_3_1);
            this.Controls.Add(this.hp_3_0);
            this.Controls.Add(this.hp_2_9);
            this.Controls.Add(this.hp_2_8);
            this.Controls.Add(this.hp_2_7);
            this.Controls.Add(this.hp_2_6);
            this.Controls.Add(this.hp_2_5);
            this.Controls.Add(this.hp_2_4);
            this.Controls.Add(this.hp_2_3);
            this.Controls.Add(this.hp_2_2);
            this.Controls.Add(this.hp_2_1);
            this.Controls.Add(this.hp_2_0);
            this.Controls.Add(this.hp_1_9);
            this.Controls.Add(this.hp_1_8);
            this.Controls.Add(this.hp_1_7);
            this.Controls.Add(this.hp_1_6);
            this.Controls.Add(this.hp_1_5);
            this.Controls.Add(this.hp_1_4);
            this.Controls.Add(this.hp_1_3);
            this.Controls.Add(this.hp_1_2);
            this.Controls.Add(this.hp_1_1);
            this.Controls.Add(this.hp_1_0);
            this.Controls.Add(this.hp_0_9);
            this.Controls.Add(this.hp_0_8);
            this.Controls.Add(this.hp_0_7);
            this.Controls.Add(this.hp_0_6);
            this.Controls.Add(this.hp_0_5);
            this.Controls.Add(this.hp_0_4);
            this.Controls.Add(this.hp_0_3);
            this.Controls.Add(this.hp_0_2);
            this.Controls.Add(this.hp_0_1);
            this.Controls.Add(this.hp_0_0);
            this.Controls.Add(this.hp_label);
            this.Controls.Add(this.ai_label);
            this.Controls.Add(this.ai_9_9);
            this.Controls.Add(this.ai_9_8);
            this.Controls.Add(this.ai_9_7);
            this.Controls.Add(this.ai_9_6);
            this.Controls.Add(this.ai_9_5);
            this.Controls.Add(this.ai_9_4);
            this.Controls.Add(this.ai_9_3);
            this.Controls.Add(this.ai_9_2);
            this.Controls.Add(this.ai_9_1);
            this.Controls.Add(this.ai_9_0);
            this.Controls.Add(this.ai_8_9);
            this.Controls.Add(this.ai_8_8);
            this.Controls.Add(this.ai_8_7);
            this.Controls.Add(this.ai_8_6);
            this.Controls.Add(this.ai_8_5);
            this.Controls.Add(this.ai_8_4);
            this.Controls.Add(this.ai_8_3);
            this.Controls.Add(this.ai_8_2);
            this.Controls.Add(this.ai_8_1);
            this.Controls.Add(this.ai_8_0);
            this.Controls.Add(this.ai_7_9);
            this.Controls.Add(this.ai_7_8);
            this.Controls.Add(this.ai_7_7);
            this.Controls.Add(this.ai_7_6);
            this.Controls.Add(this.ai_7_5);
            this.Controls.Add(this.ai_7_4);
            this.Controls.Add(this.ai_7_3);
            this.Controls.Add(this.ai_7_2);
            this.Controls.Add(this.ai_7_1);
            this.Controls.Add(this.ai_7_0);
            this.Controls.Add(this.ai_6_9);
            this.Controls.Add(this.ai_6_8);
            this.Controls.Add(this.ai_6_7);
            this.Controls.Add(this.ai_6_6);
            this.Controls.Add(this.ai_6_5);
            this.Controls.Add(this.ai_6_4);
            this.Controls.Add(this.ai_6_3);
            this.Controls.Add(this.ai_6_2);
            this.Controls.Add(this.ai_6_1);
            this.Controls.Add(this.ai_6_0);
            this.Controls.Add(this.ai_5_9);
            this.Controls.Add(this.ai_5_8);
            this.Controls.Add(this.ai_5_7);
            this.Controls.Add(this.ai_5_6);
            this.Controls.Add(this.ai_5_5);
            this.Controls.Add(this.ai_5_4);
            this.Controls.Add(this.ai_5_3);
            this.Controls.Add(this.ai_5_2);
            this.Controls.Add(this.ai_5_1);
            this.Controls.Add(this.ai_5_0);
            this.Controls.Add(this.ai_4_9);
            this.Controls.Add(this.ai_4_8);
            this.Controls.Add(this.ai_4_7);
            this.Controls.Add(this.ai_4_6);
            this.Controls.Add(this.ai_4_5);
            this.Controls.Add(this.ai_4_4);
            this.Controls.Add(this.ai_4_3);
            this.Controls.Add(this.ai_4_2);
            this.Controls.Add(this.ai_4_1);
            this.Controls.Add(this.ai_4_0);
            this.Controls.Add(this.ai_3_9);
            this.Controls.Add(this.ai_3_8);
            this.Controls.Add(this.ai_3_7);
            this.Controls.Add(this.ai_3_6);
            this.Controls.Add(this.ai_3_5);
            this.Controls.Add(this.ai_3_4);
            this.Controls.Add(this.ai_3_3);
            this.Controls.Add(this.ai_3_2);
            this.Controls.Add(this.ai_3_1);
            this.Controls.Add(this.ai_3_0);
            this.Controls.Add(this.ai_2_9);
            this.Controls.Add(this.ai_2_8);
            this.Controls.Add(this.ai_2_7);
            this.Controls.Add(this.ai_2_6);
            this.Controls.Add(this.ai_2_5);
            this.Controls.Add(this.ai_2_4);
            this.Controls.Add(this.ai_2_3);
            this.Controls.Add(this.ai_2_2);
            this.Controls.Add(this.ai_2_1);
            this.Controls.Add(this.ai_2_0);
            this.Controls.Add(this.ai_1_9);
            this.Controls.Add(this.ai_1_8);
            this.Controls.Add(this.ai_1_7);
            this.Controls.Add(this.ai_1_6);
            this.Controls.Add(this.ai_1_5);
            this.Controls.Add(this.ai_1_4);
            this.Controls.Add(this.ai_1_3);
            this.Controls.Add(this.ai_1_2);
            this.Controls.Add(this.ai_1_1);
            this.Controls.Add(this.ai_1_0);
            this.Controls.Add(this.ai_0_9);
            this.Controls.Add(this.ai_0_8);
            this.Controls.Add(this.ai_0_7);
            this.Controls.Add(this.ai_0_6);
            this.Controls.Add(this.ai_0_5);
            this.Controls.Add(this.ai_0_4);
            this.Controls.Add(this.ai_0_3);
            this.Controls.Add(this.ai_0_2);
            this.Controls.Add(this.ai_0_1);
            this.Controls.Add(this.ai_0_0);
            this.Name = "GameForm";
            this.Text = "Potapanje brodova";
            this.Load += new System.EventHandler(this.gameForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ai_0_0;
        private System.Windows.Forms.Button ai_0_1;
        private System.Windows.Forms.Button ai_0_2;
        private System.Windows.Forms.Button ai_0_3;
        private System.Windows.Forms.Button ai_0_4;
        private System.Windows.Forms.Button ai_0_5;
        private System.Windows.Forms.Button ai_0_6;
        private System.Windows.Forms.Button ai_0_7;
        private System.Windows.Forms.Button ai_0_8;
        private System.Windows.Forms.Button ai_0_9;
        private System.Windows.Forms.Button ai_1_0;
        private System.Windows.Forms.Button ai_1_1;
        private System.Windows.Forms.Button ai_1_2;
        private System.Windows.Forms.Button ai_1_3;
        private System.Windows.Forms.Button ai_1_4;
        private System.Windows.Forms.Button ai_1_5;
        private System.Windows.Forms.Button ai_1_6;
        private System.Windows.Forms.Button ai_1_7;
        private System.Windows.Forms.Button ai_1_8;
        private System.Windows.Forms.Button ai_1_9;
        private System.Windows.Forms.Button ai_2_9;
        private System.Windows.Forms.Button ai_2_8;
        private System.Windows.Forms.Button ai_2_7;
        private System.Windows.Forms.Button ai_2_6;
        private System.Windows.Forms.Button ai_2_5;
        private System.Windows.Forms.Button ai_2_4;
        private System.Windows.Forms.Button ai_2_3;
        private System.Windows.Forms.Button ai_2_2;
        private System.Windows.Forms.Button ai_2_1;
        private System.Windows.Forms.Button ai_2_0;
        private System.Windows.Forms.Button ai_3_9;
        private System.Windows.Forms.Button ai_3_8;
        private System.Windows.Forms.Button ai_3_7;
        private System.Windows.Forms.Button ai_3_6;
        private System.Windows.Forms.Button ai_3_5;
        private System.Windows.Forms.Button ai_3_4;
        private System.Windows.Forms.Button ai_3_3;
        private System.Windows.Forms.Button ai_3_2;
        private System.Windows.Forms.Button ai_3_1;
        private System.Windows.Forms.Button ai_3_0;
        private System.Windows.Forms.Button ai_4_9;
        private System.Windows.Forms.Button ai_4_8;
        private System.Windows.Forms.Button ai_4_7;
        private System.Windows.Forms.Button ai_4_6;
        private System.Windows.Forms.Button ai_4_5;
        private System.Windows.Forms.Button ai_4_4;
        private System.Windows.Forms.Button ai_4_3;
        private System.Windows.Forms.Button ai_4_2;
        private System.Windows.Forms.Button ai_4_1;
        private System.Windows.Forms.Button ai_4_0;
        private System.Windows.Forms.Button ai_5_9;
        private System.Windows.Forms.Button ai_5_8;
        private System.Windows.Forms.Button ai_5_7;
        private System.Windows.Forms.Button ai_5_6;
        private System.Windows.Forms.Button ai_5_5;
        private System.Windows.Forms.Button ai_5_4;
        private System.Windows.Forms.Button ai_5_3;
        private System.Windows.Forms.Button ai_5_2;
        private System.Windows.Forms.Button ai_5_1;
        private System.Windows.Forms.Button ai_5_0;
        private System.Windows.Forms.Button ai_6_9;
        private System.Windows.Forms.Button ai_6_8;
        private System.Windows.Forms.Button ai_6_7;
        private System.Windows.Forms.Button ai_6_6;
        private System.Windows.Forms.Button ai_6_5;
        private System.Windows.Forms.Button ai_6_4;
        private System.Windows.Forms.Button ai_6_3;
        private System.Windows.Forms.Button ai_6_2;
        private System.Windows.Forms.Button ai_6_1;
        private System.Windows.Forms.Button ai_6_0;
        private System.Windows.Forms.Button ai_7_9;
        private System.Windows.Forms.Button ai_7_8;
        private System.Windows.Forms.Button ai_7_7;
        private System.Windows.Forms.Button ai_7_6;
        private System.Windows.Forms.Button ai_7_5;
        private System.Windows.Forms.Button ai_7_4;
        private System.Windows.Forms.Button ai_7_3;
        private System.Windows.Forms.Button ai_7_2;
        private System.Windows.Forms.Button ai_7_1;
        private System.Windows.Forms.Button ai_7_0;
        private System.Windows.Forms.Button ai_8_9;
        private System.Windows.Forms.Button ai_8_8;
        private System.Windows.Forms.Button ai_8_7;
        private System.Windows.Forms.Button ai_8_6;
        private System.Windows.Forms.Button ai_8_5;
        private System.Windows.Forms.Button ai_8_4;
        private System.Windows.Forms.Button ai_8_3;
        private System.Windows.Forms.Button ai_8_2;
        private System.Windows.Forms.Button ai_8_1;
        private System.Windows.Forms.Button ai_8_0;
        private System.Windows.Forms.Button ai_9_9;
        private System.Windows.Forms.Button ai_9_8;
        private System.Windows.Forms.Button ai_9_7;
        private System.Windows.Forms.Button ai_9_6;
        private System.Windows.Forms.Button ai_9_5;
        private System.Windows.Forms.Button ai_9_4;
        private System.Windows.Forms.Button ai_9_3;
        private System.Windows.Forms.Button ai_9_2;
        private System.Windows.Forms.Button ai_9_1;
        private System.Windows.Forms.Button ai_9_0;
        private System.Windows.Forms.Label ai_label;
        private System.Windows.Forms.Label hp_label;
        private System.Windows.Forms.Button hp_9_9;
        private System.Windows.Forms.Button hp_9_8;
        private System.Windows.Forms.Button hp_9_7;
        private System.Windows.Forms.Button hp_9_6;
        private System.Windows.Forms.Button hp_9_5;
        private System.Windows.Forms.Button hp_9_4;
        private System.Windows.Forms.Button hp_9_3;
        private System.Windows.Forms.Button hp_9_2;
        private System.Windows.Forms.Button hp_9_1;
        private System.Windows.Forms.Button hp_9_0;
        private System.Windows.Forms.Button hp_8_9;
        private System.Windows.Forms.Button hp_8_8;
        private System.Windows.Forms.Button hp_8_7;
        private System.Windows.Forms.Button hp_8_6;
        private System.Windows.Forms.Button hp_8_5;
        private System.Windows.Forms.Button hp_8_4;
        private System.Windows.Forms.Button hp_8_3;
        private System.Windows.Forms.Button hp_8_2;
        private System.Windows.Forms.Button hp_8_1;
        private System.Windows.Forms.Button hp_8_0;
        private System.Windows.Forms.Button hp_7_9;
        private System.Windows.Forms.Button hp_7_8;
        private System.Windows.Forms.Button hp_7_7;
        private System.Windows.Forms.Button hp_7_6;
        private System.Windows.Forms.Button hp_7_5;
        private System.Windows.Forms.Button hp_7_4;
        private System.Windows.Forms.Button hp_7_3;
        private System.Windows.Forms.Button hp_7_2;
        private System.Windows.Forms.Button hp_7_1;
        private System.Windows.Forms.Button hp_7_0;
        private System.Windows.Forms.Button hp_6_9;
        private System.Windows.Forms.Button hp_6_8;
        private System.Windows.Forms.Button hp_6_7;
        private System.Windows.Forms.Button hp_6_6;
        private System.Windows.Forms.Button hp_6_5;
        private System.Windows.Forms.Button hp_6_4;
        private System.Windows.Forms.Button hp_6_3;
        private System.Windows.Forms.Button hp_6_2;
        private System.Windows.Forms.Button hp_6_1;
        private System.Windows.Forms.Button hp_6_0;
        private System.Windows.Forms.Button hp_5_9;
        private System.Windows.Forms.Button hp_5_8;
        private System.Windows.Forms.Button hp_5_7;
        private System.Windows.Forms.Button hp_5_6;
        private System.Windows.Forms.Button hp_5_5;
        private System.Windows.Forms.Button hp_5_4;
        private System.Windows.Forms.Button hp_5_3;
        private System.Windows.Forms.Button hp_5_2;
        private System.Windows.Forms.Button hp_5_1;
        private System.Windows.Forms.Button hp_5_0;
        private System.Windows.Forms.Button hp_4_9;
        private System.Windows.Forms.Button hp_4_8;
        private System.Windows.Forms.Button hp_4_7;
        private System.Windows.Forms.Button hp_4_6;
        private System.Windows.Forms.Button hp_4_5;
        private System.Windows.Forms.Button hp_4_4;
        private System.Windows.Forms.Button hp_4_3;
        private System.Windows.Forms.Button hp_4_2;
        private System.Windows.Forms.Button hp_4_1;
        private System.Windows.Forms.Button hp_4_0;
        private System.Windows.Forms.Button hp_3_9;
        private System.Windows.Forms.Button hp_3_8;
        private System.Windows.Forms.Button hp_3_7;
        private System.Windows.Forms.Button hp_3_6;
        private System.Windows.Forms.Button hp_3_5;
        private System.Windows.Forms.Button hp_3_4;
        private System.Windows.Forms.Button hp_3_3;
        private System.Windows.Forms.Button hp_3_2;
        private System.Windows.Forms.Button hp_3_1;
        private System.Windows.Forms.Button hp_3_0;
        private System.Windows.Forms.Button hp_2_9;
        private System.Windows.Forms.Button hp_2_8;
        private System.Windows.Forms.Button hp_2_7;
        private System.Windows.Forms.Button hp_2_6;
        private System.Windows.Forms.Button hp_2_5;
        private System.Windows.Forms.Button hp_2_4;
        private System.Windows.Forms.Button hp_2_3;
        private System.Windows.Forms.Button hp_2_2;
        private System.Windows.Forms.Button hp_2_1;
        private System.Windows.Forms.Button hp_2_0;
        private System.Windows.Forms.Button hp_1_9;
        private System.Windows.Forms.Button hp_1_8;
        private System.Windows.Forms.Button hp_1_7;
        private System.Windows.Forms.Button hp_1_6;
        private System.Windows.Forms.Button hp_1_5;
        private System.Windows.Forms.Button hp_1_4;
        private System.Windows.Forms.Button hp_1_3;
        private System.Windows.Forms.Button hp_1_2;
        private System.Windows.Forms.Button hp_1_1;
        private System.Windows.Forms.Button hp_1_0;
        private System.Windows.Forms.Button hp_0_9;
        private System.Windows.Forms.Button hp_0_8;
        private System.Windows.Forms.Button hp_0_7;
        private System.Windows.Forms.Button hp_0_6;
        private System.Windows.Forms.Button hp_0_5;
        private System.Windows.Forms.Button hp_0_4;
        private System.Windows.Forms.Button hp_0_3;
        private System.Windows.Forms.Button hp_0_2;
        private System.Windows.Forms.Button hp_0_1;
        private System.Windows.Forms.Button hp_0_0;
    }
}