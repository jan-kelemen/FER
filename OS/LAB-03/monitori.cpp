#include <array>
#include <chrono>
#include <condition_variable>
#include <csignal>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <mutex>
#include <string>
#include <thread>
#include <vector>

#include <unistd.h>

using namespace std;
using namespace std::this_thread;
using namespace std::chrono;

const int velicina = 5;

int ulaz = 0;
int izlaz = 0;
array<char,velicina> spremnik;

int broj_mjesta = velicina;
bool proizvodac_ceka = false;
bool potrosac_ceka = true;
mutex monitor;
array<condition_variable_any, 2> red;

enum Redovi
{
	PUN, PRAZAN
};

int broj_proizvodaca = 0;

void proizvodac(int tid, string poruka)
{	
	int i = 0;

	while(i != poruka.length() + 1)
	{
		monitor.lock();
		while(broj_mjesta == 0)
		{
			proizvodac_ceka = true;
			red[PUN].wait(monitor);
		}

		--broj_mjesta;
		spremnik[ulaz] = poruka[i];

		cout << "Proizvodac " << tid << " -> " << poruka[i] << endl;

		ulaz = (ulaz + 1) % velicina;
		++i;

		while(potrosac_ceka == true)
		{
			potrosac_ceka = false;
			red[PRAZAN].notify_all();
		}
		monitor.unlock();
		sleep_for(seconds(1));
	}
}

void potrosac()
{
	int primljeno_poruka = 0;
	string poruka;

	while(primljeno_poruka != broj_proizvodaca)
	{
		monitor.lock();
		while(broj_mjesta == velicina)
		{
			potrosac_ceka = true;
			red[PRAZAN].wait(monitor);
		}

		++broj_mjesta;
		char znak = spremnik[izlaz];
		poruka += znak;

		cout << "Potrosac <- " << znak << endl;

		if(znak == '\0')
			++primljeno_poruka;
		izlaz = (izlaz + 1) % velicina;

		while(proizvodac_ceka == true)
		{
			proizvodac_ceka = false;
			red[PUN].notify_all();
		}
		monitor.unlock();
	}

	cout << "Primljeno je " << poruka << endl;
}

int main(int argc, char *argv[])
{
	vector<thread> dretve;

	if(argc < 2)
		exit(1);
	else
		broj_proizvodaca = argc - 1;
	
	for(int i = 0; i < broj_proizvodaca; ++i)
		dretve.push_back(thread(proizvodac, i, string(argv[i + 1])));

	dretve.push_back(thread(potrosac));

	for(int i = 0; i < argc; ++i)
		dretve[i].join();
	return 0;
}
