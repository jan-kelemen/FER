#include <array>
#include <chrono>
#include <condition_variable>
#include <cstdlib>
#include <iostream>
#include <mutex>
#include <random>
#include <string>
#include <thread>
#include <vector>

#include <unistd.h>

using namespace std;
using namespace std::chrono;
using namespace std::this_thread;

int broj_auta_na_mostu = 0;
int smjer_na_mostu = 0;
mutex monitor;
array<condition_variable_any, 2> red;

void popni_se_na_most(int i, int smjer)
{
	monitor.lock();
	cout << "Auto " << i << " ceka na prelazak preko mosta, smjer " << smjer << endl;
	while(broj_auta_na_mostu  == 3 || ((broj_auta_na_mostu  > 0) && (smjer_na_mostu != smjer)))
		red[smjer].wait(monitor);
	++broj_auta_na_mostu ;
	smjer_na_mostu = smjer;
	monitor.unlock();
}

void sidi_s_mosta(int i, int smjer)
{
	monitor.lock();
	--broj_auta_na_mostu ;
	if(broj_auta_na_mostu == 0)
		red[1 - smjer].notify_all();
	else
		red[smjer].notify_all();
	cout  << "Auto " << i << " je presao most, smjer " << smjer << endl;
	monitor.unlock();
}

void automobil(int i, int smjer)
{
	popni_se_na_most(i, smjer);
	sleep_for(seconds(1));
	sidi_s_mosta(i, smjer);	
}


int main(int argc, char *argv[])
{
	if(argc != 2)
		exit(1);

	int broj_auti = atoi(argv[1]);
	vector<thread> dretve;
	random_device rd;

	for(int i = 0; i < broj_auti; ++i)
		dretve.push_back(thread(automobil, i, rd() % 2));

	for(int i = 0; i < broj_auti; ++i)
		dretve[i].join();

	return 0;
}
