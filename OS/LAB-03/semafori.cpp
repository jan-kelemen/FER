#include <chrono>
#include <csignal>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <string>
#include <thread>

#include <unistd.h>

#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <sys/wait.h>

using namespace std;
using namespace std::chrono;
using namespace std::this_thread;

const int velicina = 5;

int id_spremnik = -1;
int id_semafor = -1;

int *ulaz = nullptr;
int *izlaz = nullptr;
char *spremnik = nullptr;

enum Semafor
{
	PISI, PUN, PRAZAN
};

int broj_proizvodaca = 0;

int sem_op(unsigned short sem_num, short sem_op)
{
	struct sembuf sem_buf{sem_num, sem_op, 0};
	return semop(id_semafor, &sem_buf, 1);
}

void brisi(int signal = 0)
{
	shmdt(ulaz);
	shmdt(izlaz);
	shmdt(spremnik);
	shmctl(id_spremnik, IPC_RMID, nullptr);

	semctl(id_semafor, 0, IPC_RMID, 0);

	if(signal != 0)
		exit(0);
}

void proizvodac(int pid, string poruka)
{
	int i = 0;

	while(i != poruka.length() + 1)
	{
		sem_op(PUN, -1);
		sem_op(PISI, -1);

		spremnik[*ulaz] = poruka[i];
		*ulaz = (*ulaz + 1) % velicina;

		cout << "Proizvodac " << pid << " -> " << poruka[i] << endl;

		sem_op(PISI, 1);
		sem_op(PRAZAN, 1);

		sleep_for(seconds(1));
		++i;
	}
}

void potrosac()
{
	char znak;
	int primljeno_poruka = 0;
	string poruka;

	while(primljeno_poruka != broj_proizvodaca)
	{
		sem_op(PRAZAN, -1);
		znak = spremnik[*izlaz];
		poruka += znak;

		cout <<"Potrosac <- " << znak << endl;

		if(znak == '\0')
			++primljeno_poruka;
		*izlaz = (*izlaz + 1) % velicina;
		sem_op(PUN, 1);
	}

	cout << "Primljeno je " << poruka << endl;
}

int main(int argc, char *argv[])
{
	sigset(SIGINT, brisi);

	if(argc < 2)
		exit(1);
	else
	{
		broj_proizvodaca = argc - 1;
		id_spremnik = shmget(IPC_PRIVATE, 2*sizeof(int) + velicina, 0600);
		id_semafor = semget(IPC_PRIVATE, 3, 0600);

		if(id_spremnik == -1 || id_semafor == -1)
			exit(1);

		memset(shmat(id_spremnik, nullptr, 0), 0, 2*sizeof(int) + velicina);
		ulaz = static_cast<int*>(shmat(id_spremnik, nullptr, 0));
		izlaz = static_cast<int*>(shmat(id_spremnik, nullptr, 0)) + 1;
		spremnik = static_cast<char*>(shmat(id_spremnik, nullptr, 0)) + 2*sizeof(int);

		semctl(id_semafor, PISI, SETVAL, 1);
		semctl(id_semafor, PUN, SETVAL, velicina);
		semctl(id_semafor, PRAZAN, SETVAL, 0);
	}

	int cekaj = 1;
	for(int i = 0; i != broj_proizvodaca; ++i, ++cekaj)
		if(fork() == 0)
		{
			proizvodac(i, string(argv[i+1]));
			exit(0);
		}

	if(fork() == 0)
	{
		potrosac();
		exit(0);
	}

	while(cekaj--)
		wait(nullptr);

	brisi();

	return 0;
}
