# OS - Operacijski sustavi (Operating systems)
This folder contains solutions to lab tasks of Operating systems course.
Text of the lab tasks is in the .html files.

## Content
* LAB-01 - Interrupts and signals
* LAB-02 - Processes and threads
* LAB-03 - Synchronisation mechanisms
* LAB-04 - Paging simulation 
