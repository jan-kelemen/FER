﻿#include <algorithm>
#include <iostream>
#include <vector>

using namespace std;

const int velicina_spremnika{ 1000 };

struct Stat 
{
	int ukupno_blokova{ 1 };
	int slobodnih_blokova{ 1 };
	int zauzetih_blokova{ 0 };
} stat;

ostream &operator<<(ostream &out, const Stat &item)
{
	out << "S: [ukupno, slobodnih, zauzetih] = [" << item.ukupno_blokova << ", " << item.slobodnih_blokova << ", " << item.zauzetih_blokova << "]" << endl;
	return out;
}

class Blok
{
public:
	friend ostream &operator<<(ostream &, const Blok &);

	Blok() : pocetak{ 0 }, kraj{ velicina_spremnika }, velicina{ velicina_spremnika }, slobodan{ true }, prethodni{ nullptr }, sljedeci{ nullptr } {}
	Blok(Blok &stari, int v) : pocetak{ stari.pocetak + v + 12 }, kraj{ stari.kraj }, velicina{ stari.velicina - (v + 12) }, slobodan{ true }, \
		prethodni{ &stari }, sljedeci{ stari.sljedeci } 
	{
		stari.kraj = pocetak;
		stari.velicina = v + 12;
		stari.slobodan = false;
	}
	~Blok() { delete sljedeci; }
	Blok *podijeli(int);
	Blok *spoji();

	int pocetak;
	int kraj;
	int velicina;
	bool slobodan;

	Blok *prethodni;
	Blok *sljedeci;
};

Blok *Blok::podijeli(int v)
{
	if (velicina == v + 12 && slobodan == true)
	{
		slobodan = false;
		++stat.zauzetih_blokova;
		--stat.slobodnih_blokova;
		cout << "Dodieljen blok na adresi: " << pocetak << endl;
	}
	else if (velicina > v + 12 && slobodan == true)
	{
		sljedeci = new Blok(*this, v);
		
		++stat.ukupno_blokova;
		++stat.zauzetih_blokova;
		cout << "Dodijeljen blok na adresi: " << pocetak << endl;
	}
	else
	{
		cout << "Ne mogu stvoriti blok" << endl;
		return nullptr;
	}

	return this;
}

Blok *Blok::spoji()
{
	if (slobodan == true && sljedeci != nullptr && sljedeci->slobodan == true)
	{
		kraj = sljedeci->kraj;
		velicina += sljedeci->velicina;

		auto temp = sljedeci;
		sljedeci = sljedeci->sljedeci;
		temp->sljedeci = nullptr;
		delete temp;

		--stat.ukupno_blokova;
		--stat.slobodnih_blokova;
	}
	return this;
}

ostream &operator<<(ostream &out, const Blok &item)
{
	out << "[pocetak, kraj, velicina, slobodan] = [" << item.pocetak << ", " << item.kraj << ", " << item.velicina << ", " << item.slobodan << "]" << endl;
	return out;
}

class Spremnik
{
public:
	Spremnik() : pocetak{ new Blok } {}
	~Spremnik() { delete pocetak; }

	Blok *operator()(int);

	Blok *pocetak;
} spremnik;

Blok *Spremnik::operator()(int n)
{
	if (n < stat.ukupno_blokova)
	{
		auto it = pocetak;
		for (auto i = 0; i != n; ++i, it = it->sljedeci);
		return it;
	}
	else
	{
		return nullptr;
	}
}

ostream &operator<<(ostream &out, const Spremnik &item)
{
	out << stat;
	int i = 0;
	for(auto it = item.pocetak; it != nullptr; it = it->sljedeci, ++i)
		out << i <<": " << *it;
	return out;
}

bool usporedi(const Blok *lhs, const Blok *rhs)
{
	return lhs->velicina < rhs->velicina;
}

void dodijeli(int velicina)
{
	vector<Blok *> odgovarajuci_blokovi;
	for (auto it = spremnik.pocetak; it != nullptr; it = it->sljedeci)
		if (it->velicina >= velicina + 12 && it->slobodan == true)
			odgovarajuci_blokovi.push_back(it);
	sort(odgovarajuci_blokovi.begin(), odgovarajuci_blokovi.end(), usporedi);

	if (odgovarajuci_blokovi.empty() == false)
		odgovarajuci_blokovi[0]->podijeli(velicina);
	else
		cout << "Ne mogu stvoriti blok" << endl;
}

void oslobodi(int n)
{
	if (n == 0)
	{
		Blok *trenutni = spremnik.pocetak;
		if (trenutni->slobodan == false)
		{
			++stat.slobodnih_blokova;
			--stat.zauzetih_blokova;
			trenutni->slobodan = true;
			trenutni->spoji();
		}
	}
	else if (n < stat.ukupno_blokova)
	{
		Blok *prethodni = spremnik(n - 1);
		Blok *trenutni = prethodni->sljedeci;

		if (trenutni->slobodan == false)
		{
			++stat.slobodnih_blokova;
			--stat.zauzetih_blokova;

			trenutni->slobodan = true;
			trenutni->spoji();
			prethodni->spoji();
		}
	}
}

int main()
{
	while (1)
	{
		cout << spremnik << endl;
		char operacija;
		int broj;
		cout << "Unesi zahtjev: ";
		cin >> operacija >> broj;

		if (operacija == 'd')
			dodijeli(broj);
		else if (operacija == 'o')
			oslobodi(broj);
		else if (operacija == 'e')
			break;
	}
	return 0;
}