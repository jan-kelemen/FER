#include <algorithm>
#include <chrono>
#include <condition_variable>
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <random>
#include <thread>
#include <vector>

using namespace std;
using namespace std::chrono;
using namespace std::this_thread;

mutex mtx;
condition_variable_any red_generator;
condition_variable_any red_obrada;

int broj_okvira{ 10 };
int broj_zahtjeva;

random_device rd;
uniform_int_distribution<> dist(0, 9);

int trenutni_zahtjev{ -1 };
bool ima_pogodak{ false };

struct Element
{
	Element() : stranica{ -1 }, novi_element{ false }, vrijeme_ulaza{ high_resolution_clock::now() } {}
	Element(int str) : stranica{ str }, novi_element{ true }, vrijeme_ulaza{ high_resolution_clock::now() } {}
	int stranica;
	bool novi_element;
	high_resolution_clock::time_point vrijeme_ulaza;
};

bool operator<(const Element &lhs, const Element &rhs)
{
	return lhs.vrijeme_ulaza < rhs.vrijeme_ulaza;
}

ostream &operator<<(ostream &out, const Element &item)
{
	if (item.novi_element == true)
		out << " [" << item.stranica << "] ";
	else if (item.stranica == -1)
		out << "  -  ";
	else if (item.stranica == trenutni_zahtjev)
		out << " (" << item.stranica << ") ";
	else
		out << "  " << item.stranica << "  ";
	return out;
}

vector<int> zahtjevi;
vector<Element> spremnik(broj_okvira);

void ispis_stanja()
{
	for (auto i = 0; i != zahtjevi.size(); ++i)
		cout << zahtjevi[i] << " ";
	for (auto i = zahtjevi.size(); i != broj_zahtjeva; ++i)
		cout << "  ";
	cout << " " << trenutni_zahtjev << " ";
	for (auto i : spremnik)
		cout << i;
	if (ima_pogodak == true)
		cout << "#pogodak";
	cout << endl;
}

void generator_zahtjeva()
{
	mtx.lock();
	
	for (int i = 0; i != broj_zahtjeva; ++i)
		zahtjevi.push_back(dist(rd));

	red_obrada.notify_one();
	
	for (int i = 0; i != broj_zahtjeva; ++i)
	{
		red_generator.wait(mtx);
		zahtjevi.push_back(dist(rd));
		red_obrada.notify_one();
	}
	mtx.unlock();
}

void obrada_zahtjeva()
{
	mtx.lock();
	if (zahtjevi.empty() == true)
		red_obrada.wait(mtx);

	int obradenih_zahtjeva{ 0 };
	while (obradenih_zahtjeva != 2 * broj_zahtjeva)
	{
		ima_pogodak = false;
		trenutni_zahtjev = zahtjevi[0];
		for (auto &i : spremnik)
			if (i.stranica == trenutni_zahtjev)
			{
				ima_pogodak = true;
				i.vrijeme_ulaza = high_resolution_clock::now();
			}

		if (ima_pogodak == false)
			*min_element(spremnik.begin(), spremnik.end()) = Element(trenutni_zahtjev);

		ispis_stanja();
		max_element(spremnik.begin(), spremnik.end())->novi_element = false;
		zahtjevi.erase(zahtjevi.begin());

		if (obradenih_zahtjeva < broj_zahtjeva)
		{
			red_generator.notify_one();
			red_obrada.wait(mtx);
		}

		sleep_for(seconds(1));
		++obradenih_zahtjeva;
	}
	mtx.unlock();
}

void ispis_zaglavlja()
{
	cout << left << setw(2 * broj_zahtjeva) << "Zahtjevi"  << " N ";
	for (int i = 0; i != broj_okvira; ++i)
		cout << "  " << i << "  ";
	cout << endl;

	cout << setfill('-') << setw(2 * broj_zahtjeva + 1 + 5 * broj_okvira) << "-" << endl << setfill(' ');
}

int main(int argc, char *argv[])
{
	if (argc != 3)
		exit(1);
	broj_okvira = atoi(argv[1]);
	broj_zahtjeva = atoi(argv[2]);

	spremnik.resize(broj_okvira);
	ispis_zaglavlja();

	thread generator{ thread(generator_zahtjeva) };
	thread obrada{ thread(obrada_zahtjeva) };

	generator.join();
	obrada.join();

	return 0;
}
