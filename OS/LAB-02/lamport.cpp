#include <algorithm>
#include <csignal>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <vector>

#include <unistd.h>

#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/shm.h>

using namespace std;

int id_trazim = -1;
int id_broj = -1;
int id_stolovi = -1;

bool *trazim = nullptr;
int *broj = nullptr;
int *stolovi = nullptr;

int broj_procesa = 0;
int broj_stolova = 0;

inline bool usporedi(int i, int j)
{
	return broj[j] < broj[i] || (broj[j] == broj[i] && j < i);
}

inline int odaberi_stol()
{
	vector<int> slobodni_stolovi;
	for(int i = 0; i < broj_stolova; ++i)
		if(stolovi[i] == 0)
			slobodni_stolovi.push_back(i);
	return slobodni_stolovi[rand()%slobodni_stolovi.size()];
}

bool ima_slobodnih()
{
	bool stanje = false;
	for(int i = 0; i < broj_stolova; ++i)
		if(stolovi[i] == 0)
			stanje = true;
	return stanje;
}

inline void ispis_stanja()
{
	for(int i = 0; i < broj_stolova; ++i)
		cout << stolovi[i] << " / ";
	cout << endl;
}

void udi_u_KO(int i)
{
	trazim[i] = true;
	broj[i] = *max_element(broj, broj + broj_procesa) + 1;
	trazim[i] = false;

	for(int j = 0; j < broj_procesa; ++j)
	{
		while(trazim[j] != false);
		while(broj[j] != 0 && usporedi(i, j));
	}
}

void izadi_iz_KO(int i)
{
	broj[i] = 0;
}

void proces(int i)
{
	srand(static_cast<unsigned int>(i*time(nullptr)));
	while(ima_slobodnih())
	{
		int odabrani = odaberi_stol();	
		cout << "Proces " << i + 1 << ": odabirem stol " << odabrani + 1 << endl;
		sleep(1);		
		udi_u_KO(i);
		if(stolovi[odabrani] == 0)
		{
			cout << "Proces " << i + 1 <<": rezerviram stol " << odabrani + 1 << endl;
			stolovi[odabrani] = i + 1;
		}
		else
		{
			cout << "Proces " << i + 1 <<": neuspjela rezervacija stola " << odabrani + 1 << endl;
		}
		ispis_stanja();
		izadi_iz_KO(i);
	}
}

void brisi(int signal = 0)
{
	shmdt(trazim);
	shmctl(id_trazim, IPC_RMID, nullptr);

	shmdt(broj);
	shmctl(id_broj, IPC_RMID, nullptr);

	shmdt(stolovi);
	shmctl(id_stolovi, IPC_RMID, nullptr);

	exit(0);
}

int main(int argc, char *argv[])
{
	if(argc == 3)
	{
		broj_procesa = atoi(argv[1]);
		broj_stolova = atoi(argv[2]);

		id_trazim = shmget(IPC_PRIVATE, sizeof(bool)*broj_procesa, 0600);
		id_broj = shmget(IPC_PRIVATE, sizeof(int)*broj_procesa, 0600);
		id_stolovi = shmget(IPC_PRIVATE, sizeof(int)*broj_stolova, 0600);

		if(id_trazim == -1 || id_broj == -1 || id_stolovi == -1)
			exit(1);

		trazim = static_cast<bool*>(shmat(id_trazim, nullptr, 0));
		broj = static_cast<int*>(shmat(id_broj, nullptr, 0));
		stolovi = static_cast<int*>(shmat(id_stolovi, nullptr, 0));

		memset(static_cast<void*>(trazim), false, sizeof(bool)*broj_procesa);
		memset(static_cast<void*>(broj), 0, sizeof(int)*broj_procesa);
		memset(static_cast<void*>(stolovi), 0, sizeof(int)*broj_stolova);
	}
	else
		exit(1);

	sigset(SIGINT, brisi);

	int cekaj = 0;
	for(int i = 0; i < broj_procesa; ++i, ++cekaj)
		if(fork() == 0)
		{
			proces(i);
			exit(0);
		}

	while(cekaj--)
		wait(nullptr);

	brisi();

	return 0;
}
