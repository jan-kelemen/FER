#include <iostream>
#include <thread>

#include <unistd.h>

using namespace std;

int pravo;
bool zastavica[2];

void udi_u_KO(int i, int j)
{
	zastavica[i] = true;
	while(zastavica[j] == true)
	{
		if(pravo == j)
		{
			zastavica[i] = false;
			while(pravo == j);
		}
		zastavica[i] = true;
	}
}

void izadi_iz_KO(int i, int j)
{
	pravo = j;
	zastavica[i] = false;
}

inline void ispisi(int i, int k, int m)
{
	cout << "Dretva: " << i + 1 <<", K.O. br: " << k << " (" << m <<"/5)" << endl;
}

void dretva(int i)
{
	for(int k = 1; k <= 5; ++k)
	{
		udi_u_KO(i, 1 - i);
		for(int m = 1; m <= 5; ++m)
			ispisi(i, k, m);
		izadi_iz_KO(i, 1 - i);
		sleep(1);
	}
}

int main()
{
	thread prva(dretva, 0);
	thread druga(dretva, 1);

	prva.join();
	druga.join();
	return 0;
}
