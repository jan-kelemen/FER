#include <csignal>
#include <cstdio>
#include <vector>
#include <string>

#include <unistd.h>

using namespace std;

const int broj_razina = 6;

vector<int> OZNAKA_CEKANJA(broj_razina);
vector<int> PRIORITET(broj_razina);
int TEKUCI_PRIORITET;

const vector<int> signali = { SIGUSR1, SIGUSR2, SIGURG, SIGPROF, SIGINT };

void zabrani_prekidanje()
{
	for (auto i : signali)
		sighold(signali[i]);
}

void dozvoli_prekidanje()
{
	for (auto i : signali)
		sigrelse(signali[i]);
}

void ispis(int n, char c)
{
	string format;
	switch (n)
	{
	case 0:
		format = string("%c - - - - -\n");
		break;
	case 1:
		format = string("- %c - - - -\n");
		break;
	case 2:
		format = string("- - %c - - -\n");
		break;
	case 3:
		format = string("- - - %c - -\n");
		break;
	case 4:
		format = string("- - - - %c -\n");
		break;
	case 5:
		format = string("- - - - - %c\n");
		break;
	default:
		format = string("ERR - - - %c\n");
		break;
	}
	printf(format.c_str(), c);
}

void obrada_prekida(int n)
{
	ispis(n, 'P');
	for (int i = 1; i <= 5; ++i)
	{
		ispis(n, i + '0');
		sleep(1);
	}
	ispis(n, 'K');
}

void prekidna_rutina(int signal)
{
	int n = 0;
	zabrani_prekidanje();
	for (int i = 0; i < broj_razina; ++i)
		if (signali[i] == signal)
		{
		n = i + 1;
		ispis(n, 'X');
		break;
		}
	OZNAKA_CEKANJA[n]++;

	int x;
	do
	{
		x = 0;
		for (int j = TEKUCI_PRIORITET + 1; j < broj_razina; ++j)
			if (OZNAKA_CEKANJA[j] != 0)
				x = j;

		if (x > 0)
		{
			OZNAKA_CEKANJA[x]--;
			PRIORITET[x] = TEKUCI_PRIORITET;
			TEKUCI_PRIORITET = x;
			dozvoli_prekidanje();
			obrada_prekida(x);
			zabrani_prekidanje();
			TEKUCI_PRIORITET = PRIORITET[x];
		}
	} while (x>0);
}

int main(void)
{
	for (auto i : signali)
		sigset(i, prekidna_rutina);

	printf("Proces obrade prekida, PID = %d\n", getpid());
	printf("G 1 2 3 4 5\n");
	printf("-----------\n");
	for (int i = 0; i <10; ++i)
	{
		ispis(0, i + '0');
		sleep(1);
	}
	printf("Zavrsio osnovni program\n");
	return 0;
}