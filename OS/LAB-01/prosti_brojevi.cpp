#include <cmath>
#include <csignal>
#include <cstdlib>
#include <iostream>

#include <sys/time.h>

using namespace std;

typedef unsigned long long int ulli;

bool pauza = false;
ulli broj = 1000000001;
ulli zadnji = 1000000001;

const unsigned int interval = 5;

bool prost(ulli broj)
{
	if (broj % 2 == 0)
		return false;

	for (int i = 3; i <= sqrt(broj); i += 2)
		if (broj % i == 0)
			return false;

	return true;
}

inline void periodicki_ispis(int signal)
{
	cout << "zadnji prosti broj = " << zadnji << endl;
}


inline void promjeni_pauzu(int signal)
{
	pauza = pauza == true ? false : true;
}

inline void prekini(int signal)
{
	periodicki_ispis(signal);
	exit(0);
}

inline void postavi_alarm(struct itimerval &t, unsigned int sekunda)
{
	t.it_value.tv_sec = t.it_interval.tv_sec = sekunda;
	t.it_value.tv_usec = t.it_interval.tv_usec = 0;
}

int main()
{
	sigset(SIGINT, promjeni_pauzu);
	sigset(SIGTERM, prekini);
	sigset(SIGALRM, periodicki_ispis);

	struct itimerval t;
	postavi_alarm(t, interval);
	setitimer(ITIMER_REAL, &t, NULL);

	while (true)
	{
		if (prost(broj) == true)
			zadnji = broj;
		++broj;

		while (pauza == true);
	}

	return 0;
}