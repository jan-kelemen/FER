#include <csignal>
#include <cstdlib>
#include <ctime>
#include <vector>

#include <unistd.h>

using namespace std;

int pid;

const vector<int> signali = { SIGUSR1, SIGUSR2, SIGURG, SIGPROF };

inline void prekidna_rutina(int signal)
{
	kill(pid, SIGKILL);
	exit(0);
}

inline int sekunde()
{
	return rand() % 3 + 3;
}

inline int odaberi_signal()
{
	return signali[rand() % signali.size()];
}

int main(int argc, char *argv[])
{
	if (argc == 2)
		pid = atoi(argv[1]);
	else
		exit(1);

	srand(static_cast<unsigned int>(time(NULL)));

	sigset(SIGINT, prekidna_rutina);

	while (1)
	{
		kill(pid, odaberi_signal());
		sleep(sekunde());
	}
	return 0;
}
