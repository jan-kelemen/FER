﻿using CashRegister.Misc;
using System;
using System.Globalization;

namespace CashRegister.Utilities
{
    public class IOUtility
    {
        private IOUtility() { }

        public const string DEFAULT_QUIT_VALUE = "q";

        public static bool ReadInt(string message, ref int id, string quitValue = DEFAULT_QUIT_VALUE, int minValue = 1, int maxValue = int.MaxValue)
        {
            while (true)
            {
                bool success = true;
                try
                {
                    Console.Write(message);
                    var str = Console.ReadLine();
                    if (str == quitValue) { return false; }
                    var tmp = int.Parse(str);
                    if(tmp >= minValue && tmp <= maxValue) { id = tmp; return true; }
                    success = false;
                }
                catch (Exception)
                {
                    success = false;
                }

                if(!success)
                {
                    Console.WriteLine(ConsoleMessages.INVALID_ENTRY);
                }
            }
        }

        public static bool ReadDouble(string message, ref double id, string quitValue = DEFAULT_QUIT_VALUE, double minValue = 1, double maxValue = int.MaxValue)
        {
            while (true)
            {
                bool success = true;
                try
                {
                    Console.Write(message);
                    var str = Console.ReadLine();
                    if (str == quitValue) { return false; }
                    var tmp = double.Parse(str, CultureInfo.InvariantCulture);
                    if (tmp >= minValue && tmp <= maxValue) { id = tmp; return true; }
                    success = false;
                }
                catch (Exception)
                {
                    success = false;
                }

                if (!success)
                {
                    Console.WriteLine(ConsoleMessages.INVALID_ENTRY);
                }
            }
        }

        public static bool ReadDate(string message, ref DateTime date, string quitValue = DEFAULT_QUIT_VALUE)
        {
            while (true)
            {
                bool success = true;
                try
                {
                    Console.Write(message);
                    var str = Console.ReadLine();
                    if (str == quitValue) { return false; }
                    date = DateTime.ParseExact(str, "dd.MM.yyyy.", null);
                    return true;
                }
                catch (Exception)
                {
                    success = false;
                }

                if (!success)
                {
                    Console.WriteLine(ConsoleMessages.INVALID_ENTRY);
                }
            }
        }

        public static void PrintSeparator()
        {
            Console.WriteLine(ConsoleMessages.CONSOLE_SEPARATOR);
        }

        public static void PrintHeader(string title)
        {
            PrintSeparator();
            var usr = UserManager.CurrentUser;
            Console.WriteLine(title + (usr != null ? " (Korisnik: " + usr.Name + ")" : ""));
            PrintSeparator();
        }
    }
}
