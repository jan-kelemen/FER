﻿using CashRegister.Models;

namespace CashRegister.Misc
{
    public class UserManager
    {
        private static Account account;

        private UserManager() { }

        public static Account CurrentUser { get { return account; } set { account = value; } }

        public static AccountType CurrentUserType { get { return account?.Type ?? AccountType.None; } }
    }
}
