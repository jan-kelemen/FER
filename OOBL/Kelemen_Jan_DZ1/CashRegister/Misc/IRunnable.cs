﻿namespace CashRegister.Misc
{
    public interface IRunnable
    {
        bool Run();
    }
}
