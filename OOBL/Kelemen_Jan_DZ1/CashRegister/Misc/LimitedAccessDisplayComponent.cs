﻿using CashRegister.Models;

namespace CashRegister.Misc
{
    public abstract class LimitedAccessDisplayComponent : DisplayComponent, ILimitedAccessItem, IRunnable
    {
        private AccountType availableFor;

        public LimitedAccessDisplayComponent(string name, AccountType availableFor) : base(name)
        {
            this.availableFor = availableFor;
        }

        public bool IsAvailableFor(AccountType type)
        {
            return (availableFor & type) == type;
        }

        public abstract bool Run();
    }
}
