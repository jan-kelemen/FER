﻿using CashRegister.Models;

namespace CashRegister.Misc
{
    public interface ILimitedAccessItem
    {
        bool IsAvailableFor(AccountType type);
    }
}
