﻿using System;

namespace CashRegister.Misc
{
    public abstract class DisplayComponent
    {
        private int initialCursorLeft;
        private int initialCursorTop;

        public DisplayComponent(string name) { Name = name; ReadTopValue(); }

        public string Name { get; private set; }

        protected void ReadTopValue()
        {
            initialCursorLeft = Console.CursorLeft;
            initialCursorTop = Console.CursorTop;
        }

        protected void ScrollToTop()
        {
            Console.SetWindowPosition(initialCursorLeft, initialCursorTop);
        }

        protected void ShowLastResult()
        {
            var left = Math.Max(0, initialCursorLeft - 2);
            var top = Math.Max(0, initialCursorTop - 2);
            Console.SetWindowPosition(left, top);
        }

        public abstract void Display();
    }
}
