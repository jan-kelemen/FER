﻿using System;
using System.Collections.Generic;
using CashRegister.Models;
using CashRegister.UseCases;
using CashRegister.Utilities;
using CashRegister.Misc;

namespace CashRegister.Menus
{
    public class UseCaseMenu : AbstractMenu
    {
        public UseCaseMenu(string name, AccountType availableFor, List<LimitedAccessDisplayComponent> items)
            : base(name, availableFor, items)
        { }

        public override bool Run()
        {
            var showResult = false;
            var filtered = filterItemsForCurrentUser();
            while(true)
            {
                Display();
                if(showResult) { ShowLastResult(); }
                var option = 0;
                var test = IOUtility.ReadInt(ConsoleMessages.READ_MENU_OPTION, ref option, maxValue: filtered.Count);
                if (!test) { return false; }

                try
                {
                    filtered[option - 1].Display();
                    test = filtered[option - 1].Run();
                    IOUtility.PrintSeparator();
                    Console.WriteLine(test ? "Operacija obavljena." : "Operacija otkazana.");
                }
                catch (Exception e)
                {
                    IOUtility.PrintSeparator();
                    Console.WriteLine("Operacija nije uspjela: " + e.Message);
                }

                showResult = true;
            }
        }
    }
}
