﻿using CashRegister.Misc;
using CashRegister.Models;
using CashRegister.Utilities;
using System;
using System.Collections.Generic;

namespace CashRegister.Menus
{
    public abstract class AbstractMenu : LimitedAccessDisplayComponent
    {
        private List<LimitedAccessDisplayComponent> items;

        public AbstractMenu(string name, AccountType availableFor, List<LimitedAccessDisplayComponent> items) 
            : base(name, availableFor)
        {
            this.items = items;
        }

        public override void Display()
        {

            ReadTopValue();
            IOUtility.PrintHeader(Name);
            var filtered = filterItemsForCurrentUser();
            for (var i = 0; i < filtered.Count; i++)
            {
                Console.WriteLine($"{i + 1} - {filtered[i].Name}");
            }
            IOUtility.PrintSeparator();
            ScrollToTop();
        }

        protected List<LimitedAccessDisplayComponent> filterItemsForCurrentUser()
        {
            var rv = new List<LimitedAccessDisplayComponent>();

            foreach (var i in items)
            {
                if (i.IsAvailableFor(UserManager.CurrentUserType))
                {
                    rv.Add(i);
                }
            }
            return rv;
        }
    }
}
