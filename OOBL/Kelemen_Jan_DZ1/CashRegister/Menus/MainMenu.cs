﻿using System;
using System.Collections.Generic;
using CashRegister.Models;
using CashRegister.UseCases;
using CashRegister.Utilities;
using CashRegister.Misc;

namespace CashRegister.Menus
{
    public class MainMenu : AbstractMenu
    {

        public MainMenu()
            : base("Glavni izbornik", AccountType.All, new List<LimitedAccessDisplayComponent>()
        {
            new UseCaseMenu("Korisnici", AccountType.All, 
                new List<LimitedAccessDisplayComponent>() { new LogIn(), new SignOut() }),
            new UseCaseMenu("Artikli", AccountType.LoggedIn, 
                new List<LimitedAccessDisplayComponent>() { new CreateArticle(), new EditArticle(), new ListArticles() }),
            new UseCaseMenu("Računi", AccountType.LoggedIn, 
                new List<LimitedAccessDisplayComponent>() { new CreateBill(), new EditBill(), new ListBills() }),
            new UseCaseMenu("Izvješća", AccountType.LoggedIn, 
                new List<LimitedAccessDisplayComponent>() { new DailyReport(), new MostSold() }),
        })
        {
            ;
        }

        public override bool Run()
        {
            while (true)
            {
                var filtered = filterItemsForCurrentUser();

                Display();
                var option = 0;
                var test = IOUtility.ReadInt(ConsoleMessages.READ_MENU_OPTION, ref option, maxValue: filtered.Count);
                if (!test) { return false; }

                try
                {
                    test = filtered[option - 1].Run();
                }
                catch (Exception e)
                {
                    IOUtility.PrintSeparator();
                    Console.WriteLine("Operacija nije uspjela: " + e.Message);
                }
            }
        }
    }
}
