﻿using CashRegister.Menus;
using CashRegister.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace CashRegister
{
    class Program
    {
        static void Main(string[] args)
        {
            var menu = new MainMenu();
            menu.Run();
        }
    }
}
