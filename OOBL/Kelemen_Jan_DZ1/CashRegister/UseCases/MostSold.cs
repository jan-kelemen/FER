﻿using CashRegister.Models;
using CashRegister.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CashRegister.UseCases
{
    public class MostSold : AbstractUseCase
    {
        public MostSold() : base("Najprodavaniji artikli", AccountType.LoggedIn) { }

        public override bool Run()
        {
            Display();

            var idPricePairs = createArticlesAndSoldValueMap(BillRepository.Instance.GetAll());
            foreach(var p in idPricePairs)
            {
                var article = ArticleRepository.Instance.GetById(p.Key);

                var id = article.Id;
                var name = article.Name;
                var quantity = p.Value.Item1.ToString("N2");
                var charge = p.Value.Item2.ToString("N2");
                Console.WriteLine($"\t{id} - Naziv: {name}, Prodanih artikala: {quantity}, Ukupna vrijednost: {charge}");
            }
            Console.WriteLine("..."); Console.ReadLine();
            return true;
        }

        private IOrderedEnumerable<KeyValuePair<int, Tuple<double, double>>> createArticlesAndSoldValueMap(List<Bill> bills)
        {
            var rv = new Dictionary<int, Tuple<double, double>>();
            foreach(var b in bills)
            {
                foreach(var i in b.Items)
                {
                    var articleId = i.Article.Id;
                    var quantity = i.Quantity;
                    var charge = i.Article.Charge(quantity);

                    Tuple<double, double> value = rv.ContainsKey(articleId) ? 
                        rv[articleId] : new Tuple<double, double>(0, 0);

                    value = new Tuple<double, double>(value.Item1 + quantity, value.Item2 + charge);

                    rv[articleId] = value;
                }
            }

            return rv.OrderByDescending(p => p.Value.Item2);
        }
    }
}
