﻿using CashRegister.Models;
using CashRegister.Misc;

namespace CashRegister.UseCases
{
    public class SignOut : AbstractUseCase
    {
        public SignOut() : base("Odjava", AccountType.Administrator | AccountType.Cashier) { }

        public override bool Run()
        {
            Display();
            UserManager.CurrentUser = null;
            return true;
        }
    }
}
