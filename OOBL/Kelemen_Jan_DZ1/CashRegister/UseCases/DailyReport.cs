﻿using System;
using System.Linq;
using CashRegister.Models;
using CashRegister.Repositories;
using CashRegister.Utilities;

namespace CashRegister.UseCases
{
    public class DailyReport : AbstractUseCase
    {
        public DailyReport() : base("Dnevno izvješće", AccountType.LoggedIn) { }

        public override bool Run()
        {
            Display();
            var date = DateTime.Now;
            var test = IOUtility.ReadDate(ConsoleMessages.READ_DATE, ref date);
            if(!test) { return false; }

            var bills = BillRepository.Instance.GetByDate(date);
            var sum = Math.Round(bills.Sum(b => b.TotalCharge()), 2);

            Console.WriteLine("Izdano računa: " + bills.Count);
            Console.WriteLine("Ukupna vrijednost: " + sum);
            IOUtility.PrintSeparator();
            if(bills.Count > 0)
            {
                foreach (var bill in bills)
                {
                    Console.WriteLine("\t" + bill.ToString());
                }
                ScrollToTop();
            }
            Console.WriteLine("..."); Console.ReadLine();
            return true;
        }
    }
}
