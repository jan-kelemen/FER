﻿using System;
using CashRegister.Models;
using CashRegister.Utilities;
using CashRegister.Repositories;

namespace CashRegister.UseCases
{
    public abstract class CreateOrEditBill : AbstractUseCase
    {
        public CreateOrEditBill(string name, AccountType availableFor) : base(name, availableFor) { }

        protected void addNewItem(Bill bill)
        {
            var item = new BillItem();

            new ListArticles(displayHeader: false, waitInput: false).Run();
            IOUtility.PrintSeparator();
            ScrollToTop();

            while (true)
            {
                var id = 0;
                var test = IOUtility.ReadInt(ConsoleMessages.READ_ARTICLE_ID, ref id);
                if (!test) { return; }
                try
                {
                    var article = ArticleRepository.Instance.GetById(id);
                    item.Article = article;
                    break;
                }
                catch (ArgumentException e)
                {
                    Console.WriteLine("Neispravan unos: " + e.Message);
                }
            }

            while (true)
            {

                try
                {
                    if (item.Article.Type == ArticleType.Piecewise)
                    {
                        var quantity = 0;
                        var test = IOUtility.ReadInt(ConsoleMessages.READ_QUANTITY, ref quantity);
                        if (!test) { return; }
                        item.Quantity = quantity;
                    }
                    else
                    {
                        var quantity = .0;
                        var test = IOUtility.ReadDouble(ConsoleMessages.READ_QUANTITY, ref quantity, minValue: 0);
                        item.Quantity = quantity;
                    }

                    break;
                }
                catch (ArgumentException e)
                {
                    Console.WriteLine("Neispravan unos: " + e.Message);
                }
            }

            bill.AddItem(item);
        }

        protected void saveBill(Bill bill)
        {
            var test = BillRepository.Instance.GetById(bill.Id);

            if(test == null) { BillRepository.Instance.Add(bill); }
            else { BillRepository.Instance.Update(bill); }

            writeBill(bill);
        }

        protected void writeBill(Bill bill)
        {
            ReadTopValue();
            printBill(bill);
            Console.WriteLine("...");
            ScrollToTop();
            Console.ReadLine();
        }

        private void printBill(Bill bill)
        {
            IOUtility.PrintSeparator();
            Console.WriteLine("Datum izdavanja: " + bill.Time.ToShortDateString());
            Console.WriteLine("Vrijeme izdavanja: " + bill.Time.ToShortTimeString());
            IOUtility.PrintSeparator();
            Console.WriteLine("Stavke: ");
            for (var i = 0; i < bill.NumberOfItems; i++)
            {
                var item = bill.GetItemAt(i);
                Console.WriteLine($"\t{i + 1} - {item.ToString()} - {item.Article.Charge(item.Quantity).ToString("N2")}");
            }
            IOUtility.PrintSeparator();
            Console.WriteLine("Ukupno: " + bill.TotalCharge().ToString("N2"));
            IOUtility.PrintSeparator();
        }
    }
}
