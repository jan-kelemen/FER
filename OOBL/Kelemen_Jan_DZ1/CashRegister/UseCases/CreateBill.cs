﻿using CashRegister.Models;
using CashRegister.Repositories;
using CashRegister.Utilities;
using System;

namespace CashRegister.UseCases
{
    public class CreateBill : CreateOrEditBill
    {

        public CreateBill() : base("Izradi račun", AccountType.LoggedIn) { }

        public override bool Run()
        {
            Display();
            var bill = new Bill();
            bill.Id = BillRepository.Instance.NextId;
            while (true)
            {
                showHelp();
                var option = 0;
                var test = IOUtility.ReadInt(ConsoleMessages.READ_MENU_OPTION, ref option, maxValue: 3);
                if (!test) { return false; }

                if (option == 1)
                {
                    addNewItem(bill);
                }
                else if (option == 2)
                {
                    writeBill(bill);
                }
                else if (option == 3)
                {
                    saveBill(bill);
                    break;
                }
            }
            return true;
        }

        private void showHelp()
        {
            Console.WriteLine(ConsoleMessages.CONSOLE_SEPARATOR);
            Console.WriteLine("\t1 - Unos nove stavke");
            Console.WriteLine("\t2 - Trenutno stanje");
            Console.WriteLine("\t3 - Izdavanje računa");
            Console.WriteLine(ConsoleMessages.CONSOLE_SEPARATOR);
        }
    }
}
