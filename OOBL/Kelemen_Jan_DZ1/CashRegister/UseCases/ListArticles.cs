﻿using System;
using CashRegister.Models;
using CashRegister.Repositories;

namespace CashRegister.UseCases
{
    public class ListArticles : AbstractUseCase
    {
        private bool waitInput;

        public ListArticles(string name = "Popis artikla", AccountType availableFor = AccountType.LoggedIn, bool displayHeader = true, bool waitInput = true)
            : base(name, availableFor, displayHeader)
        {
            this.waitInput = waitInput;
        }

        public override bool Run()
        {
            foreach (var article in ArticleRepository.Instance.GetAll())
            {
                Console.WriteLine("\t" + article.ToString());
            }
            ScrollToTop();
            if(waitInput) { Console.WriteLine("...");  Console.ReadLine(); }
            return true;
        }
    }
}
