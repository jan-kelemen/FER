﻿using System;
using CashRegister.Models;
using CashRegister.Repositories;
using CashRegister.Utilities;

namespace CashRegister.UseCases
{
    public class EditBill : CreateOrEditBill
    {
        public EditBill(string name = "Uredi račun", AccountType allowedFor = AccountType.Administrator)
            : base(name, allowedFor)
        { }

        public override void Display()
        {
            base.Display();
            new ListBills(displayHeader: false, waitInput: false).Run();
            IOUtility.PrintSeparator();
            ScrollToTop();
        }

        public override bool Run()
        {
            Display();

            var id = 0;
            var test = IOUtility.ReadInt(ConsoleMessages.READ_ARTICLE_ID, ref id);
            if (!test) { return false; }

            IOUtility.PrintSeparator();

            var bill = BillRepository.Instance.GetById(id);
            while (true)
            {
                showHelp();
                var option = 0;
                test = IOUtility.ReadInt(ConsoleMessages.READ_MENU_OPTION, ref option, maxValue: 5);
                if (!test) { return false; }

                if (option == 1)
                {
                    addNewItem(bill);
                }
                else if (option == 2)
                {
                    writeBill(bill);
                }
                else if (option == 3)
                {
                    saveBill(bill);
                    break;
                }
                else if (option == 4)
                {
                    deleteItem(bill);
                }
                else if (option == 5)
                {
                    deleteBill(bill);
                    break;
                }
            }
            return true;
        }

        private void showHelp()
        {
            Console.WriteLine(ConsoleMessages.CONSOLE_SEPARATOR);
            Console.WriteLine("\t1 - Unos nove stavke");
            Console.WriteLine("\t2 - Trenutno stanje");
            Console.WriteLine("\t3 - Izdavanje računa");
            Console.WriteLine("\t4 - Brisanje stavke");
            Console.WriteLine("\t5 - Brisanje računa");
            Console.WriteLine(ConsoleMessages.CONSOLE_SEPARATOR);
        }

        private void deleteItem(Bill bill)
        {
            ReadTopValue();
            Console.WriteLine("Stavke: ");
            printBillItems(bill);
            IOUtility.PrintSeparator();
            ScrollToTop();
            var option = 0;
            var test = IOUtility.ReadInt(ConsoleMessages.READ_MENU_OPTION, ref option, maxValue: bill.Items.Count);
            if(!test) { return; }

            bill.RemoveItemAt(option - 1);
            BillRepository.Instance.Update(bill);           
        }

        private void deleteBill(Bill bill)
        {
            BillRepository.Instance.Delete(bill);
        }

        private void printBillItems(Bill bill)
        {
            for (var i = 0; i < bill.Items.Count; i++)
            {
                var item = bill.GetItemAt(i);
                Console.WriteLine($"\t{i + 1} - {item.ToString()} - {item.Article.Charge(item.Quantity).ToString("N2")}");
            }
        }
    }
}
