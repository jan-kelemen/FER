﻿using System;
using CashRegister.Models;
using CashRegister.Utilities;
using System.Globalization;

namespace CashRegister.UseCases
{
    public abstract class CreateOrEditArticle : AbstractUseCase
    {
        public CreateOrEditArticle(string name, AccountType availableFor) : base(name, availableFor) { }

        protected bool readArticleName(string message, Article article, string quitValue = IOUtility.DEFAULT_QUIT_VALUE)
        {
            while (true)
            {
                try
                {
                    Console.Write(message);
                    var str = Console.ReadLine();
                    if (str == quitValue) { return false; }
                    article.Name = str;
                    break;
                }
                catch (ArgumentException e)
                {
                    Console.WriteLine(e.Message);
                }
            }
            return true;
        }

        protected bool readArticlePrice(string message, Article article, string quitValue = IOUtility.DEFAULT_QUIT_VALUE)
        {
            while (true)
            {
                try
                {
                    Console.Write(message);
                    var str = Console.ReadLine();
                    if (str == quitValue) { return false; }
                    article.Price = Math.Round(double.Parse(str, CultureInfo.InvariantCulture), 2);
                    break;
                }
                catch (ArgumentException e)
                {
                    Console.WriteLine(e.Message);
                }
                catch (Exception)
                {
                    Console.WriteLine(ConsoleMessages.INVALID_ENTRY);
                }
            }
            return true;
        }

        protected bool readArticleVAT(string message, Article article, string quitValue = IOUtility.DEFAULT_QUIT_VALUE)
        {
            while (true)
            {
                try
                {
                    Console.Write(message);
                    var str = Console.ReadLine();
                    if (str == quitValue) { return false; }
                    article.VAT = int.Parse(str);
                    break;
                }
                catch (ArgumentException e)
                {
                    Console.WriteLine(e.Message);
                }
                catch (Exception)
                {
                    Console.WriteLine(ConsoleMessages.INVALID_ENTRY);
                }
            }
            return true;
        }

        protected bool readArticleType(string message, Article article, string quitValue = IOUtility.DEFAULT_QUIT_VALUE,
            string piecewiseValue = "kom", string granulatedValue = "kg")
        {
            while (true)
            {
                bool success = true;
                try
                {
                    Console.Write(message);
                    var str = Console.ReadLine();
                    if (str == quitValue) { return false; }
                    else if (str == piecewiseValue) { article.Type = ArticleType.Piecewise; break; }
                    else if (str == granulatedValue) { article.Type = ArticleType.Granulated; break; }
                    else { success = false; }
                }
                catch (ArgumentException e)
                {
                    Console.WriteLine(e.Message);
                }
                catch (Exception)
                {
                    success = false;
                }

                if (!success)
                {
                    Console.WriteLine(ConsoleMessages.INVALID_ENTRY);
                }
            }
            return true;
        }
    }
}
