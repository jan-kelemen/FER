﻿using CashRegister.Misc;
using CashRegister.Models;
using CashRegister.Utilities;

namespace CashRegister.UseCases
{
    public abstract class AbstractUseCase : LimitedAccessDisplayComponent
    {
        private bool displayHeader;

        public AbstractUseCase(string name, AccountType availableFor, bool displayHeader = true) : base(name, availableFor)
        {
            this.displayHeader = displayHeader;
        }

        public override void Display()
        {
            ReadTopValue();
            if (displayHeader) { IOUtility.PrintHeader(Name); }
            ScrollToTop();
        }
    }
}
