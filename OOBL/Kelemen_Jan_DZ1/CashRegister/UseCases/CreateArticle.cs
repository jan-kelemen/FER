﻿using CashRegister.Models;
using CashRegister.Repositories;

namespace CashRegister.UseCases
{
    public class CreateArticle : CreateOrEditArticle
    {
        public CreateArticle() : base("Dodaj novi artikl", AccountType.Administrator) { }

        public override bool Run()
        {
            Display();
            var article = new Article();
            article.Id = ArticleRepository.Instance.NextId;

            var test = readArticleName(ConsoleMessages.READ_ARTICLE_NAME, article);
            if (test) { test = readArticlePrice(ConsoleMessages.READ_ARTICLE_PRICE, article); }
            if (test) { test = readArticleVAT(ConsoleMessages.READ_ARTCLE_VAT, article); }
            if (test) { test = readArticleType(ConsoleMessages.READ_ARTICLE_TYPE, article); }

            if (test) { ArticleRepository.Instance.Add(article); }

            return true;
        }
    }
}
