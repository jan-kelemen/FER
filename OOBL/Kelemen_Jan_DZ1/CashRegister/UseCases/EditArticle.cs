﻿using CashRegister.Models;
using CashRegister.Utilities;
using CashRegister.Repositories;

namespace CashRegister.UseCases
{
    public class EditArticle : CreateOrEditArticle
    {
        public EditArticle() : base("Uredi artikl", AccountType.Administrator) { }

        public override void Display()
        {
            base.Display();
            new ListArticles(displayHeader: false, waitInput: false).Run();
            IOUtility.PrintSeparator();
            ScrollToTop();
        }

        public override bool Run()
        {
            Display();

            var id = 0;
            var test = IOUtility.ReadInt(ConsoleMessages.READ_ARTICLE_ID, ref id);
            if (!test) { return false; }

            IOUtility.PrintSeparator();
            var article = ArticleRepository.Instance.GetById(id);

            test = readArticleName(ConsoleMessages.READ_ARTICLE_NAME, article);
            if (test) { test = readArticlePrice(ConsoleMessages.READ_ARTICLE_PRICE, article); }
            if (test) { test = readArticleVAT(ConsoleMessages.READ_ARTCLE_VAT, article); }

            if (test) { ArticleRepository.Instance.Update(article); }

            return true;
        }
    }
}
