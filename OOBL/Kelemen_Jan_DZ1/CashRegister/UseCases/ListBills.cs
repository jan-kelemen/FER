﻿using System;
using CashRegister.Models;
using CashRegister.Repositories;

namespace CashRegister.UseCases
{
    public class ListBills : AbstractUseCase
    {
        private bool waitInput;

        public ListBills(string name = "Popis računa", AccountType availableFor = AccountType.LoggedIn, bool displayHeader = true, bool waitInput = true) 
            : base(name, availableFor, displayHeader)
        {
            this.waitInput = waitInput;
        }

        public override bool Run()
        {
            foreach (var bill in BillRepository.Instance.GetAll())
            {
                Console.WriteLine("\t" + bill.ToString());
            }
            ScrollToTop();
            if (waitInput) { Console.WriteLine("..."); Console.ReadLine(); }
            return true;
        }
    }
}
