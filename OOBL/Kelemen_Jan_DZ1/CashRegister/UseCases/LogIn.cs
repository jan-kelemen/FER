﻿using System;
using CashRegister.Models;
using CashRegister.Repositories;
using CashRegister.Misc;

namespace CashRegister.UseCases
{
    public class LogIn : AbstractUseCase
    {
        public LogIn() : base("Prijava", AccountType.None)
        {
        }

        public override bool Run()
        {
            Display();
            Console.WriteLine(ConsoleMessages.READ_USERNAME);
            var username = Console.ReadLine();
            Console.WriteLine(ConsoleMessages.READ_PASSWORD);
            var password = Console.ReadLine();

            var user = AccountRepository.Instance.GetByUsername(username);

            if(user.LogIn(password))
            {
                UserManager.CurrentUser = user;
                return true;
            }

            return false;
        }
    }
}
