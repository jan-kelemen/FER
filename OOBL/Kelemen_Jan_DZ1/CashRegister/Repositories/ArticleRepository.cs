﻿using CashRegister.Models;

namespace CashRegister.Repositories
{
    public class ArticleRepository : BaseRepository<Article>
    {
        private static ArticleRepository instance = new ArticleRepository();

        public static ArticleRepository Instance { get { return instance; } }

        private ArticleRepository() : base("articles.xml")
        {
            //data = new System.Collections.Generic.List<Article>()
            //{
            //    new Article(1, "Hrenovke", 22.13, 24, ArticleType.Piecewise),
            //    new Article(2, "Lubenica", 13.12, 5, ArticleType.Granulated),
            //    new Article(3, "Voda", 5.99, 10, ArticleType.Piecewise),
            //};

            //Serialize();
        }
    }
}
