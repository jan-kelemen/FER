﻿using CashRegister.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CashRegister.Repositories
{
    public class BillRepository : BaseRepository<Bill>
    {
        private static BillRepository instance = new BillRepository();

        public static BillRepository Instance { get { return instance; } }

        public BillRepository() : base("bills.xml") {
            //data = new List<Bill>()
            //{
            //    new Bill(1, DateTime.Now, new List<BillItem>()
            //    {
            //        new BillItem
            //        { Article = new Article(2, "Lubenica", 13.12, 5, ArticleType.Granulated), Quantity = 2 } })
            //};
            //Serialize();
        }

        public List<Bill> GetByDate(DateTime date)
        {
            return (from b in data
                    where b.Time.Date == date.Date
                    select b).ToList();
        }
    }
}
