﻿using CashRegister.Models;
using CashRegister.Utilities;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;

namespace CashRegister.Repositories
{
    public abstract class BaseRepository<T> where T : ModelBase
    {
        private string filename;

        protected List<T> data = new List<T>();

        public BaseRepository(string filename)
        {
            this.filename = filename;
            Deserialize();
        }

        protected void Serialize()
        {
            File.Delete(filename);

            var file = new FileStream(filename, FileMode.OpenOrCreate);
            XmlSerializer serializer = new XmlSerializer(data.GetType());
            serializer.Serialize(file, data);
            file.Close();

            Deserialize();
        }

        protected void Deserialize()
        {
            var file = new FileStream(filename, FileMode.OpenOrCreate);
            XmlSerializer serializer = new XmlSerializer(typeof(List<T>));
            data = (List<T>)serializer.Deserialize(file);
            file.Close();
        }

        public int NextId
        {
            get
            {
                if (ItemCount == 0)
                {
                    return 1;
                }

                return data.Last().Id + 1;
            }
        }

        public int ItemCount { get { return data.Count; } }

        public List<T> GetAll()
        {
            return data;
        }

        public T GetById(int id)
        {
            return data.Find(a => a.Id == id);
        }


        public bool Add(T item)
        {
            data.Add(item);
            Serialize();

            return true;
        }

        public bool Update(T item)
        {
            var tmp = GetById(item.Id);
            if (!ReferenceEquals(tmp, item)) { ReflectionUtility.CopyProperties(item, tmp); }
            Serialize();

            return true;
        }

        public bool Delete(T item)
        {
            data.Remove(item);
            Serialize();

            return true;
        }

    }
}
