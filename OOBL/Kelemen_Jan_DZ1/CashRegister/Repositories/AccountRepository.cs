﻿using CashRegister.Models;
using System.Collections.Generic;

namespace CashRegister.Repositories
{
    public class AccountRepository : BaseRepository<Account>
    {
        private static AccountRepository instance = new AccountRepository();

        public static AccountRepository Instance { get { return instance; } }

        public AccountRepository() : base("accounts.xml")
        {
            //data = new List<Account>()
            //{
            //    new Account(1, "jan", "1234", AccountType.Administrator),
            //    new Account(2, "xenia", "1234", AccountType.Cashier)
            //};
            //Serialize();
        }

        public Account GetByUsername(string username)
        {
            return data.Find(a => a.Name == username);
        }
    }
}
