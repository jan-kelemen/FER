﻿using System;

namespace CashRegister.Models
{
    public class Account : ModelBase
    {
        private string name;
        private string password;
        private AccountType type;

        public Account() { }

        public Account(int id, string name, string password, AccountType type) : base(id)
        {
            Name = name;
            Password = password;
            Type = type;
        }

        public string Name
        {
            get { return name; }
            set
            {
                if(string.IsNullOrEmpty(value))
                {
                    throw new ArgumentException(ExceptionMessages.ACCOUNT_NAME_INVALID);
                }
                name = value;
            }
        }

        public string Password
        {
            get { return password; }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentException(ExceptionMessages.ACCOUNT_PASSWORD_INVALID);
                }
                password = value;
            }
        }

        public AccountType Type
        {
            get { return type; }
            set { type = value; }
        }

        public bool LogIn(string password)
        {
            return Password.Equals(password);
        }
    }
}
