﻿using System;
using System.Xml.Serialization;

namespace CashRegister.Models
{
    public class BillItem
    {
        private Article article;
        private double quantity;

        public Article Article
        {
            get { return article; }
            set
            {
                if(value == null)
                {
                    throw new ArgumentException(ExceptionMessages.BILL_ARTICLE_NULL);
                }
                article = value;
            }
        }

        public string TextQuantity
        {
            get { return Quantity.ToString("N2"); }
            set { Quantity = double.Parse(value); }
        }

        [XmlIgnore]
        public double Quantity
        {
            get { return quantity; }
            set
            {
                if(!Article.ValidQuantity(Article, value))
                {
                    throw new ArgumentException(ExceptionMessages.BILL_QUANTITY_INVALID);
                }
                quantity = value;
            }
        }

        public override string ToString()
        {
            var art = "Artikl: " + Article.Name;
            var quant = "Količina: " + (Article.Type == ArticleType.Piecewise ? 
                ((int)Quantity).ToString() : Quantity.ToString("N2"));

            return $"{art}, {quant}";
        }
    }
}
