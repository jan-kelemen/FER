﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace CashRegister.Models
{
    public class Bill : ModelBase
    {
        private DateTime time;
        private List<BillItem> items;

        public Bill() { time = DateTime.Now; items = new List<BillItem>(); }

        public Bill(int id, DateTime time, List<BillItem> items) : base(id)
        {
            Time = time;
            Items = items;
        }

        [XmlIgnore]
        public DateTime Time
        {
            get { return time; }
            set { time = value; }
        }

        public string TextTime
        {
            get { return Time.ToString("yyyy-MM-dd HH:mm:ss"); }
            set { Time = DateTime.Parse(value); }
        }

        public List<BillItem> Items
        {
            get { return items; }
            set { items = value; }
        }

        [XmlIgnore]
        public int NumberOfItems
        {
            get { return items.Count; }
        }

        public void AddItem(BillItem item)
        {
            items.Add(item);
        }

        public void RemoveItemAt(int index) { items.RemoveAt(index); }

        public BillItem GetItemAt(int index) { return items[index]; }

        public double TotalCharge()
        {
            var sum = .0;
            foreach (var i in items)
            {
                sum += i.Article.Charge(i.Quantity);
            }
            return Math.Round(sum, 2);
        }

        public override string ToString()
        {
            var date = "Datum: " + Time.ToShortDateString();
            var time = "Vrijeme: " + Time.ToShortTimeString();
            var items = "Stavki: " + NumberOfItems;
            var charge = "Iznos: " + TotalCharge();

            return $"{Id} - {date}, {time}, {items} - {charge}";
        }
    }
}
