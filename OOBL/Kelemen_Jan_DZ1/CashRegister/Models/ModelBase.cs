﻿using System.Xml.Serialization;

namespace CashRegister.Models
{
    [XmlInclude(typeof(Account))]
    [XmlInclude(typeof(Article))]
    [XmlInclude(typeof(Bill))]
    public abstract class ModelBase
    {
        public ModelBase() { }

        public ModelBase(int id) { Id = id; }

        public int Id { get; set; }
    }
}
