﻿using System;
using System.Xml.Serialization;

namespace CashRegister.Models
{
    public class Article : ModelBase
    {
        public static bool ValidQuantity(Article article, double quantity)
        {
            return article.Type == ArticleType.Granulated || ((quantity % 1) < 0.00001);
        }

        private string name;
        private double price;
        private int vat;
        private ArticleType type;

        public Article() { }

        public Article(int id, string name, double price, int vat, ArticleType type) : base(id)
        {
            Name = name;
            Price = price;
            VAT = vat;
            Type = type;
        }

        public string Name
        {
            get { return name; }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentException(ExceptionMessages.ARTICLE_NAME_INVALID);
                }
                name = value;
            }
        }

        //only for serialization of price attribute
        public string TextPrice
        {
            get { return Price.ToString("N2"); }
            set { Price = double.Parse(value); }
        }

        [XmlIgnore]
        public double Price
        {
            get { return price; }
            set
            {
                if (value < 0) { throw new ArgumentException(ExceptionMessages.ARTICLE_PRICE_INVALID); }
                price = value;
            }
        }

        public int VAT
        {
            get { return vat; }
            set
            {
                if (value <= 0) { throw new ArgumentException(ExceptionMessages.ARTICLE_VAT_INVALID); }
                vat = value;
            }
        }

        public ArticleType Type
        {
            get { return type; }
            set { type = value; }
        }

        public double Charge(double quantity)
        {
            if (quantity <= 0) { throw new ArgumentException(ExceptionMessages.ARTICLE_CHARGE_QUANTITY_INVALID); }

            return Price * quantity * (1 + (double)VAT / 100);
        }

        public override string ToString()
        {
            var tmp = type == ArticleType.Granulated ? "kg" : "kom";
            return $"{Id} - Naziv: {Name}, Cijena: {Price.ToString("N2")}, PDV: {VAT}, Granulacija: {tmp}";
        }
    }
}
