﻿using System;

namespace CashRegister.Models
{
    [Flags]
    public enum AccountType
    {
        None = 1,
        Administrator = 2,
        Cashier = 4,
        LoggedIn = Administrator | Cashier,
        All = None | Administrator | Cashier
    }
}
